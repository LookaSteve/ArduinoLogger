package card;

import core.Xeon;
import gui.SerialConsole;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

public class Card implements SerialPortEventListener{
	
    private Xeon xeon;
	private CardManager manager;
	private SerialPort serialPort;
	private SerialConsole listeningConsole = null;
	
	private String displayName="";
	private String port;
	private int dataRate;
	private long nbrReceivedStrings = 0;
	private String hogString="";
	
	public Card(Xeon xeon,CardManager manager,String port, int dataRate) {
		
		this.xeon = xeon;
		
		this.manager = manager;
		
		this.port = port;
		
		this.dataRate = dataRate;
		
	}
	
	public void serialEvent(SerialPortEvent event) {
		//Object type SerialPortEvent carries information about which event occurred and a value.
        //For example, if the data came a method event.getEventValue() returns us the number of bytes in the input buffer.
		
		//If char is recieved
		if(event.isRXCHAR()){
			
            if(event.getEventValue() > 0){
            	
                try {
                	
                	//Read
                    byte buffer[] = serialPort.readBytes(event.getEventValue());
                    
                    //Turn bytes to string data
                    String inputLine = new String(buffer);
                    
    				//Append to hogString
    				hogString=hogString+inputLine;
    				
    				//If end of line
    				if(inputLine.endsWith("\n")){
    					
    				    //Increase the counter of received strings
    					nbrReceivedStrings++;
    					
    					//If a serial console is currently paired with the card
    					if(listeningConsole != null){
    					
    					    //We send the received string to the console
    						listeningConsole.addRemoteBroadcast(hogString);
    						
    					}
    					
    					//Send the received string to the data processor along with the source
    					xeon.getDataProcessor().processData(hogString,xeon.getDataProcessor().getLocalSource());
    					
    					//Reset the buffer
    					hogString = "";
    					
    				}
    				
                }catch(SerialPortException ex){
                    System.out.println(ex);
                }
                
            }
        }
		
        //If the CTS line status has changed, then the method event.getEventValue() returns 1 if the line is ON and 0 if it is OFF.
        else if(event.isCTS()){
            if(event.getEventValue() == 1){
                System.out.println("CTS - ON");
            }else{
                System.out.println("CTS - OFF");
            }
        }
		
		//Same for DSR
        else if(event.isDSR()){
            if(event.getEventValue() == 1){
                System.out.println("DSR - ON");
            }else{
                System.out.println("DSR - OFF");
            }
        }
		

	}
	
	public boolean changeDataRate(int newDataRate){
		
		if (newDataRate > 0) {
		    
		    dataRate = newDataRate;
			
			try {
				
				return serialPort.setParams(newDataRate, 8, 1, 0);

			} catch (SerialPortException e) {
				e.printStackTrace();
				return false;
			}
			
		}
		
		return false;
		
	}
	
	public boolean writeData(String data) {
	    
		try {
			
			if(serialPort.writeString(data)){
				System.out.println("Sent: " + data);
				return false;
				
			}else{
				System.out.println("Could not write to port");
				return true;
			}
			
		} catch (Exception e) {
			System.out.println("Could not write to port [Exception]");
			return false;
		}
		
	}

	//Open the serial port with a given baud rate
	public boolean open(int baudRate){
	    
	    //New serialPort Obj
        serialPort = new SerialPort(port);
        
        try {
            
            //Save local data rate
            dataRate = baudRate;
            
            //Open port
            serialPort.openPort();
            
            //Setup
            serialPort.setParams(baudRate, 8, 1, 0);
            int mask = SerialPort.MASK_RXCHAR;
            serialPort.setEventsMask(mask);
            
            //Add listener
            serialPort.addEventListener(this);
            
        } catch (SerialPortException e) {
            e.printStackTrace();
            close();
            return false;
        }
	    
	    return true;
	    
	}
	
	//Close the serial port
	public boolean close(){
		
	    //If the serial port is set
		if(serialPort!=null){
			
		    //If the serial port is currently opened
			if(serialPort.isOpened()){
				
				try {
				    
					//Purge port
				    serialPort.purgePort(1);
				    serialPort.purgePort(2);
				    
				    //Close port
					serialPort.closePort();
					
				}catch(SerialPortException e){
					//Expected exception if the port was forcefully disconnected
				}
				
			}
			
			//Unset the serial port  object before returning true
			serialPort = null;
			return true;
			
		}
		
		//Can't close a null object
		return false;
		
	}
	
	public String getDefaultNickname(){
	    return "Card on port " + port;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public SerialPort getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(SerialPort serialPort) {
		this.serialPort = serialPort;
	}

	public int getDataRate() {
		return dataRate;
	}

	public void setDataRate(int dataRate) {
		
		changeDataRate(dataRate);
		
	}

	public long getNbrReceivedStrings() {
		return nbrReceivedStrings;
	}

	public void setNbrReceivedStrings(long nbrReceivedStrings) {
		this.nbrReceivedStrings = nbrReceivedStrings;
	}

	public CardManager getManager() {
		return manager;
	}

	public void setManager(CardManager manager) {
		this.manager = manager;
	}

	public SerialConsole getListeningConsole() {
		return listeningConsole;
	}

	public void setListeningConsole(SerialConsole listeningConsole) {
		this.listeningConsole = listeningConsole;
	}

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
