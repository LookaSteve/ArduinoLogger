package card;

import java.util.ArrayList;
import java.util.List;

import core.Launcher;
import core.Xeon;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class CardScanner {
	
	private Thread scanThread;
	
	private CardManager manager;
	
	private List<String> bannedPorts = new ArrayList<String>();
	
	private Xeon xeon;
	
	public CardScanner(Xeon xeon,CardManager manager){
		
		this.xeon = xeon;
		
		this.manager = manager;
		
	}
	
	public void addBannedPort(String portToBan){
		
		bannedPorts.add(portToBan);
		
	}
	
	public void start(){
		
		scanThread = new Thread() {
		    public void run() {
		    	
		        while(true){
		        	
		        	scan();
		        	
		        	try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        	
		        }
		        
		    }  
		};
		
		scanThread.start();
		
	}
	
	public void stop(){
		
		//Warning ! Only call this method when closing the scanner
		
		scanThread.stop();
		
		scanThread = null;
		
	}
	
	public void scan(){
	    
	    //Get checsum of detect card list for comparison later
	    String previousChecksum = manager.getCheckString();
	    
	    //Clear detected card list
	    manager.getDetectedCards().clear();
		
		//Get list of ports
		String[] portNames = SerialPortList.getPortNames();

		//Scan for new port
        for(int i = 0; i < portNames.length; i++){
            
            //Try to find existing card in one of the lists
        	Boolean isReferenced = false;
        	//Checking among the detected cards
        	for(int j = 0; j < manager.getDetectedCards().size(); j++){
        		if(manager.getDetectedCards().get(j).getPort().equals(portNames[i])){
        			isReferenced = true;
        			break;
        		}
        	}
        	//Checking among the connected cards
        	for(int j = 0; j < manager.getConnectedCards().size(); j++){
                if(manager.getConnectedCards().get(j).getPort().equals(portNames[i])){
                    isReferenced = true;
                    break;
                }
            }
        	
        	//If new port
        	if(!isReferenced){
        	    
        		//If not banned
        		if(!isBannedPort(portNames[i])){
                	
        		    //Add to manager's list
                    manager.getDetectedCards().add(new Card(xeon,manager,portNames[i],xeon.getDefaultBaudRate()));
                    
                    //System.out.println("PORT " + portNames[i] + " is busy.");
                    
               
                }
        		
        	}
        	
        }
        
        //Compare the new checksum with the old, if there is a difference we refresh
        if(!manager.getCheckString().equals(previousChecksum)){
            manager.getParent().getMainWindow().refreshDetectedCardList();
        }
		
	}
	
	private boolean isBannedPort(String port) {
		for (int i = 0; i < bannedPorts.size(); i++) {
			if (port.equals(bannedPorts.get(i))) {
				return true;
			}
		}
		return false;
	}

	public List<String> getBannedPorts() {
		return bannedPorts;
	}

	public void setBannedPorts(List<String> bannedPorts) {
		this.bannedPorts = bannedPorts;
	}
	
}
