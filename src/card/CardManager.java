package card;

import java.util.ArrayList;

import core.Xeon;

public class CardManager {

	private ArrayList<Card> detectedCards = new ArrayList<Card>();
	private ArrayList<Card> connectedCards = new ArrayList<Card>();
	
	private CardScanner scanner;
	
	private Xeon parent;
	
	public CardManager(Xeon parent){
		
		this.parent = parent;
		
		scanner = new CardScanner(parent,this);
		
	}
	
	public void startCardScanner() {
		
		scanner.start();
		
	}
	
	//Gets a checksum of all detected cards, used to detect change in structure
	public String getCheckString(){
	    
	    String returnString = "";
	    
	    for(int i = 0; i < detectedCards.size(); i++){
	        
	        returnString += detectedCards.get(i).getPort();
	        
	    }
	    
	    return returnString;
	    
	}

	public ArrayList<Card> getDetectedCards() {
        return detectedCards;
    }

    public void setDetectedCards(ArrayList<Card> detectedCards) {
        this.detectedCards = detectedCards;
    }

    public ArrayList<Card> getConnectedCards() {
        return connectedCards;
    }

    public void setConnectedCards(ArrayList<Card> connectedCards) {
        this.connectedCards = connectedCards;
    }

    public CardScanner getScanner() {
		return scanner;
	}

	public void setScanner(CardScanner scanner) {
		this.scanner = scanner;
	}

	public Xeon getParent() {
		return parent;
	}

	public void setParent(Xeon parent) {
		this.parent = parent;
	}
	
	
	
}
