package xml;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import core.Xeon;

public class SaveManager {

	private Xeon xeon;
	
	private Configuration xml;
	
	private File currentFile = null;
	
	public SaveManager(Xeon xeon){
		
		this.xeon = xeon;
		
		xml = new Configuration(xeon);
		
	}
	
	public void promptLoad(){

		 JFileChooser fileChooser = new JFileChooser();
		 fileChooser.setCurrentDirectory(xeon.getDefaultSavePath());
		 fileChooser.setDialogTitle("Select a configuration file (.xml)");
		 FileNameExtensionFilter filter = new FileNameExtensionFilter("XML Configuration Files", "xml", "XML");
		 fileChooser.setFileFilter(filter);
		 
		 int result = fileChooser.showOpenDialog(null);
		 if (result == JFileChooser.APPROVE_OPTION) {
			   xeon.getMainWindow().clearUI();
			   xml.loadXml(fileChooser.getSelectedFile());
			   xml.loadConfiguration();
			   xeon.clearActions();
			   xeon.setDefaultSavePath(fileChooser.getCurrentDirectory());
			   xeon.getDefaultConfigManager().saveConfiguration();
		 }
		
	}
	
	public boolean promptSave(String title){
		
	    boolean finishedSave = false;
	    
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(xeon.getDefaultSavePath());
		fileChooser.setDialogTitle(title);  
		FileNameExtensionFilter filter = new FileNameExtensionFilter("XML Configuration Files", "xml", "XML");
		fileChooser.setFileFilter(filter);
		 
		int result = fileChooser.showSaveDialog(null);
		 
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			if (file.getName().toLowerCase().endsWith(".xml")) {

			} else {
			    file = new File(file.toString() + ".xml");
			}
			currentFile = file;
			xml.writeXml(file);
			xeon.clearActions();
			xeon.setDefaultSavePath(fileChooser.getCurrentDirectory());
            xeon.getDefaultConfigManager().saveConfiguration();
			playSaveAnimation();
			finishedSave=true;
		}
		
		return finishedSave;
		
	}
	
	public void silentSave(){
		
		xml.writeXml(currentFile);
		xeon.clearActions();
		playSaveAnimation();
		
	}
	
	public void newConfig(){
		
		xml = new Configuration(xeon);
		
	}
	
	public void playSaveAnimation(){
		
		Thread animationThread = new Thread(){
		    public void run(){
		      xeon.getMainWindow().getLabelSave().setText("   Saved.   ");
		      try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      xeon.getMainWindow().getLabelSave().setText("");
		    }
		};

		animationThread.start();
		
	}

    public File getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(File currentFile) {
        this.currentFile = currentFile;
    }

    public Configuration getXml() {
        return xml;
    }

    public void setXml(Configuration xml) {
        this.xml = xml;
    }
	
}
