package xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class Xml {
	
	private NodeList blocList;
	
	private Document doc;
	
	public Xml(){
		
	}
	
	public void loadXml(File file,String nodeSeparator){
		
		try {
			 
			 DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         
			 DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         
	         doc = dBuilder.parse(file);
	         
	         doc.getDocumentElement().normalize();
	         
	         blocList = doc.getElementsByTagName(nodeSeparator);
	         
	      } catch (Exception e) {
	    	  
	         e.printStackTrace();
	         
	      }
		
	}
	
	public String cleanString(String string){
		
		if(string == null){
			
			return "";
			
		}
		
		if(string.startsWith(" ") && string.length()>1){
			
			string = string.substring(1);
			
		}
		
		if(string.endsWith(" ") && string.length()>1){
			
			string = string.substring(0,string.length() - 1);
			
		}
		
		return string;
		
	}
	
	public String prepareString(String string){
		
		return " " + string + " ";
		
	}

	public NodeList getBlocList() {
		return blocList;
	}

	public void setBlocList(NodeList blocList) {
		this.blocList = blocList;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}
	
	
	
}
