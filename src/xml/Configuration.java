package xml;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import Modules.StatusObject;
import Modules.TimerEvent;
import Modules.Tools;
import core.Xeon;
import gui.ClientGui;
import gui.CoordinateSet;
import gui.EventScheduler;
import gui.Gauge;
import gui.InternalFrame;
import gui.NumericGraph;
import gui.ServerGui;
import gui.StatusIndicator;
import gui.TimeoutAlarm;

public class Configuration {
	
	private Xml xml;
	
	private Xeon xeon;
	
	public Configuration(Xeon xeon){
		
		this.xeon = xeon;
		
		xml = new Xml();
		
	}
	
	public void loadXml(File file){
		
		xeon.getSaveManager().setCurrentFile(file);
		
		xml = new Xml();
		
		xml.loadXml(file, "bloc");
		
	}
	
	public void loadConfiguration(){
		
		for (int i = 0; i < xml.getBlocList().getLength(); i++) {
			
            Node nNode = xml.getBlocList().item(i);
            
            if(nNode.getNodeName().equals("bloc")){
            	
            	NodeList itemBlocList = nNode.getChildNodes();
            	
            	for (int j = 0; j < itemBlocList.getLength(); j++) {
        			
                    Node subNode = itemBlocList.item(j);
                    
                    NodeList genBlocList = subNode.getChildNodes();
                    
                    if(subNode.getNodeName().equals("general")){
                    	
                    	parseGeneral(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("numgraph")){
                    	
                    	parseNumGraph(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("statusindic")){
                    	
                    	parseStatusIndicator(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("gauge")){
                    	
                    	parseGauge(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("eventscheduler")){
                    	
                    	parseEventScheduler(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("server")){
                    	
                    	parseServer(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("client")){
                    	
                    	parseClient(genBlocList);
                    	
                    }
                    
                    if(subNode.getNodeName().equals("timeoutalarm")){
                        
                        parseTimeoutAlarm(genBlocList);
                        
                    }
            	
            	}
            	
            }
            
           
            
         }
		
	}
	
	private void parseGeneral(NodeList genBlocList){
		
		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            switch(nNode.getNodeName()){
            
            	case "separator" : xeon.setDataSeparator(xml.cleanString(nNode.getTextContent())); break;
            	case "logpath" : xeon.setLogPath(new File(xml.cleanString(nNode.getTextContent()))); break;
            	case "splitsize" : xeon.getMainWindow().getSplitPane().setDividerLocation(Integer.parseInt(xml.cleanString(nNode.getTextContent())));break;
            	case "enablepreprocessing" : xeon.getPreProcessor().setEnablePreProcessing(Boolean.parseBoolean(xml.cleanString(nNode.getTextContent())));break;
            	case "removeheader" : xeon.getPreProcessor().setHeaderToRemove(xml.cleanString(nNode.getTextContent()));break;
            	case "removefooter" : xeon.getPreProcessor().setFooterToRemove(xml.cleanString(nNode.getTextContent()));break;
            	case "matchregex" : xeon.getPreProcessor().setRegexToExecute(xml.cleanString(nNode.getTextContent()));break;
            	case "matchreplace" : xeon.getPreProcessor().setRegexToReplace(xml.cleanString(nNode.getTextContent()));break;
            	case "logheader" : xeon.setLogHeader(xml.cleanString(nNode.getTextContent()));break;
            
            }
            
         }
		
	}
	
	private void parseNumGraph(NodeList genBlocList){
		
		String windowtitle = "Numeric Graph";
		String title = "Numeric Graph";
		int x = 10,y = 10,w = 200,h = 150,index = 0,range = 60;
		String ytitle = "Value";
		Color linecolor = Color.red;
		Color backcolor = Color.lightGray;
		Color gridcolor = Color.white;
		Boolean showx = true;
		Boolean showy = true;
		Boolean autotimerange = true;

		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());
            
            switch(nNode.getNodeName()){
            
	        	case "windowtitle" : windowtitle = (data); break;
	        	case "title" : title = (data); break;
	        	case "x" : x = Integer.parseInt(data); break;
	        	case "y" : y = Integer.parseInt(data); break;
	        	case "w" : w = Integer.parseInt(data); break;
	        	case "h" : h = Integer.parseInt(data); break;
	        	case "ytitle" : ytitle = (data); break;
	        	case "index" : index = Integer.parseInt(data); break;
	        	case "linecolor" : linecolor = Color.decode(data); break;
	        	case "backcolor" : backcolor = Color.decode(data); break;
	        	case "gridcolor" : gridcolor = Color.decode(data); break;
	        	case "showx" : showx = Boolean.parseBoolean(data); break;
	        	case "shwoy" : showy = Boolean.parseBoolean(data); break;
	        	case "autotimerange" : autotimerange = Boolean.parseBoolean(data); break;
	        	case "range" : range = Integer.parseInt(data); break;
        
            }
            
         }
		
		CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
		
		xeon.getMainWindow().getInternalFrameManager().addNewGraphWindow(windowtitle, index, linecolor, backcolor, gridcolor, title, ytitle, showx, showy, autotimerange, range,coordSet);
		
	}
	
	private void parseStatusIndicator(NodeList genBlocList){
		
		String windowtitle = "Status Indicator";
		String title = "Status Indicator";
		int x = 10,y = 10,w = 200,h = 150,index = 0,defaultindex = 0;
		ArrayList<StatusObject> statuslist = new ArrayList<StatusObject>();

		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());

            switch(nNode.getNodeName()){
            
	        	case "windowtitle" : windowtitle = (data); break;
	        	case "title" : title = (data); break;
	        	case "x" : x = Integer.parseInt(data); break;
	        	case "y" : y = Integer.parseInt(data); break;
	        	case "w" : w = Integer.parseInt(data); break;
	        	case "h" : h = Integer.parseInt(data); break;
	        	case "index" : index = Integer.parseInt(data); break;
	        	case "defaultindex" : defaultindex = Integer.parseInt(data); break;
	        	case "statuses" : statuslist = parseStatus(nNode.getChildNodes()); break;
        
            }
            
         }
		
		CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
		
		xeon.getMainWindow().getInternalFrameManager().addNewStatusIndicator(windowtitle, title, index, defaultindex, statuslist,coordSet);
		
	}
	
	private ArrayList<StatusObject> parseStatus(NodeList genBlocList){

		ArrayList<StatusObject> statuslist = new ArrayList<StatusObject>();
		
		for (int i = 0; i < genBlocList.getLength(); i++) {
			
			String statusname = "Status";
			int statusindex = 0;
			Color statuscolor = Color.lightGray;
			
			int statusHit = 0;
			
            Node nNode = genBlocList.item(i);
            
            NodeList statusFields = nNode.getChildNodes();
            
            for (int j = 0; j < statusFields.getLength(); j++) {
            
            	Node subNode = statusFields.item(j);
            	
            	String data = xml.cleanString(subNode.getTextContent());
            	
            	switch(subNode.getNodeName()){
                
		        	case "statusname" : statusname = (data); statusHit++; break;
		        	case "statusindex" : statusindex = Integer.parseInt(data); statusHit++; break;
		        	case "statuscolor" : statuscolor = Color.decode(data); statusHit++; break;
        
            	}
            	
            }
            
            if(statusHit >= 3){
                
                statuslist.add(new StatusObject(statusname, statusindex, statuscolor));
            	
            }
            
         }
		
		return statuslist;
		
	}
	
	private void parseGauge(NodeList genBlocList){
		
		String windowtitle = "Gauge";
		String title = "Gauge";
		int x = 10,y = 10,w = 200,h = 150,index = 0,lowvalue = 0,highvalue = 100;
		Color lowcolor = Color.green;
		Color highcolor = Color.red;
		Color backcolor = Color.lightGray;

		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());
            
            switch(nNode.getNodeName()){
            
	        	case "windowtitle" : windowtitle = (data); break;
	        	case "title" : title = (data); break;
	        	case "x" : x = Integer.parseInt(data); break;
	        	case "y" : y = Integer.parseInt(data); break;
	        	case "w" : w = Integer.parseInt(data); break;
	        	case "h" : h = Integer.parseInt(data); break;
	        	case "index" : index = Integer.parseInt(data); break;
	        	case "lowvalue" : lowvalue = Integer.parseInt(data); break;
	        	case "highvalue" : highvalue = Integer.parseInt(data); break;
	        	case "lowcolor" : lowcolor = Color.decode(data); break;
	        	case "highcolor" : highcolor = Color.decode(data); break;
	        	case "backcolor" : backcolor = Color.decode(data); break;
        
            }
            
         }
		
		CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
		
		xeon.getMainWindow().getInternalFrameManager().addNewGauge(windowtitle, title, index, lowcolor, highcolor, backcolor, lowvalue, highvalue,coordSet);
		
	}
	
	private void parseEventScheduler(NodeList genBlocList){
		
		String windowtitle = "Event Sheduler";
		int x = 10,y = 10,w = 200,h = 150;
		long countdownvalue = 0;
		ArrayList<TimerEvent> eventList = new ArrayList<TimerEvent>();
		boolean isNegative = false;

		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());

            switch(nNode.getNodeName()){
            
	        	case "x" : x = Integer.parseInt(data); break;
	        	case "y" : y = Integer.parseInt(data); break;
	        	case "w" : w = Integer.parseInt(data); break;
	        	case "h" : h = Integer.parseInt(data); break;
	        	case "countdownvalue" : countdownvalue = Long.parseLong(data); break;
	        	case "isnegative" : isNegative = Boolean.parseBoolean(data); break;
	        	case "events" : eventList = parseEvents(nNode.getChildNodes()); break;
        
            }
            
         }
		
		CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
		
		EventScheduler es = xeon.getMainWindow().getInternalFrameManager().addNewEventSheduler(windowtitle, eventList,coordSet);
		
		es.setTimeCount(countdownvalue);
		es.setBackTimeCount(countdownvalue);
		es.setNegative(isNegative);
		es.setBackIsNegative(isNegative);
		
		//Adding the Event Scheduler references in all the events
		
		for(int i = 0; i < es.getCellRenderer().getListOfEvents().size(); i++){
			
			es.getCellRenderer().getListOfEvents().get(i).setEventScheduler(es);
			
		}
		
		es.paintCountDown();
		es.sortEvents();
		
	}
	
	private ArrayList<TimerEvent> parseEvents(NodeList genBlocList){

		ArrayList<TimerEvent> eventList = new ArrayList<TimerEvent>();
		
		for (int i = 0; i < genBlocList.getLength(); i++) {
			
			String statusname = "Event";
			long executionTime = 0;
			Color eventcolor = Color.white;
			String command = "";
			boolean isPostZero = false;
			
			int statusHit = 0;
			
            Node nNode = genBlocList.item(i);
            
            NodeList statusFields = nNode.getChildNodes();
            
            for (int j = 0; j < statusFields.getLength(); j++) {
            
            	Node subNode = statusFields.item(j);
            	
            	String data = xml.cleanString(subNode.getTextContent());
            	
            	switch(subNode.getNodeName()){
                
		        	case "eventname" : statusname = data; statusHit++; break;
		        	case "eventexectime" : executionTime = Long.parseLong(data); statusHit++; break;
		        	case "eventcommand" : command = data; statusHit++; break;
		        	case "eventcolor" : eventcolor = Color.decode(data); statusHit++; break;
		        	case "ispostzero" : isPostZero = Boolean.parseBoolean(data); statusHit++; break;
        
            	}
            	
            }
            
            if(statusHit >= 5){
                
            	//The event sheduler reference is null because the object does not exists yet
            	
                eventList.add(new TimerEvent(xeon,null,statusname, executionTime, command, eventcolor, isPostZero));
            	
            }
            
         }
		
		return eventList;
		
	}
	
	private void parseServer(NodeList genBlocList){
		
		String name = "remote server";
		int x = 10,y = 10,w = 200,h = 150,port = 0;
		boolean autoconnect = true;

		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());
            
            switch(nNode.getNodeName()){
            
	        	case "name" : name = data; break;
	        	case "x" : x = Integer.parseInt(data); break;
	        	case "y" : y = Integer.parseInt(data); break;
	        	case "w" : w = Integer.parseInt(data); break;
	        	case "h" : h = Integer.parseInt(data); break;
	        	case "port" : port = Integer.parseInt(data); break;
	        	case "autoconnect" : autoconnect = Boolean.parseBoolean(data); break;
        
            }
            
         }
		
		CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
		
		xeon.getMainWindow().getInternalFrameManager().addServer(name, port,autoconnect,coordSet);
		
	}
	
	private void parseClient(NodeList genBlocList){
		
		String name = "remote server";
		String address = "";
		int x = 10,y = 10,w = 200,h = 150,port = 0,timeout = 100;
		boolean autoconnect = true;

		for (int i = 0; i < genBlocList.getLength(); i++) {
			
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());
            
            switch(nNode.getNodeName()){
            
	        	case "name" : name = data; break;
	        	case "x" : x = Integer.parseInt(data); break;
	        	case "y" : y = Integer.parseInt(data); break;
	        	case "w" : w = Integer.parseInt(data); break;
	        	case "h" : h = Integer.parseInt(data); break;
	        	case "port" : port = Integer.parseInt(data); break;
	        	case "timeout" : timeout = Integer.parseInt(data); break;
	        	case "address" : address = data; break;
	        	case "autoconnect" : autoconnect = Boolean.parseBoolean(data); break;
        
            }
            
         }
		
		CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
		
		xeon.getMainWindow().getInternalFrameManager().addClient(name, address, port, timeout,autoconnect,coordSet);
		
	}
	
	private void parseTimeoutAlarm(NodeList genBlocList){
        
        int x = 10,y = 10,w = 200,h = 150,timeout = 100;
        boolean autoenable = true, muted = false;

        for (int i = 0; i < genBlocList.getLength(); i++) {
            
            Node nNode = genBlocList.item(i);
            
            String data = xml.cleanString(nNode.getTextContent());
            
            switch(nNode.getNodeName()){
            
                case "x" : x = Integer.parseInt(data); break;
                case "y" : y = Integer.parseInt(data); break;
                case "w" : w = Integer.parseInt(data); break;
                case "h" : h = Integer.parseInt(data); break;
                case "timeout" : timeout = Integer.parseInt(data); break;
                case "autoenable" : autoenable = Boolean.parseBoolean(data); break;
                case "muted" : muted = Boolean.parseBoolean(data); break;
                
            }
            
         }
        
        CoordinateSet coordSet = new CoordinateSet(x, y, w, h);
        
        xeon.getMainWindow().getInternalFrameManager().addNewTimeOutAlarm(timeout,autoenable,coordSet,muted);
        
    }
	
	public void writeXml(File f){
		
		try {
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("configuration");
			doc.appendChild(rootElement);
			
			//General bloc
			Element generalBloc = doc.createElement("bloc");
			rootElement.appendChild(generalBloc);
			
			//General
			Element general = doc.createElement("general");
			generalBloc.appendChild(general);
			
			//Separator
			Element separator = doc.createElement("separator");
			separator.appendChild(doc.createTextNode(xml.prepareString(xeon.getDataSeparator())));
			general.appendChild(separator);
			
			//Log path
			Element logPath = doc.createElement("logpath");
			logPath.appendChild(doc.createTextNode(xml.prepareString(xeon.getLogPath().toString())));
			general.appendChild(logPath);
            
            Element splitSize = doc.createElement("splitsize");
            splitSize.appendChild(doc.createTextNode(xml.prepareString(xeon.getMainWindow().getSplitPane().getDividerLocation()+"")));
            general.appendChild(splitSize);
            
            Element enablepreprocessing = doc.createElement("enablepreprocessing");
            enablepreprocessing.appendChild(doc.createTextNode(xml.prepareString(xeon.getPreProcessor().isEnablePreProcessing()+"")));
            general.appendChild(enablepreprocessing);
            
            Element removeheader = doc.createElement("removeheader");
            removeheader.appendChild(doc.createTextNode(xml.prepareString(xeon.getPreProcessor().getHeaderToRemove())));
            general.appendChild(removeheader);
            
            Element removefooter = doc.createElement("removefooter");
            removefooter.appendChild(doc.createTextNode(xml.prepareString(xeon.getPreProcessor().getFooterToRemove())));
            general.appendChild(removefooter);
            
            Element matchregex = doc.createElement("matchregex");
            matchregex.appendChild(doc.createTextNode(xml.prepareString(xeon.getPreProcessor().getRegexToExecute())));
            general.appendChild(matchregex);
            
            Element matchreplace = doc.createElement("matchreplace");
            matchreplace.appendChild(doc.createTextNode(xml.prepareString(xeon.getPreProcessor().getRegexToReplace())));
            general.appendChild(matchreplace);
            
            Element logheader = doc.createElement("logheader");
            logheader.appendChild(doc.createTextNode(xml.prepareString(xeon.getLogHeader())));
            general.appendChild(logheader);
            
			ArrayList<InternalFrame> listIF = xeon.getMainWindow().getInternalFrameManager().getListOfIF();
			
			for(int i = 0; i < listIF.size(); i ++){
				
				Element bloc = doc.createElement("bloc");
				
				switch(listIF.get(i).getType()){
				
					case "NUMGRAPH" : writeNumericGraph((NumericGraph) listIF.get(i),bloc,doc); break;
					case "STATUSINDIC" : writeStatusIndicator((StatusIndicator) listIF.get(i),bloc,doc); break;
					case "GAUGE" : writeGauge((Gauge) listIF.get(i),bloc,doc); break;
					case "EVENTSHEDULER" : writeEventSheduler((EventScheduler) listIF.get(i), bloc, doc); break;
					case "DATASERVER" : writeServer((ServerGui) listIF.get(i),bloc,doc); break;
					case "DATACLIENT" : writeClient((ClientGui) listIF.get(i),bloc,doc); break;
					case "TIMEOUTALARM" : writeTimeoutAlarm((TimeoutAlarm) listIF.get(i),bloc,doc); break;
					
				}
				
				rootElement.appendChild(bloc);
				
			}
			
			//Write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);
			
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
	
	private void writeNumericGraph(NumericGraph ng,Element bloc,Document doc){
		
		//Graph
		Element graph = doc.createElement("numgraph");
		bloc.appendChild(graph);
		
		Element windowtitle = doc.createElement("windowtitle");
		windowtitle.appendChild(doc.createTextNode(xml.prepareString(ng.getTitle())));
		graph.appendChild(windowtitle);
		
		Element title = doc.createElement("title");
		title.appendChild(doc.createTextNode(xml.prepareString(ng.getCrossHairChart().getGraphTitle())));
		graph.appendChild(title);
		
		Element x = doc.createElement("x");
		x.appendChild(doc.createTextNode(xml.prepareString(ng.getX()+"")));
		graph.appendChild(x);
		
		Element y = doc.createElement("y");
		y.appendChild(doc.createTextNode(xml.prepareString(ng.getY()+"")));
		graph.appendChild(y);
		
		Element w = doc.createElement("w");
		w.appendChild(doc.createTextNode(xml.prepareString(ng.getWidth()+"")));
		graph.appendChild(w);
		
		Element h = doc.createElement("h");
		h.appendChild(doc.createTextNode(xml.prepareString(ng.getHeight()+"")));
		graph.appendChild(h);
		
		Element ytitle = doc.createElement("ytitle");
		ytitle.appendChild(doc.createTextNode(xml.prepareString(ng.getCrossHairChart().getyAxisTitle())));
		graph.appendChild(ytitle);
		
		Element index = doc.createElement("index");
		index.appendChild(doc.createTextNode(xml.prepareString(ng.getDataIndex()+"")));
		graph.appendChild(index);
		
		Element linecolor = doc.createElement("linecolor");
		linecolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(ng.getCrossHairChart().getLineColor()))));
		graph.appendChild(linecolor);
		
		Element backcolor = doc.createElement("backcolor");
		backcolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(ng.getCrossHairChart().getBackGroundColor()))));
		graph.appendChild(backcolor);
		
		Element gridcolor = doc.createElement("gridcolor");
		gridcolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(ng.getCrossHairChart().getGridColor()))));
		graph.appendChild(gridcolor);
		
		Element showx = doc.createElement("showx");
		showx.appendChild(doc.createTextNode(xml.prepareString(ng.getCrossHairChart().isXAxisVisible()+"")));
		graph.appendChild(showx);
		
		Element showy = doc.createElement("showy");
		showy.appendChild(doc.createTextNode(xml.prepareString(ng.getCrossHairChart().isXAxisVisible()+"")));
		graph.appendChild(showy);
		
		Element autotimerange = doc.createElement("autotimerange");
		autotimerange.appendChild(doc.createTextNode(xml.prepareString(ng.getCrossHairChart().isRangeAuto()+"")));
		graph.appendChild(autotimerange);
		
		Element range = doc.createElement("range");
		range.appendChild(doc.createTextNode(xml.prepareString(ng.getCrossHairChart().getRange()+"")));
		graph.appendChild(range);
		
	}
	
	private void writeStatusIndicator(StatusIndicator si,Element bloc,Document doc){
		
		//Graph
		Element statusindic = doc.createElement("statusindic");
		bloc.appendChild(statusindic);
		
		Element windowtitle = doc.createElement("windowtitle");
		windowtitle.appendChild(doc.createTextNode(xml.prepareString(si.getTitle())));
		statusindic.appendChild(windowtitle);
		
		Element title = doc.createElement("title");
		title.appendChild(doc.createTextNode(xml.prepareString(si.getStatusIndicTitle())));
		statusindic.appendChild(title);
		
		Element x = doc.createElement("x");
		x.appendChild(doc.createTextNode(xml.prepareString(si.getX()+"")));
		statusindic.appendChild(x);
		
		Element y = doc.createElement("y");
		y.appendChild(doc.createTextNode(xml.prepareString(si.getY()+"")));
		statusindic.appendChild(y);
		
		Element w = doc.createElement("w");
		w.appendChild(doc.createTextNode(xml.prepareString(si.getWidth()+"")));
		statusindic.appendChild(w);
		
		Element h = doc.createElement("h");
		h.appendChild(doc.createTextNode(xml.prepareString(si.getHeight()+"")));
		statusindic.appendChild(h);
		
		Element index = doc.createElement("index");
		index.appendChild(doc.createTextNode(xml.prepareString(si.getDataIndex()+"")));
		statusindic.appendChild(index);
		
		Element defaultindex = doc.createElement("defaultindex");
		defaultindex.appendChild(doc.createTextNode(xml.prepareString(si.getDefaultIndex()+"")));
		statusindic.appendChild(defaultindex);
		
		//Statuses
		Element statuses = doc.createElement("statuses");
		statusindic.appendChild(statuses);
		
		for(int i = 0; i < si.getStatusList().size(); i++){
			
			StatusObject so = si.getStatusList().get(i);
			
			//Status
			Element status = doc.createElement("status");
			statuses.appendChild(status);
			
			Element statusindex = doc.createElement("statusindex");
			statusindex.appendChild(doc.createTextNode(xml.prepareString(so.getStatusId()+"")));
			status.appendChild(statusindex);
			
			Element statusname = doc.createElement("statusname");
			statusname.appendChild(doc.createTextNode(xml.prepareString(so.getStatusName())));
			status.appendChild(statusname);
			
			Element statuscolor = doc.createElement("statuscolor");
			statuscolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(so.getStatusColor()))));
			status.appendChild(statuscolor);
			
		}
		
		
	}
	
	private void writeEventSheduler(EventScheduler es,Element bloc,Document doc){
		
		//Graph
		Element statusindic = doc.createElement("eventscheduler");
		bloc.appendChild(statusindic);
		
		Element x = doc.createElement("x");
		x.appendChild(doc.createTextNode(xml.prepareString(es.getX()+"")));
		statusindic.appendChild(x);
		
		Element y = doc.createElement("y");
		y.appendChild(doc.createTextNode(xml.prepareString(es.getY()+"")));
		statusindic.appendChild(y);
		
		Element w = doc.createElement("w");
		w.appendChild(doc.createTextNode(xml.prepareString(es.getWidth()+"")));
		statusindic.appendChild(w);
		
		Element h = doc.createElement("h");
		h.appendChild(doc.createTextNode(xml.prepareString(es.getHeight()+"")));
		statusindic.appendChild(h);
		
		Element countdownvalue = doc.createElement("countdownvalue");
		countdownvalue.appendChild(doc.createTextNode(xml.prepareString(es.getBackTimeCount()+"")));
		statusindic.appendChild(countdownvalue);
		
		Element isnegative = doc.createElement("isnegative");
		isnegative.appendChild(doc.createTextNode(xml.prepareString(es.isBackIsNegative()+"")));
		statusindic.appendChild(isnegative);
		
		//Events
		Element statuses = doc.createElement("events");
		statusindic.appendChild(statuses);
		
		for(int i = 0; i < es.getCellRenderer().getListOfEvents().size(); i++){
			
			TimerEvent te = es.getCellRenderer().getListOfEvents().get(i);
			
			//Status
			Element status = doc.createElement("event");
			statuses.appendChild(status);
			
			Element eventname = doc.createElement("eventname");
			eventname.appendChild(doc.createTextNode(xml.prepareString(te.getName())));
			status.appendChild(eventname);
			
			Element eventexectime = doc.createElement("eventexectime");
			eventexectime.appendChild(doc.createTextNode(xml.prepareString(te.getExecutionTime()+"")));
			status.appendChild(eventexectime);
			
			Element eventcommand = doc.createElement("eventcommand");
			eventcommand.appendChild(doc.createTextNode(xml.prepareString(te.getCommand())));
			status.appendChild(eventcommand);
			
			Element eventcolor = doc.createElement("eventcolor");
			eventcolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(te.getColor()))));
			status.appendChild(eventcolor);
			
			Element ispostzero = doc.createElement("ispostzero");
			ispostzero.appendChild(doc.createTextNode(xml.prepareString(te.isPostZero() + "")));
			status.appendChild(ispostzero);
			
		}
		
		
	}
	
	private void writeGauge(Gauge gau,Element bloc,Document doc){
		
		//Graph
		Element gauge = doc.createElement("gauge");
		bloc.appendChild(gauge);
		
		Element windowtitle = doc.createElement("windowtitle");
		windowtitle.appendChild(doc.createTextNode(xml.prepareString(gau.getTitle())));
		gauge.appendChild(windowtitle);
		
		Element title = doc.createElement("title");
		title.appendChild(doc.createTextNode(xml.prepareString(gau.getGaugeTitle())));
		gauge.appendChild(title);
		
		Element x = doc.createElement("x");
		x.appendChild(doc.createTextNode(xml.prepareString(gau.getX()+"")));
		gauge.appendChild(x);
		
		Element y = doc.createElement("y");
		y.appendChild(doc.createTextNode(xml.prepareString(gau.getY()+"")));
		gauge.appendChild(y);
		
		Element w = doc.createElement("w");
		w.appendChild(doc.createTextNode(xml.prepareString(gau.getWidth()+"")));
		gauge.appendChild(w);
		
		Element h = doc.createElement("h");
		h.appendChild(doc.createTextNode(xml.prepareString(gau.getHeight()+"")));
		gauge.appendChild(h);
		
		Element index = doc.createElement("index");
		index.appendChild(doc.createTextNode(xml.prepareString(gau.getDataIndex()+"")));
		gauge.appendChild(index);
		
		Element lowvalue = doc.createElement("lowvalue");
		lowvalue.appendChild(doc.createTextNode(xml.prepareString(gau.getMinVal()+"")));
		gauge.appendChild(lowvalue);
		
		Element highvalue = doc.createElement("highvalue");
		highvalue.appendChild(doc.createTextNode(xml.prepareString(gau.getMaxVal()+"")));
		gauge.appendChild(highvalue);
		
		Element lowcolor = doc.createElement("lowcolor");
		lowcolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(gau.getLowCol()))));
		gauge.appendChild(lowcolor);
		
		Element highcolor = doc.createElement("highcolor");
		highcolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(gau.getHighCol()))));
		gauge.appendChild(highcolor);
		
		Element backcolor = doc.createElement("backcolor");
		backcolor.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(gau.getBackCol()))));
		gauge.appendChild(backcolor);
		
	}
	
	private void writeServer(ServerGui ser,Element bloc,Document doc){
		
		//Graph
		Element server = doc.createElement("server");
		bloc.appendChild(server);
		
		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode(xml.prepareString(ser.getName())));
		server.appendChild(name);
		
		Element x = doc.createElement("x");
		x.appendChild(doc.createTextNode(xml.prepareString(ser.getX()+"")));
		server.appendChild(x);
		
		Element y = doc.createElement("y");
		y.appendChild(doc.createTextNode(xml.prepareString(ser.getY()+"")));
		server.appendChild(y);
		
		Element w = doc.createElement("w");
		w.appendChild(doc.createTextNode(xml.prepareString(ser.getWidth()+"")));
		server.appendChild(w);
		
		Element h = doc.createElement("h");
		h.appendChild(doc.createTextNode(xml.prepareString(ser.getHeight()+"")));
		server.appendChild(h);
		
		Element port = doc.createElement("port");
		port.appendChild(doc.createTextNode(xml.prepareString(ser.getPort()+"")));
		server.appendChild(port);
		
		Element autoConnect = doc.createElement("autoconnect");
		autoConnect.appendChild(doc.createTextNode(xml.prepareString(ser.isAutoConnect()+"")));
		server.appendChild(autoConnect);
		
	}
	
	private void writeClient(ClientGui cli,Element bloc,Document doc){
		
		//Graph
		Element client = doc.createElement("client");
		bloc.appendChild(client);
		
		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode(xml.prepareString(cli.getName())));
		client.appendChild(name);
		
		Element x = doc.createElement("x");
		x.appendChild(doc.createTextNode(xml.prepareString(cli.getX()+"")));
		client.appendChild(x);
		
		Element y = doc.createElement("y");
		y.appendChild(doc.createTextNode(xml.prepareString(cli.getY()+"")));
		client.appendChild(y);
		
		Element w = doc.createElement("w");
		w.appendChild(doc.createTextNode(xml.prepareString(cli.getWidth()+"")));
		client.appendChild(w);
		
		Element h = doc.createElement("h");
		h.appendChild(doc.createTextNode(xml.prepareString(cli.getHeight()+"")));
		client.appendChild(h);
		
		Element port = doc.createElement("port");
		port.appendChild(doc.createTextNode(xml.prepareString(cli.getPort()+"")));
		client.appendChild(port);
		
		Element timeout = doc.createElement("timeout");
		timeout.appendChild(doc.createTextNode(xml.prepareString(cli.getTimeout()+"")));
		client.appendChild(timeout);
		
		Element address = doc.createElement("address");
		address.appendChild(doc.createTextNode(xml.prepareString(cli.getAddress())));
		client.appendChild(address);
		
		Element autoConnect = doc.createElement("autoconnect");
		autoConnect.appendChild(doc.createTextNode(xml.prepareString(cli.isAutoConnect()+"")));
		client.appendChild(autoConnect);
		
	}
	
	private void writeTimeoutAlarm(TimeoutAlarm toa,Element bloc,Document doc){
        
        //Graph
        Element client = doc.createElement("timeoutalarm");
        bloc.appendChild(client);
        
        Element x = doc.createElement("x");
        x.appendChild(doc.createTextNode(xml.prepareString(toa.getX()+"")));
        client.appendChild(x);
        
        Element y = doc.createElement("y");
        y.appendChild(doc.createTextNode(xml.prepareString(toa.getY()+"")));
        client.appendChild(y);
        
        Element w = doc.createElement("w");
        w.appendChild(doc.createTextNode(xml.prepareString(toa.getWidth()+"")));
        client.appendChild(w);
        
        Element h = doc.createElement("h");
        h.appendChild(doc.createTextNode(xml.prepareString(toa.getHeight()+"")));
        client.appendChild(h);
        
        Element timeout = doc.createElement("timeout");
        timeout.appendChild(doc.createTextNode(xml.prepareString(toa.getTimeoutSecond()+"")));
        client.appendChild(timeout);
        
        Element autoenable = doc.createElement("autoenable");
        autoenable.appendChild(doc.createTextNode(xml.prepareString(toa.isAutoenable()+"")));
        client.appendChild(autoenable);
        
        Element muted = doc.createElement("muted");
        muted.appendChild(doc.createTextNode(xml.prepareString(toa.isMuted()+"")));
        client.appendChild(muted);
        
    }
	
}
