package xml;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.tools.Tool;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import Modules.Tools;
import core.Xeon;
import gui.EventScheduler;
import gui.Gauge;
import gui.InternalFrame;
import gui.NumericGraph;
import gui.StatusIndicator;

public class DefaultConfig {

	private Xeon xeon;
	
	private Xml xml;
	
	private File file;
	
	private String rootNode = "config";
	
	private String nodeSeparator = "parameter";
	
	public DefaultConfig(Xeon xeon,File file){
		
		this.xeon = xeon;
		
		this.file = file;
		
		loadXml(file);
		
	}
	
	private void loadXml(File file){
		
		xml = new Xml();
		xml.loadXml(file, nodeSeparator);
		
	}
	
	public void saveConfiguration(){
		
		writeXml();
		
	}
	
	public void loadConfiguration(){
		

		for (int i = 0; i < xml.getBlocList().getLength(); i++) {
			
            Node nNode = xml.getBlocList().item(i);
            
            if(nNode.getNodeName().equals(nodeSeparator)){
            	
            	NodeList itemBlocList = nNode.getChildNodes();
            	
            	for (int j = 0; j < itemBlocList.getLength(); j++) {
        			
                    Node subNode = itemBlocList.item(j);
                    
                    switch (subNode.getNodeName()){
                    
                    	case "serverCommunicationPort": xeon.setServerCommunicationPort(Integer.parseInt(xml.cleanString(subNode.getTextContent()))); break;
                    	case "serverTimeout": xeon.setServerTimeout(Integer.parseInt(xml.cleanString(subNode.getTextContent()))); break;
                    	case "defaultBaudRate": xeon.setDefaultBaudRate(Integer.parseInt(xml.cleanString(subNode.getTextContent()))); break;
                    	case "loopProtection": xeon.setLoopProtection(Boolean.parseBoolean(xml.cleanString(subNode.getTextContent()))); break;
                    	case "logoFile": xeon.setLogoFile(checkLogo(xml.cleanString(subNode.getTextContent())));break;
                    	case "logoSize": xeon.setLogoSize(Integer.parseInt(xml.cleanString(subNode.getTextContent())));break;
                    	case "defaultPath": xeon.setDefaultSavePath(checkSavePath(xml.cleanString(subNode.getTextContent())));break;
                    	case "backgroundColor": xeon.setBackgroundColor(Color.decode(xml.cleanString(subNode.getTextContent())));break;
                    	case "bannedPorts": parseBannedPorts(subNode.getChildNodes());break;
                    	case "defaultDataFileManagementPath": xeon.setDefaultDataFileManagementPath(checkSavePath(xml.cleanString(subNode.getTextContent())));break;
                    	case "logoOpacity": xeon.setLogoOpacity(Integer.parseInt(xml.cleanString(subNode.getTextContent())));break;
                    	
                    }
            	
            	}
            	
            } 
            
         }
		
	}
	
	private void parseBannedPorts(NodeList genBlocList){
	    
	    for (int i = 0; i < genBlocList.getLength(); i++) {
	        
	        Node nNode = genBlocList.item(i);
	        
	        String data = xml.cleanString(nNode.getTextContent());
	        
	        if(nNode.getNodeName().equals("port")){
	            
	            xeon.getCardManager().getScanner().getBannedPorts().add(data);
	            
	        }
	        
	    }
	    
	}
	
	private void writeXml(){
		
		try {
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement(rootNode);
			doc.appendChild(rootElement);
			
			for(int i = 0; i < 11; i ++){
				
				//Add node separator
				Element node = doc.createElement(nodeSeparator);
				rootElement.appendChild(node);
				
				Element config = null;
				
				switch (i) {
				
					case 0:
						
						config = doc.createElement("serverCommunicationPort");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.getServerCommunicationPort()+"")));
						
					break;
					
					case 1:
						
						config = doc.createElement("serverTimeout");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.getServerTimeout()+"")));
						
					break;

					case 2:
						
						config = doc.createElement("defaultBaudRate");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.getDefaultBaudRate()+"")));
						
					break;
					
					case 3:
						
						config = doc.createElement("loopProtection");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.isLoopProtection()+"")));
						
					break;
					
					case 4:
						
						config = doc.createElement("logoFile");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.getLogoFile().toString())));
						
					break;
					
					case 5:
						
						config = doc.createElement("logoSize");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.getLogoSize()+"")));
						
					break;

					case 6:
						
						config = doc.createElement("defaultPath");
						config.appendChild(doc.createTextNode(xml.prepareString(xeon.getDefaultSavePath().toString())));
						
					break;
					
					case 7:
						
						config = doc.createElement("backgroundColor");
						config.appendChild(doc.createTextNode(xml.prepareString(Tools.colorToHex(xeon.getBackgroundColor()))));
						
					break;
					
					case 8:
                        
                        config = doc.createElement("bannedPorts");
                        writeBannedPorts(doc,config);
                        
                    break;
                    
					case 9:
                        
					    config = doc.createElement("defaultDataFileManagementPath");
                        config.appendChild(doc.createTextNode(xml.prepareString(xeon.getDefaultDataFileManagementPath().toString())));
                        
                    break;
                    
					case 10:
                        
                        config = doc.createElement("logoOpacity");
                        config.appendChild(doc.createTextNode(xml.prepareString(xeon.getLogoOpacity()+"")));
                        
                    break;

					
				}
				
				if(config != null){
					
					node.appendChild(config);
					
				}
				
			}
			
			//Write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}

		
		
	}
	
	private void writeBannedPorts(Document doc,Element node){
	    
	    List<String> bannedPorts = xeon.getCardManager().getScanner().getBannedPorts();
	    
	    for(int i = 0; i < bannedPorts.size(); i++){
	        
	        Text portTextNode = doc.createTextNode(xml.prepareString(bannedPorts.get(i)));
	        
	        Element portNode = doc.createElement("port");
	        
	        portNode.appendChild(portTextNode);
	        
	        node.appendChild(portNode);
	        
	    }
	    
	}
	
	private File checkLogo(String filePath){
		
		File file = new File(filePath);
		
		if(file.exists()){
			
			return file;
			
		}else{
			
			return new File(xeon.getDefaultLogoPath());
			
		}
		
	}
	
	private File checkSavePath(String filePath){
		
		File file = new File(filePath);
		
		if(file.exists()){
			
			return file;
			
		}else{
			
			return xeon.getPlaceholderPath();
			
		}
		
	}
	
}
