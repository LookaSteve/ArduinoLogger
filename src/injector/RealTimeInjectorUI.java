package injector;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import Modules.NumericField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.BoxLayout;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.miginfocom.swing.MigLayout;

public class RealTimeInjectorUI extends JPanel {

    private RealTimeInjector realTimeInjector;
    private NumericField autoInjectorCursorPositionField;
    private JProgressBar autoInjectorProgressBar;
    private JLabel lblStatusAutoInjector;
    private JButton btnReset;
    private JLabel lblCountOfItems;
    private JLabel lblReplayedTime;
    private JLabel lblNextTime;
    
    public RealTimeInjectorUI(RealTimeInjector realTimeInjector) {
        
        this.realTimeInjector = realTimeInjector;
        
        setLayout(new BorderLayout(0, 0));

        JPanel autoInjectorControlPanel = new JPanel();
        this.add(autoInjectorControlPanel, BorderLayout.NORTH);
        
        JButton btnStart = new JButton("Start");
        btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                realTimeInjector.start();
                
            }
        });
        
        JButton btnPause = new JButton("Pause");
        btnPause.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                realTimeInjector.pause();
            }
        });
        
        JButton btnStop = new JButton("Stop");
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                realTimeInjector.stop();
            }
        });
        
        btnReset = new JButton("Reset");
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                realTimeInjector.reset();
            }
        });
        autoInjectorControlPanel.setLayout(new MigLayout("", "[65px,grow][73px,grow][65px,grow][71px,grow]", "[29px]"));
        autoInjectorControlPanel.add(btnStart, "cell 0 0,growx,aligny top");
        autoInjectorControlPanel.add(btnPause, "cell 1 0,growx,aligny top");
        autoInjectorControlPanel.add(btnStop, "cell 2 0,growx,aligny top");
        autoInjectorControlPanel.add(btnReset, "cell 3 0,growx,aligny top");
        
        JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);
        panel.setLayout(new BorderLayout(0, 0));
        
        autoInjectorProgressBar = new JProgressBar();
        autoInjectorProgressBar.setString("Idle.");
        autoInjectorProgressBar.setStringPainted(true);
        panel.add(autoInjectorProgressBar, BorderLayout.NORTH);
        
        JScrollPane scrollPane = new JScrollPane();
        add(scrollPane, BorderLayout.CENTER);
        
        JPanel autoInjectorSettingsPanel = new JPanel();
        scrollPane.setViewportView(autoInjectorSettingsPanel);
        GridBagLayout gbl_autoInjectorSettingsPanel = new GridBagLayout();
        gbl_autoInjectorSettingsPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_autoInjectorSettingsPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_autoInjectorSettingsPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_autoInjectorSettingsPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        autoInjectorSettingsPanel.setLayout(gbl_autoInjectorSettingsPanel);
        
        JLabel label = new JLabel("     ");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        autoInjectorSettingsPanel.add(label, gbc_label);
        
        JLabel label_1 = new JLabel("     ");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 5, 0);
        gbc_label_1.gridx = 3;
        gbc_label_1.gridy = 0;
        autoInjectorSettingsPanel.add(label_1, gbc_label_1);
        
        JLabel lblCursorPosition = new JLabel("Cursor position");
        GridBagConstraints gbc_lblCursorPosition = new GridBagConstraints();
        gbc_lblCursorPosition.anchor = GridBagConstraints.WEST;
        gbc_lblCursorPosition.insets = new Insets(0, 0, 5, 5);
        gbc_lblCursorPosition.gridx = 1;
        gbc_lblCursorPosition.gridy = 1;
        autoInjectorSettingsPanel.add(lblCursorPosition, gbc_lblCursorPosition);
        
        JPanel cursorPositionPanel = new JPanel();
        GridBagConstraints gbc_cursorPositionPanel = new GridBagConstraints();
        gbc_cursorPositionPanel.insets = new Insets(0, 0, 5, 5);
        gbc_cursorPositionPanel.fill = GridBagConstraints.BOTH;
        gbc_cursorPositionPanel.gridx = 1;
        gbc_cursorPositionPanel.gridy = 2;
        autoInjectorSettingsPanel.add(cursorPositionPanel, gbc_cursorPositionPanel);
        cursorPositionPanel.setLayout(new BoxLayout(cursorPositionPanel, BoxLayout.X_AXIS));
        
        autoInjectorCursorPositionField = new NumericField();
        cursorPositionPanel.add(autoInjectorCursorPositionField);
        autoInjectorCursorPositionField.setValue(realTimeInjector.getCursorPosition());
        autoInjectorCursorPositionField.setColumns(10);
        autoInjectorCursorPositionField.getDocument().addDocumentListener(new DocumentListener() {
            
            public void changedUpdate(DocumentEvent e) {
              update();
            }
            public void removeUpdate(DocumentEvent e) {
              update();
            }
            public void insertUpdate(DocumentEvent e) {
              update();
            }

            public void update() {
                
                //If we are in edition mode
                if(realTimeInjector.isActive() && !realTimeInjector.isPaused()){
                    return;
                }
                
                //Get field value
                int value = autoInjectorCursorPositionField.getValue();
                
                //If value is at least 0
                if(value>=0){
                    
                    //If the new value is smallet than the OOB of the dataset
                    if(value < realTimeInjector.getDataSetSize()){
                        //Accepected new value
                        realTimeInjector.setCursorPosition(value);
                    }else if(realTimeInjector.getDataSetSize()==0){
                        //In the case that the dataset is null
                        //Set current value to 0
                        realTimeInjector.setCursorPosition(0);
                        Runnable doHighlight = new Runnable() {
                            @Override
                            public void run() {
                                autoInjectorCursorPositionField.setValue(0);
                            }
                        };       
                        SwingUtilities.invokeLater(doHighlight);
                    }else{
                        //If we have a value higher than the OOB limit
                        //Set the penultimate value of the index
                        realTimeInjector.setCursorPosition(realTimeInjector.getDataSetSize()-2);
                        Runnable doHighlight = new Runnable() {
                            @Override
                            public void run() {
                                autoInjectorCursorPositionField.setValue(realTimeInjector.getDataSetSize()-1);
                            }
                        };       
                        SwingUtilities.invokeLater(doHighlight);
                        
                    }
                    
                    realTimeInjector.setTimeFromCurrentIndex();
                    
                    realTimeInjector.updateProgressBar();
                    
                }else{
                    
                    //Set to 0
                    realTimeInjector.setCursorPosition(0);
                    Runnable doHighlight = new Runnable() {
                        @Override
                        public void run() {
                            autoInjectorCursorPositionField.setValue(1);
                        }
                    };       
                    SwingUtilities.invokeLater(doHighlight);
                }
                
            }
            
          });
        
        lblCountOfItems = new JLabel("   Out of 0 items   ");
        GridBagConstraints gbc_lblCountOfItems = new GridBagConstraints();
        gbc_lblCountOfItems.insets = new Insets(0, 0, 5, 5);
        gbc_lblCountOfItems.gridx = 2;
        gbc_lblCountOfItems.gridy = 2;
        autoInjectorSettingsPanel.add(lblCountOfItems, gbc_lblCountOfItems);
        
        JLabel label_4 = new JLabel("     ");
        GridBagConstraints gbc_label_4 = new GridBagConstraints();
        gbc_label_4.insets = new Insets(0, 0, 5, 5);
        gbc_label_4.gridx = 0;
        gbc_label_4.gridy = 3;
        autoInjectorSettingsPanel.add(label_4, gbc_label_4);
        
        lblReplayedTime = new JLabel("Replayed Time : 00:00:00:000");
        GridBagConstraints gbc_lblReplayedTime = new GridBagConstraints();
        gbc_lblReplayedTime.anchor = GridBagConstraints.WEST;
        gbc_lblReplayedTime.insets = new Insets(0, 0, 5, 5);
        gbc_lblReplayedTime.gridx = 1;
        gbc_lblReplayedTime.gridy = 4;
        autoInjectorSettingsPanel.add(lblReplayedTime, gbc_lblReplayedTime);
        
        JButton btnGoBacks = new JButton("Go back 10s");
        btnGoBacks.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                realTimeInjector.goBackInTime(10);
            }
        });
        GridBagConstraints gbc_btnGoBacks = new GridBagConstraints();
        gbc_btnGoBacks.anchor = GridBagConstraints.EAST;
        gbc_btnGoBacks.insets = new Insets(0, 0, 5, 5);
        gbc_btnGoBacks.gridx = 2;
        gbc_btnGoBacks.gridy = 4;
        autoInjectorSettingsPanel.add(btnGoBacks, gbc_btnGoBacks);
        
        lblNextTime = new JLabel("Until next Line  : 00:00:00:000");
        GridBagConstraints gbc_lblNextTime = new GridBagConstraints();
        gbc_lblNextTime.anchor = GridBagConstraints.WEST;
        gbc_lblNextTime.insets = new Insets(0, 0, 5, 5);
        gbc_lblNextTime.gridx = 1;
        gbc_lblNextTime.gridy = 5;
        autoInjectorSettingsPanel.add(lblNextTime, gbc_lblNextTime);
        
        JLabel label_2 = new JLabel("    ");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 6;
        autoInjectorSettingsPanel.add(label_2, gbc_label_2);
        
        lblStatusAutoInjector = new JLabel("Status : " + realTimeInjector.getInjectorStatus());
        GridBagConstraints gbc_lblStatusAutoInjector = new GridBagConstraints();
        gbc_lblStatusAutoInjector.anchor = GridBagConstraints.WEST;
        gbc_lblStatusAutoInjector.insets = new Insets(0, 0, 5, 5);
        gbc_lblStatusAutoInjector.gridx = 1;
        gbc_lblStatusAutoInjector.gridy = 7;
        autoInjectorSettingsPanel.add(lblStatusAutoInjector, gbc_lblStatusAutoInjector);
        
        JLabel label_3 = new JLabel("    ");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.insets = new Insets(0, 0, 0, 5);
        gbc_label_3.gridx = 0;
        gbc_label_3.gridy = 8;
        autoInjectorSettingsPanel.add(label_3, gbc_label_3);
        
    }

    public RealTimeInjector getAutoInjector() {
        return realTimeInjector;
    }

    public void setAutoInjector(RealTimeInjector realTimeInjector) {
        this.realTimeInjector = realTimeInjector;
    }

    public NumericField getAutoInjectorCursorPositionField() {
        return autoInjectorCursorPositionField;
    }

    public void setAutoInjectorCursorPositionField(NumericField autoInjectorCursorPositionField) {
        this.autoInjectorCursorPositionField = autoInjectorCursorPositionField;
    }

    public JProgressBar getAutoInjectorProgressBar() {
        return autoInjectorProgressBar;
    }

    public void setAutoInjectorProgressBar(JProgressBar autoInjectorProgressBar) {
        this.autoInjectorProgressBar = autoInjectorProgressBar;
    }

    public JLabel getLblStatusAutoInjector() {
        return lblStatusAutoInjector;
    }

    public void setLblStatusAutoInjector(JLabel lblStatusAutoInjector) {
        this.lblStatusAutoInjector = lblStatusAutoInjector;
    }

    public JButton getBtnReset() {
        return btnReset;
    }

    public void setBtnReset(JButton btnReset) {
        this.btnReset = btnReset;
    }

    public JLabel getLblCountOfItems() {
        return lblCountOfItems;
    }

    public void setLblCountOfItems(JLabel lblCountOfItems) {
        this.lblCountOfItems = lblCountOfItems;
    }

    public JLabel getLblReplayedTime() {
        return lblReplayedTime;
    }

    public void setLblReplayedTime(JLabel lblReplayedTime) {
        this.lblReplayedTime = lblReplayedTime;
    }

    public JLabel getLblNextTime() {
        return lblNextTime;
    }

    public void setLblNextTime(JLabel lblNextTime) {
        this.lblNextTime = lblNextTime;
    }

}
