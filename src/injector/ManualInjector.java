package injector;

import javax.swing.JPanel;

import Modules.CheckListItem;
import core.Xeon;
import gui.LogManagementUI;

public class ManualInjector extends Injector{

    private ManualInjectorUI manualInjectorUI;
    
    public ManualInjector(Xeon xeon, LogManagementUI ui) {
        super(xeon, ui);
    }

    @Override
    public void onLoad(){
        updateItemCount();
    }
    
    @Override
    public JPanel onDraw(){
        
        manualInjectorUI = new ManualInjectorUI(this);
        return manualInjectorUI;
        
    }
    
    public void reset(){
        setCursorPosition(0);
        updateCursorField();
        updateProgressBar();
        writeInjectorStatus("Reset.");
    }
    
    public void step(){
        
        if(isCursorValid()){
            
            String dataString = "";
            
            for(int j = 0; j < getUi().getDataSet().getDataLines().get(getCursorPosition()).safeGetDataListSize(); j ++){
                
                if(((CheckListItem) getUi().getListAvailableInjectionModel().getElementAt(j)).isSelected()){
                
                    dataString = dataString + getUi().getDataSet().getDataLines().get(getCursorPosition()).safeGetDataList(j);
                    
                }
                
                dataString = dataString + getXeon().getDataSeparator();
                
            }
            
            System.out.println("RDSTR -> " +dataString);
            
            getXeon().getDataProcessor().processData(dataString,getXeon().getDataProcessor().getLocalSource());
            
            writeInjectorStatus("Injected line "+getCursorPosition()+", step to continue.");
            
            incrementCursor();
            
            updateCursorField();
            
            updateProgressBar();
            
            if(!isCursorValid()){
                writeInjectorStatus("End of dataset reached.");
            }
            
        }else{
            writeInjectorStatus("End of dataset reached.");
        }
        
    }
    
    public void resetCursor(){
        setCursorPosition(0);
        updateCursorField();
    }
    
    public void writeInjectorStatus(String status){
        manualInjectorUI.getLblStatusAutoInjector().setText("Status : " + status);
    }
    
    public void updateProgressBar(){
        manualInjectorUI.getAutoInjectorProgressBar().setMaximum(getDataSetSize());
        manualInjectorUI.getAutoInjectorProgressBar().setValue(getCursorPosition());
        manualInjectorUI.getAutoInjectorProgressBar().setString(getCursorPosition()+"/"+getDataSetSize());
    }
    
    public void updateCursorField(){
        manualInjectorUI.getAutoInjectorCursorPositionField().setValue(getCursorPosition());
    }
    
    public void updateItemCount(){
        manualInjectorUI.getLblCountOfItems().setText("   Out of "+getDataSetSize()+" items   ");
    }
    
}
