package injector;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import Modules.CheckListItem;
import Modules.NumericField;
import core.Xeon;
import gui.LogManagementUI;

public class AutoInjector extends Injector{

    private Thread injectionThread;
    private AutoInjectorUI autoInjectorUI;
    
    private String injectorStatus = "Idle.";
    
    private boolean isActive = false;
    private boolean isPaused = false;
    
    public AutoInjector(Xeon xeon, LogManagementUI ui) {
        super(xeon, ui);
    }

    @Override
    public void onLoad(){
        updateItemCount();
    }
    
    @Override
    public void onClose(){
        stop();
    }
    
    @Override
    public JPanel onDraw(){
        
        autoInjectorUI =  new AutoInjectorUI(this);
        return autoInjectorUI;
        
    }
    
    public void stop(){
        if(injectionThread != null){
            if(injectionThread.isAlive()){
                injectionThread.stop();
            }
        }
        isActive = false;
        isPaused = false;
        resetCursor();
        writeInjectorStatus("Stopped.");
        autoInjectorUI.getAutoInjectorCursorPositionField().setEditable(true);
        autoInjectorUI.getBtnReset().setEnabled(true);
    }
    
    public void pause(){
        
        if(isActive){
            
            if(isPaused){
                //Resume
                resume();
            }else{
                //Pause
                isPaused=true;
                autoInjectorUI.getAutoInjectorCursorPositionField().setEditable(true);
                autoInjectorUI.getBtnReset().setEnabled(true);
                writeInjectorStatus("Paused.");
            }
            
        }
        
    }
    
    private void resume(){
        isPaused = false;
        autoInjectorUI.getAutoInjectorCursorPositionField().setEditable(false);
        autoInjectorUI.getBtnReset().setEnabled(false);
        writeInjectorStatus("Injecting...");
    }
    
    public void reset(){
        if(!isActive || isPaused){
            setCursorPosition(0);
            updateCursorField();
            updateProgressBar();
            writeInjectorStatus("Reset.");
        }
    }
    
    public void start(){
        
        //Check if we are already started
        if(isActive){
            
            //If we want to resume redirect
            if(isPaused){
                
                resume();
                
            }
            
            //Then return because wa are already started
            return;
        }
        
        //Set correct state on all booleans
        isActive = true;
        isPaused = false;
        
        //Start message
        writeInjectorStatus("Injecting...");
        
        //Disable edition field
        autoInjectorUI.getAutoInjectorCursorPositionField().setEditable(false);
        //Disable reset button
        autoInjectorUI.getBtnReset().setEnabled(false);
        
        injectionThread = new Thread(){
        
            public void run(){
              
                while(isCursorValid()){
                    
                    //Check for pause
                    while(isPaused){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    
                    String dataString = "";
                    
                    for(int j = 0; j < getUi().getDataSet().getDataLines().get(getCursorPosition()).safeGetDataListSize(); j ++){
                        
                        if(((CheckListItem) getUi().getListAvailableInjectionModel().getElementAt(j)).isSelected()){
                        
                            dataString = dataString + getUi().getDataSet().getDataLines().get(getCursorPosition()).safeGetDataList(j);
                            
                        }
                        
                        dataString = dataString + getXeon().getDataSeparator();
                        
                    }
                    
                    System.out.println("RDSTR -> " +dataString);
                    
                    getXeon().getDataProcessor().processData(dataString,getXeon().getDataProcessor().getLocalSource());
                    
                    incrementCursor();
                    
                    updateCursorField();
                    
                    updateProgressBar();
                    
                    if(!isCursorValid()){
                        break;
                    }
                    
                    int delay = 1;
                    
                    try {
                        delay = Math.abs(autoInjectorUI.getAutoInjectorTimeIntervalField().getValue());
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } 
                    
                    if(delay < 1){               
                        delay = 1;
                    }
                    
                    try {
                        sleep(delay);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    
                }
                
                //Release field
                autoInjectorUI.getAutoInjectorCursorPositionField().setEditable(true);
                autoInjectorUI.getBtnReset().setEnabled(true);
                
                //Release booleans
                isActive = false;
                isPaused = false;
                
                //We are done.
                writeInjectorStatus("Injection complete.");
                
            }
        };

        injectionThread.start();
        
    }
    
    public void resetCursor(){
        setCursorPosition(0);
        updateCursorField();
    }
    
    public void writeInjectorStatus(String status){
        injectorStatus = status;
        autoInjectorUI.getLblStatusAutoInjector().setText("Status : " + injectorStatus);
    }
    
    public void updateProgressBar(){
        autoInjectorUI.getAutoInjectorProgressBar().setMaximum(getDataSetSize());
        autoInjectorUI.getAutoInjectorProgressBar().setValue(getCursorPosition());
        autoInjectorUI.getAutoInjectorProgressBar().setString(getCursorPosition()+"/"+getDataSetSize());
    }
    
    public void updateCursorField(){
        autoInjectorUI.getAutoInjectorCursorPositionField().setValue(getCursorPosition());
    }
    
    public void updateItemCount(){
        autoInjectorUI.getLblCountOfItems().setText("   Out of "+getDataSetSize()+" items   ");
    }

    public String getInjectorStatus() {
        return injectorStatus;
    }

    public void setInjectorStatus(String injectorStatus) {
        this.injectorStatus = injectorStatus;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }
    
}
