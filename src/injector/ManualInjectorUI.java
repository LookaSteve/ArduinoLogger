package injector;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import Modules.NumericField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.BoxLayout;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.miginfocom.swing.MigLayout;

public class ManualInjectorUI extends JPanel {

    private ManualInjector manualInjector;
    private NumericField autoInjectorCursorPositionField;
    private JProgressBar autoInjectorProgressBar;
    private JLabel lblStatusAutoInjector;
    private JButton btnReset;
    private JLabel lblCountOfItems;
    
    public ManualInjectorUI(ManualInjector manualInjector) {
        
        this.manualInjector = manualInjector;
        
        setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);
        panel.setLayout(new BorderLayout(0, 0));
        
        autoInjectorProgressBar = new JProgressBar();
        autoInjectorProgressBar.setString("Idle.");
        autoInjectorProgressBar.setStringPainted(true);
        panel.add(autoInjectorProgressBar, BorderLayout.NORTH);
        
        JScrollPane scrollPane = new JScrollPane();
        add(scrollPane, BorderLayout.CENTER);
        
        JPanel autoInjectorSettingsPanel = new JPanel();
        scrollPane.setViewportView(autoInjectorSettingsPanel);
        GridBagLayout gbl_autoInjectorSettingsPanel = new GridBagLayout();
        gbl_autoInjectorSettingsPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_autoInjectorSettingsPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_autoInjectorSettingsPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_autoInjectorSettingsPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        autoInjectorSettingsPanel.setLayout(gbl_autoInjectorSettingsPanel);
        
        JLabel label = new JLabel("     ");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        autoInjectorSettingsPanel.add(label, gbc_label);
        
        JLabel label_1 = new JLabel("     ");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 5, 0);
        gbc_label_1.gridx = 3;
        gbc_label_1.gridy = 0;
        autoInjectorSettingsPanel.add(label_1, gbc_label_1);
        
        JLabel lblCursorPosition = new JLabel("Cursor position");
        GridBagConstraints gbc_lblCursorPosition = new GridBagConstraints();
        gbc_lblCursorPosition.anchor = GridBagConstraints.WEST;
        gbc_lblCursorPosition.insets = new Insets(0, 0, 5, 5);
        gbc_lblCursorPosition.gridx = 1;
        gbc_lblCursorPosition.gridy = 1;
        autoInjectorSettingsPanel.add(lblCursorPosition, gbc_lblCursorPosition);
        
        JPanel cursorPositionPanel = new JPanel();
        GridBagConstraints gbc_cursorPositionPanel = new GridBagConstraints();
        gbc_cursorPositionPanel.insets = new Insets(0, 0, 5, 5);
        gbc_cursorPositionPanel.fill = GridBagConstraints.BOTH;
        gbc_cursorPositionPanel.gridx = 1;
        gbc_cursorPositionPanel.gridy = 2;
        autoInjectorSettingsPanel.add(cursorPositionPanel, gbc_cursorPositionPanel);
        cursorPositionPanel.setLayout(new BoxLayout(cursorPositionPanel, BoxLayout.X_AXIS));
        
        autoInjectorCursorPositionField = new NumericField();
        cursorPositionPanel.add(autoInjectorCursorPositionField);
        autoInjectorCursorPositionField.setValue(manualInjector.getCursorPosition());
        autoInjectorCursorPositionField.setColumns(10);
        autoInjectorCursorPositionField.getDocument().addDocumentListener(new DocumentListener() {
            
            public void changedUpdate(DocumentEvent e) {
              update();
            }
            public void removeUpdate(DocumentEvent e) {
              update();
            }
            public void insertUpdate(DocumentEvent e) {
              update();
            }

            public void update() {
                
                int value = autoInjectorCursorPositionField.getValue();
               
                System.out.println("EVENT VALUE : " + autoInjectorCursorPositionField.getValue());
                
                //If value is at least 0
                if(value>=0){
                    
                    System.out.println("HIGHER THAN 0");
                    
                    //If the new value is smallet than the OOB of the dataset
                    if(value < manualInjector.getDataSetSize()){
                        //Accepected new value
                        System.out.println("ACCEPTED VALUE " + value);
                        manualInjector.setCursorPosition(value);
                    }else if(manualInjector.getDataSetSize()==0){
                        //In the case that the dataset is null
                        //Set current value to 0
                        System.out.println("NULL LIST SET TO 0");
                        manualInjector.setCursorPosition(0);
                        Runnable doHighlight = new Runnable() {
                            @Override
                            public void run() {
                                autoInjectorCursorPositionField.setValue(0);
                            }
                        };       
                        SwingUtilities.invokeLater(doHighlight);
                    }else{
                        //If we have a value higher than the OOB limit
                        //Set the penultimate value of the index
                        System.out.println("TOO HIGH REPLACE BY MAX " + (manualInjector.getDataSetSize()-1));
                        manualInjector.setCursorPosition(manualInjector.getDataSetSize()-1);
                        Runnable doHighlight = new Runnable() {
                            @Override
                            public void run() {
                                autoInjectorCursorPositionField.setValue(manualInjector.getDataSetSize()-1);
                            }
                        };       
                        SwingUtilities.invokeLater(doHighlight);
                        
                    }
                    
                    manualInjector.updateProgressBar();
                    
                }else{
                    
                    System.out.println("SET TO 0-1");
                    
                    manualInjector.setCursorPosition(0);
                    Runnable doHighlight = new Runnable() {
                        @Override
                        public void run() {
                            autoInjectorCursorPositionField.setValue(1);
                        }
                    };       
                    SwingUtilities.invokeLater(doHighlight);
                }
                
            }
            
          });
        
        lblCountOfItems = new JLabel("   Out of 0 items   ");
        GridBagConstraints gbc_lblCountOfItems = new GridBagConstraints();
        gbc_lblCountOfItems.insets = new Insets(0, 0, 5, 5);
        gbc_lblCountOfItems.gridx = 2;
        gbc_lblCountOfItems.gridy = 2;
        autoInjectorSettingsPanel.add(lblCountOfItems, gbc_lblCountOfItems);
        
        JLabel label_4 = new JLabel("     ");
        GridBagConstraints gbc_label_4 = new GridBagConstraints();
        gbc_label_4.insets = new Insets(0, 0, 5, 5);
        gbc_label_4.gridx = 0;
        gbc_label_4.gridy = 3;
        autoInjectorSettingsPanel.add(label_4, gbc_label_4);
        
        JButton btnStep = new JButton("Step / Inject line");
        btnStep.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                manualInjector.step();
            }
        });
        GridBagConstraints gbc_btnStep = new GridBagConstraints();
        gbc_btnStep.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnStep.insets = new Insets(0, 0, 5, 5);
        gbc_btnStep.gridx = 1;
        gbc_btnStep.gridy = 4;
        autoInjectorSettingsPanel.add(btnStep, gbc_btnStep);
        
        btnReset = new JButton("Reset");
        GridBagConstraints gbc_btnReset = new GridBagConstraints();
        gbc_btnReset.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnReset.insets = new Insets(0, 0, 5, 5);
        gbc_btnReset.gridx = 2;
        gbc_btnReset.gridy = 4;
        autoInjectorSettingsPanel.add(btnReset, gbc_btnReset);
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                manualInjector.reset();
            }
        });
        
        JLabel label_2 = new JLabel("    ");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 5;
        autoInjectorSettingsPanel.add(label_2, gbc_label_2);
        
        lblStatusAutoInjector = new JLabel("Status : Idle.");
        GridBagConstraints gbc_lblStatusAutoInjector = new GridBagConstraints();
        gbc_lblStatusAutoInjector.anchor = GridBagConstraints.WEST;
        gbc_lblStatusAutoInjector.insets = new Insets(0, 0, 5, 5);
        gbc_lblStatusAutoInjector.gridx = 1;
        gbc_lblStatusAutoInjector.gridy = 6;
        autoInjectorSettingsPanel.add(lblStatusAutoInjector, gbc_lblStatusAutoInjector);
        
        JLabel label_3 = new JLabel("    ");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.insets = new Insets(0, 0, 0, 5);
        gbc_label_3.gridx = 0;
        gbc_label_3.gridy = 7;
        autoInjectorSettingsPanel.add(label_3, gbc_label_3);
        
    }

    public ManualInjector getAutoInjector() {
        return manualInjector;
    }

    public void setAutoInjector(ManualInjector manualInjector) {
        this.manualInjector = manualInjector;
    }

    public NumericField getAutoInjectorCursorPositionField() {
        return autoInjectorCursorPositionField;
    }

    public void setAutoInjectorCursorPositionField(NumericField autoInjectorCursorPositionField) {
        this.autoInjectorCursorPositionField = autoInjectorCursorPositionField;
    }

    public JProgressBar getAutoInjectorProgressBar() {
        return autoInjectorProgressBar;
    }

    public void setAutoInjectorProgressBar(JProgressBar autoInjectorProgressBar) {
        this.autoInjectorProgressBar = autoInjectorProgressBar;
    }

    public JLabel getLblStatusAutoInjector() {
        return lblStatusAutoInjector;
    }

    public void setLblStatusAutoInjector(JLabel lblStatusAutoInjector) {
        this.lblStatusAutoInjector = lblStatusAutoInjector;
    }

    public JButton getBtnReset() {
        return btnReset;
    }

    public void setBtnReset(JButton btnReset) {
        this.btnReset = btnReset;
    }

    public JLabel getLblCountOfItems() {
        return lblCountOfItems;
    }

    public void setLblCountOfItems(JLabel lblCountOfItems) {
        this.lblCountOfItems = lblCountOfItems;
    }

}
