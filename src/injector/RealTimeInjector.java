package injector;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import Logger.DataItem;
import Modules.CheckListItem;
import Modules.NumericField;
import Modules.Tools;
import core.Xeon;
import gui.LogManagementUI;

public class RealTimeInjector extends Injector{

    private Thread injectionThread;
    private RealTimeInjectorUI realTimeInjectorUI;
    
    private String injectorStatus = "Idle.";
    
    private boolean isActive = false;
    private boolean isPaused = false;
    private boolean rebakeStartTime = false;
    
    private long startTime;
    private long simulatedTime;
    private long firstTimeStamp;
    
    public RealTimeInjector(Xeon xeon, LogManagementUI ui) {
        super(xeon, ui);
    }

    @Override
    public void onLoad(){
        updateItemCount();
        //Load start time of first item
        loadStartTime();
    }
    
    @Override
    public void onClose(){
        stop();
    }
    
    @Override
    public JPanel onDraw(){
        
        realTimeInjectorUI =  new RealTimeInjectorUI(this);
        return realTimeInjectorUI;
        
    }
    
    public void stop(){
        if(injectionThread != null){
            if(injectionThread.isAlive()){
                injectionThread.stop();
            }
        }
        isActive = false;
        isPaused = false;
        resetCursor();
        writeInjectorStatus("Stopped.");
        realTimeInjectorUI.getAutoInjectorCursorPositionField().setEditable(true);
        realTimeInjectorUI.getBtnReset().setEnabled(true);
    }
    
    public void pause(){
        
        if(isActive){
            
            if(isPaused){
                //Resume
                resume();
            }else{
                //Pause
                isPaused=true;
                realTimeInjectorUI.getAutoInjectorCursorPositionField().setEditable(true);
                realTimeInjectorUI.getBtnReset().setEnabled(true);
                writeInjectorStatus("Paused.");
            }
            
        }
        
    }
    
    private void resume(){
        rebakeTime();
        isPaused = false;
        realTimeInjectorUI.getAutoInjectorCursorPositionField().setEditable(false);
        realTimeInjectorUI.getBtnReset().setEnabled(false);
        writeInjectorStatus("Injecting...");
    }
    
    public void reset(){
        if(!isActive || isPaused){
            setCursorPosition(0);
            updateCursorField();
            updateProgressBar();
            writeInjectorStatus("Reset.");
        }
    }
    
    public void start(){
        
        //Check if we are already started
        if(isActive){
            
            //If we want to resume redirect
            if(isPaused){
                
                resume();
                
            }
            
            //Then return because wa are already started
            return;
        }
        
        //Set correct state on all booleans
        isActive = true;
        isPaused = false;
        
        //Start message
        writeInjectorStatus("Injecting...");
        
        //Disable edition field
        realTimeInjectorUI.getAutoInjectorCursorPositionField().setEditable(false);
        //Disable reset button
        realTimeInjectorUI.getBtnReset().setEnabled(false);
        
        injectionThread = new Thread(){
        
            public void run(){
              
                if(rebakeStartTime){
                    rebakeStartTime=false;
                    rebakeTime();
                }else{
                    //Record startTime
                    startTime = new Date().getTime();
                }
                
                
                //To call a graphic refresh each 100 cycles
                int graphicalRefreshCounter = 0;
                
                while(isCursorValid()){
                    
                    //Check for pause
                    while(isPaused){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    
                    //Get new simulated time
                    simulatedTime = new Date().getTime() - startTime;
                    
                    //If a graphical refresh is due
                    if(graphicalRefreshCounter >= 100){
                        updateSimulatedTimeField();
                        graphicalRefreshCounter=0;
                    }else{
                        graphicalRefreshCounter++;
                    }
                    
                    if(isCursorValid()){
                        //Get data item associated to current index
                        DataItem item = getUi().getDataSet().getDataLines().get(getCursorPosition());
                        
                        //Get timeStamp of the item
                        long itemTimeStamp = item.getTimeMilliseconds();
                        
                        //If timestamp is valid (positive)
                        if(itemTimeStamp>=0){

                            //Reprocess timestamp to have simulated delta
                            itemTimeStamp = (itemTimeStamp-firstTimeStamp);
                            
                            //If item time stamp delta above simulated time
                            if(simulatedTime >= itemTimeStamp){
                                //Go for injection
                                injectLine();
                            }
                            
                        }else{  
                            //If the timestamp was invalid
                            //Skip line
                            skipLine(item);
                        }
                    }
                    
                }
                
                //Release field
                realTimeInjectorUI.getAutoInjectorCursorPositionField().setEditable(true);
                realTimeInjectorUI.getBtnReset().setEnabled(true);
                
                //Release booleans
                isActive = false;
                isPaused = false;
                
                //We are done.
                writeInjectorStatus("Injection complete.");
                
            }
        };

        injectionThread.start();
        
    }
    
    private void rebakeTime(){
        startTime = new Date().getTime()-simulatedTime;
    }
    
    private void loadStartTime(){
        for(int i = 0; i < getUi().getDataSet().getDataLines().size(); i++){
            DataItem item = getUi().getDataSet().getDataLines().get(i);
            if(item.getTimeMilliseconds()>=0){
                firstTimeStamp=item.getTimeMilliseconds();
                return;
            }
        }
    }
    
    private long timeUntilNextInjection(){
        //If current cursor and future cursor is valid
        if(isCursorValid() && (getCursorPosition()+1)<getUi().getDataSet().getDataLines().size()){
            //Look for the next item with a valid time definition
            for(int i = getCursorPosition()+1; i < getUi().getDataSet().getDataLines().size(); i++){
                DataItem item = getUi().getDataSet().getDataLines().get(i);
                //If has a valid time definition
                if(item.getTimeMilliseconds()>=0){
                    //Calculate timestamp relative to t0
                    long deltaTimeStamp = item.getTimeMilliseconds()-firstTimeStamp;
                    //Returns time between future timestamp and curent simulated time
                    return deltaTimeStamp-simulatedTime;
                }
            }
        }
        return 0;
    }
    
    public void setSimulatedTimeToNextValidTime(){
        //If current cursor and future cursor is valid
        if(isCursorValid() && (getCursorPosition()+1)<getUi().getDataSet().getDataLines().size()){
            //Look for the next item with a valid time definition
            for(int i = (getCursorPosition()+1); i < getUi().getDataSet().getDataLines().size(); i++){
                DataItem item = getUi().getDataSet().getDataLines().get(i);
                //If has a valid time definition
                if(item.getTimeMilliseconds()>=0){
                    //Set simulated time the timestamp relative to t0
                    simulatedTime = item.getTimeMilliseconds()-firstTimeStamp;
                    //Order rebake
                    rebakeStartTime = true;
                    return;
                }
            }
        }
        //Case for OOB values
        if(isCursorValid() && (getCursorPosition()+1)==getUi().getDataSet().getDataLines().size()){
          //Look for the next item with a valid time definition
            for(int i = (getCursorPosition()); i < getUi().getDataSet().getDataLines().size(); i++){
                DataItem item = getUi().getDataSet().getDataLines().get(i);
                //If has a valid time definition
                if(item.getTimeMilliseconds()>=0){
                    //Set simulated time the timestamp relative to t0
                    simulatedTime = item.getTimeMilliseconds()-firstTimeStamp;
                    //Order rebake
                    rebakeStartTime = true;
                    return;
                }
            }
        }
        //Case for invalid values
        simulatedTime=0;
        //Order rebake
        rebakeStartTime = true;
    }
    
    public void goBackInTime(int seconds){
        
        //If we are OOB
        if(!isCursorValid()&&getCursorPosition()>0){
            //Go back to last valid position
            setCursorPosition(getUi().getDataSet().getDataLines().size()-1);
        }
        
        //Parse all valid timed data items
        for(int i = getCursorPosition(); i>=0; i--){
           DataItem dataItem = getUi().getDataSet().getDataLines().get(i);
           if(dataItem.getTimeMilliseconds()>=0){
               //Reprocess timestamp to have simulated delta
               long itemTimeStamp = (dataItem.getTimeMilliseconds()-firstTimeStamp);
               //If back in time smaller than future item
               if((simulatedTime-(seconds*1000)) >= itemTimeStamp){
                   //Set as current cursor and go back in time
                   startTime+=(seconds*1000);
                   setCursorPosition(i);
                   //If we are paused or inactive we must refresh the UI
                   if(!isActive()||isPaused){
                       updateSimulatedTimeField();
                       updateCursorField();
                   }
                   return;
               }
               
           }
        }
        
        //If we hit the minimum
        //Set cursor to 0
        setCursorPosition(0);
        //We start over
        startTime = new Date().getTime();
        
    }
    
    public void setTimeFromCurrentIndex(){
        setSimulatedTimeToNextValidTime();
        updateSimulatedTimeField();
    }
    
    private void skipLine(DataItem dataItem){
        
        System.out.println("Skipped line, invalid time: '"+dataItem.getTime()+"'");
        incrementCursor();
        
    }
    
    private void injectLine(){
        
        String dataString = "";
        
        for(int j = 0; j < getUi().getDataSet().getDataLines().get(getCursorPosition()).safeGetDataListSize(); j ++){
            
            if(((CheckListItem) getUi().getListAvailableInjectionModel().getElementAt(j)).isSelected()){
            
                dataString = dataString + getUi().getDataSet().getDataLines().get(getCursorPosition()).safeGetDataList(j);
                
            }
            
            dataString = dataString + getXeon().getDataSeparator();
            
        }
        
        //Inject data
        getXeon().getDataProcessor().processData(dataString,getXeon().getDataProcessor().getLocalSource());
        
        incrementCursor();
        
        updateCursorField();
        
        updateProgressBar();
        
    }
    
    public void resetCursor(){
        setCursorPosition(0);
        updateCursorField();
    }
    
    public void writeInjectorStatus(String status){
        injectorStatus = status;
        realTimeInjectorUI.getLblStatusAutoInjector().setText("Status : " + injectorStatus);
    }
    
    public void updateProgressBar(){
        realTimeInjectorUI.getAutoInjectorProgressBar().setMaximum(getDataSetSize());
        realTimeInjectorUI.getAutoInjectorProgressBar().setValue(getCursorPosition());
        realTimeInjectorUI.getAutoInjectorProgressBar().setString(getCursorPosition()+"/"+getDataSetSize());
    }
    
    public void updateSimulatedTimeField(){
        realTimeInjectorUI.getLblReplayedTime().setText("Replayed Time : " + Tools.quickTimeFormat(simulatedTime));
        realTimeInjectorUI.getLblNextTime().setText("Until next Line  : " + Tools.quickTimeFormat(timeUntilNextInjection()));
    }
    
    public void updateCursorField(){
        realTimeInjectorUI.getAutoInjectorCursorPositionField().setValue(getCursorPosition());
    }
    
    public void updateItemCount(){
        realTimeInjectorUI.getLblCountOfItems().setText("   Out of "+getDataSetSize()+" items   ");
    }

    public String getInjectorStatus() {
        return injectorStatus;
    }

    public void setInjectorStatus(String injectorStatus) {
        this.injectorStatus = injectorStatus;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }
    
}
