package injector;

import javax.swing.JPanel;

import core.Xeon;
import gui.LogManagementUI;

public abstract class Injector {

    private Xeon xeon;
    private LogManagementUI ui;
    private int cursorPosition = 0;
    
    public Injector(Xeon xeon,LogManagementUI ui){
        
        this.xeon = xeon;
        this.ui = ui;
        
    }
    
    public void onLoad(){
        //To override
    }
    
    public void onClose(){
        //To override
    }
    
    public JPanel onDraw(){
        return null; //To override
    }
    
    public boolean isCursorValid(){
        //Check if current position of cursor holds data
        if(ui == null){return false;}
        if(ui.getDataSet() == null){return false;}
        if(cursorPosition < ui.getDataSet().getDataLines().size()){
            return true;
        }
        return false;
        
    }
    
    public void incrementCursor(){
        cursorPosition++;
    }

    public int getDataSetSize(){
        if(ui == null){return 0;}
        if(ui.getDataSet() == null){return 0;}
        return ui.getDataSet().getDataLines().size();
    }
    
    public LogManagementUI getUi() {
        return ui;
    }

    public void setUi(LogManagementUI ui) {
        this.ui = ui;
    }

    public int getCursorPosition() {
        return cursorPosition;
    }

    public void setCursorPosition(int cursorPosition) {
        this.cursorPosition = cursorPosition;
    }

    public Xeon getXeon() {
        return xeon;
    }

    public void setXeon(Xeon xeon) {
        this.xeon = xeon;
    }
    
}
