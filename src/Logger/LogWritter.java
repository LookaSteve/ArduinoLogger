package Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import javax.swing.filechooser.FileSystemView;

import Modules.Tools;
import core.Xeon;

public class LogWritter {
	
	private Xeon xeon;
	
	private File file;
	private FileWriter savefile;
	private Boolean isLogReady = false;
	
	public LogWritter(Xeon xeon){
		
		this.xeon = xeon;
		
	}
	
	public void open(){
		
		//Check logpath
		if(xeon.getLogPath() == null){
			
			return;
			
		}
		
		if(!xeon.getLogPath().exists()){
			
			return;
			
		}
		
		if(!xeon.getLogPath().isDirectory()){
			
			return;
			
		}
		
		//Init file path
		file = new File(xeon.getLogPath() + "//" + xeon.getLogHeader() + "_" + Tools.getOrderedTime() + ".log");
		
		//If file does not exist create new
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		
		//Open file
		try {
			savefile = new FileWriter(file,true);
			isLogReady = true;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
	}
	
	public void write(String data){
		
		try {
			//Write time of reception, the data then adds a new line
			savefile.write(Tools.getFormatedTime() + "	" + data + System.getProperty("line.separator"));
			savefile.close();
			savefile = new FileWriter(file,true);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
	}

	public void close(){
		
		//Closes the file
		if(isLogReady){
		
			isLogReady = false;
			
			try {
				savefile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public FileWriter getSavefile() {
		return savefile;
	}

	public void setSavefile(FileWriter savefile) {
		this.savefile = savefile;
	}

	public Boolean getIsLogReady() {
		return isLogReady;
	}

	public void setIsLogReady(Boolean isLogReady) {
		this.isLogReady = isLogReady;
	}
	
}
