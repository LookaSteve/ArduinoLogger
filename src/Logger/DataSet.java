package Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import core.Xeon;

public class DataSet {
	
	private Xeon xeon;

	private File file;
	private Date logDate;
	private LogParser logParser;

	private ArrayList<String> headers = new ArrayList<String>();
	private ArrayList<DataItem> dataLines = new ArrayList<DataItem>();

	public DataSet(Xeon xeon){
		
		this.xeon = xeon;
		this.logParser = new LogParser(xeon,this);
		
	}
	
	public boolean promptOpenFile(String title){
		
	    JFileChooser fileChooser = new JFileChooser();
	    
	    //If no specified log path
	    if(xeon.getLogPath().getAbsolutePath().equals(xeon.getPlaceholderPath().getAbsolutePath())){
	        //Load default file management path
	        fileChooser.setCurrentDirectory(xeon.getDefaultDataFileManagementPath());
	    }else{
	        fileChooser.setCurrentDirectory(xeon.getLogPath());
	    }
		
		fileChooser.setDialogTitle(title);
		fileChooser.setFileFilter(new FileNameExtensionFilter("Data Files ('.log', '.ems', '.csv', '.txt')", "log","ems","csv","txt"));
		int result = fileChooser.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION){
		    file = fileChooser.getSelectedFile();
		    
		    //If no specified log path
		    if(xeon.getLogPath().getAbsolutePath().equals(xeon.getPlaceholderPath().getAbsolutePath())){
	            //Save new selected path
		        xeon.setDefaultDataFileManagementPath(fileChooser.getCurrentDirectory());
	            xeon.getDefaultConfigManager().saveConfiguration();
	        }
		    
		    return true;
		}
		return false;
		
	}
	
	public void loadFile(){
	    
	    //Reset dataset
	    reset();
	    logParser.resetLogParser();

	    //Reparse
	    logParser.parse();
        //logParser.printLog();
        
	}
	
	public void reset(){
	    dataLines.clear();
	    headers.clear();
        logDate=null;
	}

	public ArrayList<DataItem> getDataLines() {
		return dataLines;
	}

	public void setDataLines(ArrayList<DataItem> dataLines) {
		this.dataLines = dataLines;
	}

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public LogParser getLogParser() {
        return logParser;
    }

    public void setLogParser(LogParser logParser) {
        this.logParser = logParser;
    }

    public ArrayList<String> getHeaders() {
        return headers;
    }

    public void setHeaders(ArrayList<String> headers) {
        this.headers = headers;
    }
}
