package Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import Modules.Tools;
import core.Xeon;

public class DataItem {
	
	private Xeon xeon;
	
	private ArrayList<String> dataList = new ArrayList<String>();
	
	private String rawData = "";
	
	private String time;
	
	public DataItem(Xeon xeon,String time, String dataStr){
		
		this.xeon=xeon;
		this.time=time;
		
		if(dataStr != null){
			
			if(dataStr.length() > 0){
			    
			    rawData = dataStr;
				
			    //Only splits separators not contained in quoted text
				String [] splitedStr = dataStr.split(xeon.getDataSeparator()+"(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
				
				dataList = new ArrayList<String>(Arrays.asList(postProcessItems(splitedStr)));
				
			}
			
		}
		
	}
	
	public DataItem(Xeon xeon,String time, String dataStr, String separator){
        
        this.xeon=xeon;
        this.time=time;
        
        if(dataStr != null){
            
            if(dataStr.length() > 0){
                
                rawData = dataStr;
                
                //Only splits separators not contained in quoted text
                String [] splitedStr = dataStr.split(separator+"(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                
                dataList = new ArrayList<String>(Arrays.asList(postProcessItems(splitedStr)));
                
            }
            
        }
        
    }
	
	private String[] postProcessItems(String[] items){
	    
	    for(int i = 0; i < items.length; i++){
	        
	        //Check for double quote wrapped item
	        if(items[i].startsWith("\"") && items[i].endsWith("\"") && items[i].length()>2){
	            
	            //Cut first and last caracters
	            items[i] = items[i].substring(1, items[i].length()-1);
	            
	            continue;
	        }
	        
	        //Check for single quote wrapped item
            if(items[i].startsWith("\'") && items[i].endsWith("\'") && items[i].length()>2){
                
                //Cut first and last caracters
                items[i] = items[i].substring(1, items[i].length()-1);
                
                continue;
            }
	        
	    }
	    
	    return items;
	    
	}
	
	public int safeGetDataListSize(){
		
		if(dataList == null)return 0;
		
		return dataList.size();
		
	}
	
	public String safeGetDataList(int index){
		
		if(dataList == null)return "";
		if(dataList.size()==0)return "";
		
		if(index < dataList.size()){
			
			return dataList.get(index);
			
		}
		
		return "";
		
	}
	
	public long getTimeMilliseconds(){
        
        long itemTimeStamp = -1;
        
        //If time definition isn't null
        if(time!=null){
            //If time definition isn't empty
            if(time.length()>0){
                //Parse EMS timestamp
                if(Tools.isInt(time)){ //Is int also works with longs
                    //Get EMS time value as a long
                    itemTimeStamp = Tools.cleanInputLong(time);
                }else{
                    //Get LOG time value as an array
                    int[] timeArray = Tools.parseTime(time);
                    //Get date from time array
                    Date timeStampTimeArray = Tools.timeArrayToDate(timeArray);
                    //Check validity of time array
                    if(timeStampTimeArray!=null){
                        //Finally get timestamp
                        itemTimeStamp = timeStampTimeArray.getTime();
                    }
                }
            }
        }
        return itemTimeStamp;
    }

	public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	

}
