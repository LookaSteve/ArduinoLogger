package Logger;

import java.io.File;

import javax.swing.JFileChooser;

import core.Xeon;

public class LogManager {

	private Xeon xeon;
	
	private LogWritter logWritter;
	
	private Boolean doLog = false;
	
	public LogManager(Xeon xeon){
		
		this.xeon = xeon;
		
		logWritter = new LogWritter(xeon);
		
	}
	
	public void log(String data){
		
		if(doLog){
			
			if(!logWritter.getIsLogReady()){
				logWritter.open();	
			}
			
			logWritter.write(data);
			
		}
		
		
	}
	
	public void changeLogPath(){
		
		JFileChooser chooser = new JFileChooser(); 
		
		chooser.setCurrentDirectory(xeon.getLogPath());
	    chooser.setDialogTitle("Choose log path");
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    chooser.setAcceptAllFileFilterUsed(false);
	    
	    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
	    	
	    	File f = chooser.getSelectedFile();
	    	
	    	if(f.exists() && f.isDirectory()){
	    		
	    		xeon.setLogPath(f);
	    		
	    		if(xeon.getMainWindow().getConfigSettings() != null){
	    			
	    			if(xeon.getMainWindow().getConfigSettings().isVisible()){
	    				
	    				xeon.getMainWindow().getConfigSettings().getLogPath().setText(f.toString());
	    				
	    			}
	    			
	    		}
	    		
	    		xeon.action();
	    		
	    	}
        }
		
	}

	public LogWritter getLogWritter() {
		return logWritter;
	}

	public void setLogWritter(LogWritter logWritter) {
		this.logWritter = logWritter;
	}

	public Boolean getDoLog() {
		return doLog;
	}

	public void setDoLog(Boolean doLog) {
		this.doLog = doLog;
	}
	
	
	
}
