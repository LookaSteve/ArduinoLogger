package Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Modules.Tools;
import core.Xeon;
import gui.LogManagementUI;

public class LogParser {

    public static final int FILETYPE_LOG = 0;
    public static final int FILETYPE_EMS = 1;
    public static final int FILETYPE_CSV = 2;
    public static String[] fileTypes = new String[] {"DenebRC LOG file", "EMS file", "CSV file"};
    
    private Xeon xeon;
    private DataSet dataSet;
    
    //Statuses and logs
    private String dateCollectionStatus = "Not parsed.";
    private String parsingReturnStatus = "Not parsed.";
    private ArrayList<String> parsingLog = new ArrayList<>();
    
    //File type detection flags
    private int detectedFileType = -1;
    private boolean skipAutoDetectFileType = false;
    private String detectedSeparator = "&";
    
    //More options
    private boolean ignoreEmptyTimes = false;
    
    private ArrayList<String> loadedFile = new ArrayList<>();
    
    public LogParser(Xeon xeon,DataSet dataSet){
        
        this.xeon = xeon;
        this.dataSet = dataSet;
        
    }
    
    //Parses the selected file of the dataset
    public void parse(){
        
        String returnStatus = checkAndStart();
        logParse("");
        logParse(returnStatus);
        parsingReturnStatus = returnStatus;
        
    }
    
    //Runs quick checks, extracts the date of creation of the file and starts the parsing
    private String checkAndStart(){
        
        String errorMessage = "Parsing ended with a failure.";
        
        //Intro
        logParse("Log Parser started.");
        logParse("-------------------");
        logParse("");
        logParse("Quick checks:");
        
        //Quick checks
        //Check for null dataset definition
        if(dataSet==null){
            logParse("\t[ERROR] Null dataset.");
            return errorMessage;
        }else{
            logParse("\t[OK] Dataset ready.");
        }
        //Check for null file definition
        if(dataSet.getFile()==null){
            logParse("\t[ERROR] Null file.");
            return errorMessage;
        }else{
            logParse("\t[OK] File defined.");
        }
        //Check for file existence
        if(dataSet.getFile().exists()){
            logParse("\t[OK] File exists.");
        }else{
            logParse("\t[ERROR] File not found: '" + dataSet.getFile().getAbsolutePath() + "'");
            return errorMessage;
        }
        //Check for directory
        if(dataSet.getFile().isDirectory()){
            logParse("\t[ERROR] File is a directory: '" + dataSet.getFile().getAbsolutePath() + "'");
            return errorMessage;
        }else{
            logParse("\t[OK] File is a file.");
        }
        //Check for file readability
        if(dataSet.getFile().canRead()){
            logParse("\t[OK] File is readable.");
        }else{
            logParse("\t[ERROR] File unreadable: '" + dataSet.getFile().getAbsolutePath() + "'");
            return errorMessage;
        }
        
        logParse("Checks complete.");
        logParse("");
        logParse("Extracting date from file name.");
        
        extractDate();
        
        logParse(dateCollectionStatus);
        logParse("");
        logParse("Reading file.");
        
        //Load file, returns error if something went wrong
        if(!loadFile()){
            return errorMessage;
        }
        
        logParse("File read.");
        logParse("");
        
        //If the autodetection isn't skipped run the autodetection routine
        if(!skipAutoDetectFileType){
            logParse("Autodetecting file type.");
            
            autoDetectFileType();
            
            if(detectedFileType!=-1){
                logParse("File type autodetected as " + fileTypes[detectedFileType] + ".");
            }else{
                logParse("Could not autodetect file type.");
            }
            logParse("");
        }
        
        logParse("Autodetecting file separator.");
        if(autoDetectSeparator()){
            logParse("Separator autodetected as " + detectedSeparator + ".");
            logParse("");
        }else{
            //Fallback to config separator
            detectedSeparator = xeon.getDataSeparator();
            logParse("Could not detect a separator, falling back to the current configuration's separator.");
            logParse("");
        }
        
        logParse("Parsing file.");
        
        String endResult = cacheData();
        
        logParse("Log Parsed.");
        
        return endResult;
        
    }
    
    //Tries to extract the date of the start of the log
    private void extractDate(){
        
        //Collect date from file
        
        //Check for null dataset definition
        if(dataSet==null){return;}
        //Check for null file definition
        if(dataSet.getFile()==null){return;}
        
        //Is the file a LOG compatible file ?
        String filename = dataSet.getFile().getName();
        //Track success of extraction
        boolean gotDate = false;
        //Remove file extension
        filename = filename.substring(0, filename.lastIndexOf('.'));
        //Contains log title_date separator
        if(filename.contains("_")){
            //Split by _
            String[] splitDate = filename.split("_");
            //Check length
            if(splitDate.length>=2){
                //Extract date (Last index selection enables multi _ title)
                String dateExtr = splitDate[splitDate.length-1];
                //Check date contains day-time separator
                if(dateExtr.contains("-")){
                    //Split by -
                    String[] splitDay = dateExtr.split("-");
                    //Check length
                    if(splitDay.length>=2){
                        //Extract date
                        String dayExtr = splitDay[0];
                        //String data extraction with hell's regex
                        String[] dateData = dayExtr.split("[^A-Z0-9]+|(?<=[A-Z])(?=[0-9])|(?<=[0-9])(?=[A-Z])");
                        //Check format of date
                        if(dateData.length >= 3){
                            //Check that the format is int-String-int
                            if(Tools.isInt(dateData[0]) && !Tools.isInt(dateData[1]) && Tools.isInt(dateData[2])){
                                
                                //Extraction
                                int year = Integer.parseInt(dateData[0]);
                                int month = Tools.reverseMonth(dateData[1]);
                                int day = Integer.parseInt(dateData[2]);
                                
                                //Convert to date
                                Calendar c = Calendar.getInstance();
                                c.set(year, month, day, 0, 0);  
                                //Save in parent dataset
                                dataSet.setLogDate(c.getTime());

                                logParse("\tExtracted: " + dataSet.getLogDate().toString());
                                
                                gotDate = true;
                                
                                dateCollectionStatus = "Date parsed.";
                                
                            }else{
                                dateCollectionStatus = "PARSING ERROR 06: Time format is not in the standard Int,String,Int configuration.";
                            }
                        }else{
                            dateCollectionStatus = "PARSING ERROR 05: Time format wasn incorrect.";
                        }
                    }else{
                        dateCollectionStatus = "PARSING ERROR 04: Time definition wasn't found.";
                    }
                }else{
                    dateCollectionStatus = "PARSING ERROR 03: Date could not be separated from Time, missing hyphen.";
                }
            }else{
                dateCollectionStatus = "PARSING ERROR 02: File title did not have a date definition.";
            }
        }else{
            dateCollectionStatus = "File title did not contain an underscore, assuming there is no time definition.";
        }
        //If no valid date found inside
        if(!gotDate){
            try {
                //Last chance, extract creation date of the file
                BasicFileAttributes attr = Files.readAttributes(dataSet.getFile().toPath(), BasicFileAttributes.class);
                dataSet.setLogDate(new Date(attr.creationTime().toMillis()));
                logParse("  Extracted: " + dataSet.getLogDate().toString());
                dateCollectionStatus = "Date extracted from file attributes.";
            } catch (IOException e) {
                e.printStackTrace();
                //If something went wrong, fuck it, use the 1970 date.
                dataSet.setLogDate(new Date());
                dateCollectionStatus = "PARSING ERROR 01: File attribute could not be read.";
            }
        }else{
            dateCollectionStatus = "Start date of this log parsed with success.";
        }
        
        //Adding tab to be compliant
        dateCollectionStatus = "\t" + dateCollectionStatus;
        
    }
    
    //Loads the file selected from the dataset
    private boolean loadFile(){
        
        //Ensure empty list
        loadedFile.clear();
        
        //Real file
        try (BufferedReader br = new BufferedReader(new FileReader(dataSet.getFile()))) {
            String line;
            
            int lineCount = 0;
            
            //Read the entire file
            while ((line = br.readLine()) != null) {

                //Skip empty lines
                if(line.length() > 0) {
                
                    //Save line
                    loadedFile.add(line);
                    
                }
                
                lineCount++;
                
            }
            
            logParse("\tRead " + lineCount + " lines.");
            
            //Return true if everything went right
            return true;
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            logParse("\t[ERROR] File not found exception.");
        } catch (IOException e) {
            e.printStackTrace();
            logParse("\t[ERROR] Unknown IO Exception.");
        }
        
        return false;
        
    }
    
    //Tries to detect the file type
    private void autoDetectFileType(){
        
        //Easy case, file type detection
        switch(Tools.getFileExtension(dataSet.getFile()).toUpperCase()){
            case ".LOG" : detectedFileType = FILETYPE_LOG; return;
            case ".EMS" : detectedFileType = FILETYPE_EMS; return;
            case ".CSV" : detectedFileType = FILETYPE_CSV; return;
        }
        
    }
    
    private boolean autoDetectSeparator(){
        
        //Run different separator method depending on the file type
        switch(detectedFileType){
            case FILETYPE_CSV: return autoDetectCSVSeparator();
            case FILETYPE_EMS: return autoDetectNativeSeparator();
            case FILETYPE_LOG: return autoDetectNativeSeparator();
        }
        
        return false;
        
    }
    
    //Autodetects the separator used on native EMS and LOG files
    private boolean autoDetectNativeSeparator(){
        
        //Hashmap of candidates and the ammount of time they where found in the data
        Map<Character, Integer> candidatesSeparator = new HashMap<Character, Integer>();
        
        //Parsing the data
        for(int i = 0; i < loadedFile.size(); i++){
            
            //Non empty string
            if(loadedFile.get(i).length()>0){
                
                //Get trailing char, this is our candidate
                char candidate = loadedFile.get(i).charAt(loadedFile.get(i).length()-1);
                
                //If candidate already spotted
                if(candidatesSeparator.get(candidate)!=null){
                    //Increment value
                    candidatesSeparator.put(candidate, candidatesSeparator.get(candidate) + 1);
                }else{
                    //Create candidate object
                    candidatesSeparator.put(candidate, 1);
                }
                
            }
            
        }
        
        Character maxCandidate = null;
        int maxCandidateValue = 0;
        for (Map.Entry<Character, Integer> entry : candidatesSeparator.entrySet()) {
          if (entry.getValue() > maxCandidateValue) {
            maxCandidate = entry.getKey();
            maxCandidateValue = entry.getValue();
          }
        }
        
        //No null results or else fails
        if(maxCandidate == null){
            return false;
        }
        
        //The candidate has been found
        detectedSeparator = maxCandidate + "";
        
        return true;
    }
    
    //Autonomously finds the data separator in a CSV file
    private boolean autoDetectCSVSeparator() {
        
        char[] supportedSeparators = {',',';','&','|','-','_'};
        
        //If we have some data
        if(loadedFile.size()>0){
            
            //Try as much as possible to find the separator
            for(int i = 0; i < loadedFile.size(); i++){
                
                //To flag when we are in a string
                char isInStringMarker = ' ';
                
                //To flag next cycle should have a separator
                boolean isOutOfString = true;
                
                //To isolate the best separator candidate
                char separatorCandidate = ' ';
                
                //Parse every caracter of the line
                for(int j = 0; j < loadedFile.get(i).length(); j++){
                    
                    //Get character
                    char currentCaracter = loadedFile.get(i).charAt(j);
                    
                    //Detect string start or end marker for the " char
                    if(currentCaracter == '"'){
                        
                        //If the is in string marker is signaling a end of String
                        if(isInStringMarker == currentCaracter){
                            //We got out of a string
                            isOutOfString = true;
                            isInStringMarker = ' ';
                        }
                        
                        //If the is in string marker is empty
                        if(isInStringMarker == ' '){
                            //We've just entered a string
                            isOutOfString = false;
                            isInStringMarker = '"';
                        }
                        
                    }
                    
                    //Detect string start or end marker for the ' char
                    if(currentCaracter == '\''){
                        
                        //If the is in string marker is signaling a end of String
                        if(isInStringMarker == currentCaracter){
                            //We got out of a string
                            isOutOfString = true;
                            isInStringMarker = ' ';
                        }
                        
                        //If the is in string marker is empty
                        if(isInStringMarker == ' '){
                            //We've just entered a string
                            isOutOfString = false;
                            isInStringMarker = '\'';
                        }
                        
                    }
                    
                    //If we aren't in a string and we've found a matching separator
                    if(isOutOfString && (new String(supportedSeparators).contains(currentCaracter+""))){
                        
                        //Congrats, we have a candidate
                        if(separatorCandidate == ' '){
                            separatorCandidate = currentCaracter;
                        }
                        
                        //If a comma is the current candidate but we have a better candidate
                        if(separatorCandidate == ',' && currentCaracter != ','){
                            //Any other separator is better than the simple coma
                            separatorCandidate = currentCaracter;
                        }
                        
                    }
                    
                }
                
                //If a separator candidate was found
                if(separatorCandidate != ' '){
                    
                    //We have high confidence that it is the right separator
                    detectedSeparator = separatorCandidate+"";
                    
                    return true;
                }
                
            }
            
        }
        
        return false;
            
    }
    
    private boolean isCSVHeader(String line,String separator){
        
        String[] splitHeader = line.split(separator);
        
        //Check for empty line / empty split array
        if(splitHeader.length>0 && line.length()>0){
            
            //Parse line
            for(int i = 0; i < splitHeader.length; i++){
                
                //Remove all potential quotes
                String cleanLine = splitHeader[i].replace("\"", "");
                cleanLine = cleanLine.replace("\'", "");
                
                //If contains a value
                if(Tools.isDouble(cleanLine)){
                    
                    //Not an array
                    return false;
                    
                }
                
            }
            
        }
        
        return true;
    }
    
    //Parses the file and caches the data
    private String cacheData(){
        
        //Import data
        
        //Check for null dataset definition
        if(dataSet==null){
            logParse("\t[ERROR] Null dataset.");
            return "Parsing ended with errors.";
        }
        //Check for null file definition
        if(dataSet.getFile()==null){
            logParse("\t[ERROR] No dataset file definition.");
            return "Parsing ended with errors.";
        }
        
        //List of logs definition
        ArrayList<DataItem> listOfLogs = dataSet.getDataLines();
        
        //Parsing monitoring
        int dataPointCount = 0;
        boolean readAsLOG = false;
        boolean readAsEMS = false;
        boolean readAsCSV = false;
        boolean hadWarning = false;
        boolean hasHeader = false;
        
        //Parse loaded file
        for(int i = 0; i < loadedFile.size(); i++){
            
            String line = loadedFile.get(i);
            
            //If a header hasn't been found yet
            if(!hasHeader){
                //If we are parsing a CSV
                if(detectedFileType == FILETYPE_CSV){
                  //If current line is detected as a header
                    if(isCSVHeader(line,detectedSeparator)){
                        hasHeader = true;
                        logParse("\tFound CSV header, parsing.");
                        String[] splitHeader = line.split(detectedSeparator);
                        for(int j = 0; j < splitHeader.length; j++){
                            dataSet.getHeaders().add(splitHeader[j]);
                        }
                        logParse("\t" + splitHeader.length + " columns parsed.");
                        continue;
                    }
                }
                //If we are parsing a LOG 3.0 compatible file
                if(detectedFileType == FILETYPE_LOG){
                    //If contains log 3.0 header flag
                    if(line.startsWith("LOG\t")){
                        hasHeader = true;
                        logParse("\tFound LOG 3.0 header, parsing.");
                        String cleanHeader = line.replace("LOG\t", "");
                        String[] splitHeader = cleanHeader.split(detectedSeparator);
                        for(int j = 0; j < splitHeader.length; j++){
                            dataSet.getHeaders().add(splitHeader[j]);
                        }
                        logParse("\t" + splitHeader.length + " columns parsed.");
                        continue;
                    }
                }
                //If we are parsing an EMS 2.0 compatible file
                if(detectedFileType == FILETYPE_EMS){
                    //If contains log 3.0 header flag
                    if(line.startsWith("EMS\t")){
                        hasHeader = true;
                        logParse("\tFound EMS 2.0 header, parsing.");
                        String cleanHeader = line.replace("EMS\t", "");
                        String[] splitHeader = cleanHeader.split(detectedSeparator);
                        for(int j = 0; j < splitHeader.length; j++){
                            dataSet.getHeaders().add(splitHeader[j]);
                        }
                        logParse("\t" + splitHeader.length + " columns parsed.");
                        continue;
                    }
                }
                
            }
            
            //Official log file
            if(line.contains("\t") && detectedFileType == FILETYPE_LOG){
                
                readAsLOG = true;
                
                String [] splittedData = line.split("\t",2);
                
                if(splittedData.length == 2){
                    
                    DataItem di = new DataItem(xeon, splittedData[0], splittedData[1], detectedSeparator);
                    
                    listOfLogs.add(di);
                    
                }
                
            }else if(line.contains("  ") && detectedFileType == FILETYPE_EMS){
                
                //EMS File
                
                readAsEMS = true;
                
                String [] splittedData = line.split("  ",2);
                
                if(splittedData.length == 2){

                    DataItem di = new DataItem(xeon, splittedData[0], splittedData[1], detectedSeparator);
                    
                    listOfLogs.add(di);
                    
                }
                
            }else{
                
                //Standard CSV file
                
                boolean canReadCSVLine = true;
                
                //If we ignore timeless lines and we aren't a CSV file
                if(detectedFileType != FILETYPE_CSV && ignoreEmptyTimes){
                    //Ignore ths line
                    canReadCSVLine = false;
                }
                
                //Always mark that we had a CSV line, even if ignored
                readAsCSV = true;
                
                if(canReadCSVLine){
                    DataItem di = new DataItem(xeon, null, line, detectedSeparator);
                    listOfLogs.add(di);
                }
                
            }
            
            dataPointCount++;
            
        }
    
        //Post parse analysis
        if(readAsLOG && readAsCSV){
            logParse("\t[WARNING] Some lines were read without date, check that your file hasn't been corrupted.");
            logParse("\t[WARNING] Disable 'Use datapoints without datetime' to remove them.");
            hadWarning = true;
        }
        if(readAsEMS && readAsCSV){
            logParse("\t[WARNING] Some lines were read without date, check that your file hasn't been corrupted.");
            logParse("\t[WARNING] Disable 'Use datapoints without datetime' to remove them.");
            hadWarning = true;
        }
        
        logParse("\tParsed " + dataPointCount + " data points.");
        
        if(hadWarning){
            return "Parsing ended with warnings.";
        }
        
        return "Parsing ended with success.";
        
    }
    
    //Resets the statuses, clears the logs and the loaded file
    public void resetLogParser(){
        loadedFile.clear();
        dateCollectionStatus = "Not parsed.";
        parsingReturnStatus = "Not parsed.";
        parsingLog.clear();
    }
    
    //Request a reparsing of the current file with a different file type strategy
    public void requestReparse(LogManagementUI ui,int fileType){
        
        //First we reset the parser
        resetLogParser();
        
        //Then we reset the dataset
        dataSet.reset();
        
        logParse("Requesting re-parsing as " + fileTypes[fileType] + ".");
        logParse("");
        
        detectedFileType = fileType;
        skipAutoDetectFileType = true;
        
        //Parse with the settings
        parse();
        
        //Reload the UI
        ui.fullUILoad();
        
        
    }
    
    //Request to reparse using
    public void requestReparseWithCSVTimeStamp(LogManagementUI ui,int channel){
        
        //No null channels
        if(channel < 0){
            return;
        }
        
        //First we reset the parser
        resetLogParser();
        
        //Then we reset the dataset
        dataSet.reset();
        
        logParse("Requesting re-parsing with timestamp channel (" + channel + ").");
        logParse("");
        
        //Can only be CSV
        detectedFileType = FILETYPE_CSV;
        skipAutoDetectFileType = true;
        
        //Parse with the settings
        parse();
        
        logParse("");
        logParse("Transfering timestamp data to time field.");
        
        //Reparse data
        for(int i = 0; i < dataSet.getDataLines().size(); i++){
            
            //Get data item
            
            DataItem item = dataSet.getDataLines().get(i);
            
            //Check that the channel is present (Could not be)
            if(item.safeGetDataListSize()>channel){
                
                //Transfer the time
                item.setTime(item.safeGetDataList(channel));
                
            }
            
        }
        
        logParse("Timestamp data transfered.");
        
        //Request selection of the channel in the combobox
        ui.setRequestCSVTimeStampComboboxSelection(channel);
        
        //Reload the UI
        ui.fullUILoad();
        
    }
    
    //Quick function to log a string
    public void logParse(String log){
        parsingLog.add(log);
    }
    
    //Prints log to the System out, debug function
    public void printLog(){
        for(int i = 0; i < parsingLog.size(); i++){
            System.out.println(parsingLog.get(i));
        }
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public String getDateCollectionStatus() {
        return dateCollectionStatus;
    }

    public void setDateCollectionStatus(String dateCollectionStatus) {
        this.dateCollectionStatus = dateCollectionStatus;
    }

    public ArrayList<String> getParsingLog() {
        return parsingLog;
    }

    public void setParsingLog(ArrayList<String> parsingLog) {
        this.parsingLog = parsingLog;
    }

    public String getParsingReturnStatus() {
        return parsingReturnStatus;
    }

    public void setParsingReturnStatus(String parsingReturnStatus) {
        this.parsingReturnStatus = parsingReturnStatus;
    }

    public int getDetectedFileType() {
        return detectedFileType;
    }

    public void setDetectedFileType(int detectedFileType) {
        this.detectedFileType = detectedFileType;
    }

    public String getDetectedSeparator() {
        return detectedSeparator;
    }

    public void setDetectedSeparator(String detectedSeparator) {
        this.detectedSeparator = detectedSeparator;
    }

    public boolean isIgnoreEmptyTimes() {
        return ignoreEmptyTimes;
    }

    public void setIgnoreEmptyTimes(boolean ignoreEmptyTimes) {
        this.ignoreEmptyTimes = ignoreEmptyTimes;
    }
    
}
