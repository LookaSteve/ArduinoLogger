package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.NumericField;
import card.Card;
import card.CardManager;
import core.Xeon;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CardManageUI extends JFrame {

    private Xeon xeon;
    private Card card;
    
    private JPanel contentPane;
    private NumericField baudRateField;
    private JTextField cardNameField;

    public CardManageUI(Xeon xeon,Card card) {
        setTitle("Card management");
        
        this.xeon = xeon;
        this.card = card;
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 250);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        JButton btnConnect = new JButton("Apply");
        btnConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                apply();
                
            }
        });
        panel.add(btnConnect);
        
        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                if(apply()){
                    dispose();
                }
                
            }
        });
        panel.add(btnOk);
        
        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                dispose();
                
            }
        });
        panel.add(btnCancel);
        
        JLabel lblConnectToPort = new JLabel("Manage port " + card.getPort());
        lblConnectToPort.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblConnectToPort.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblConnectToPort, BorderLayout.NORTH);
        
        JPanel panel_1 = new JPanel();
        contentPane.add(panel_1, BorderLayout.CENTER);
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);
        
        JLabel lblNewLabel = new JLabel("      ");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 0;
        panel_1.add(lblNewLabel, gbc_lblNewLabel);
        
        JLabel label = new JLabel("      ");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 0);
        gbc_label.gridx = 5;
        gbc_label.gridy = 0;
        panel_1.add(label, gbc_label);
        
        JLabel label_1 = new JLabel("      ");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 5, 5);
        gbc_label_1.gridx = 0;
        gbc_label_1.gridy = 1;
        panel_1.add(label_1, gbc_label_1);
        
        JLabel lblDisplayName = new JLabel("Display name");
        GridBagConstraints gbc_lblDisplayName = new GridBagConstraints();
        gbc_lblDisplayName.anchor = GridBagConstraints.EAST;
        gbc_lblDisplayName.insets = new Insets(0, 0, 5, 5);
        gbc_lblDisplayName.gridx = 2;
        gbc_lblDisplayName.gridy = 2;
        panel_1.add(lblDisplayName, gbc_lblDisplayName);
        
        cardNameField = new JTextField(card.getDisplayName());
        GridBagConstraints gbc_cardNameField = new GridBagConstraints();
        gbc_cardNameField.insets = new Insets(0, 0, 5, 5);
        gbc_cardNameField.fill = GridBagConstraints.HORIZONTAL;
        gbc_cardNameField.gridx = 3;
        gbc_cardNameField.gridy = 2;
        panel_1.add(cardNameField, gbc_cardNameField);
        cardNameField.setColumns(10);
        
        JLabel label_2 = new JLabel("            ");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.insets = new Insets(0, 0, 0, 5);
        gbc_label_2.gridx = 1;
        gbc_label_2.gridy = 3;
        panel_1.add(label_2, gbc_label_2);
        
        JLabel lblBaudRate = new JLabel("Baud rate ");
        GridBagConstraints gbc_lblBaudRate = new GridBagConstraints();
        gbc_lblBaudRate.insets = new Insets(0, 0, 0, 5);
        gbc_lblBaudRate.anchor = GridBagConstraints.EAST;
        gbc_lblBaudRate.gridx = 2;
        gbc_lblBaudRate.gridy = 3;
        panel_1.add(lblBaudRate, gbc_lblBaudRate);
        
        baudRateField = new NumericField();
        baudRateField.setValue(card.getDataRate());
        GridBagConstraints gbc_baudRateField = new GridBagConstraints();
        gbc_baudRateField.insets = new Insets(0, 0, 0, 5);
        gbc_baudRateField.fill = GridBagConstraints.HORIZONTAL;
        gbc_baudRateField.gridx = 3;
        gbc_baudRateField.gridy = 3;
        panel_1.add(baudRateField, gbc_baudRateField);
        baudRateField.setColumns(10);
        
        JLabel label_3 = new JLabel("            ");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.insets = new Insets(0, 0, 0, 5);
        gbc_label_3.gridx = 4;
        gbc_label_3.gridy = 3;
        panel_1.add(label_3, gbc_label_3);
        
        getRootPane().setDefaultButton(btnOk);
        
    }
    
    public boolean apply(){
        
        int selectedBaudRate = baudRateField.getValue();
        if(selectedBaudRate <= 0){
            JOptionPane.showMessageDialog(null,
                    "Baud rate must be positive and not zero.",
                    "Invalid Baud Rate",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }else{
            
            //Boolean for list refresh
            boolean needForListRefresh = false;
            
            //Do we need to change the data rate ?
            if(card.getDataRate() != selectedBaudRate){
                //Change data rate
                if(card.changeDataRate(selectedBaudRate)){
                    needForListRefresh = true;
                }else{
                    JOptionPane.showMessageDialog(null,
                            "Could not change data rate",
                            "Serial Port Error",
                            JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
            
            //Do we need to update the display name ?
            if(!card.getDisplayName().equals(cardNameField.getText())){
                card.setDisplayName(cardNameField.getText());
                needForListRefresh = true;
            }
            
            //If we need to have a list refresh
            if(needForListRefresh){
                xeon.getMainWindow().refreshConnectedCardList();
                xeon.getMainWindow().refreshDetectedCardList();
            }
            
        }
        
        return true;
        
    }

}
