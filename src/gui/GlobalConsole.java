package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

import Modules.Tools;
import card.Card;
import core.Xeon;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JSplitPane;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GlobalConsole extends JFrame {

	private Xeon xeon;
	
	private JPanel contentPane;
	private JTextField inputConsole;
	private JTextArea SourceConsole;
	private JTextArea RemoteConsole;

	public GlobalConsole(final Xeon xeon) {
		
		this.xeon = xeon;
		
		xeon.getDataProcessor().setGlobalConsole(this);
		
		setTitle("Global console");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 679, 453);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		inputConsole = new JTextField();
		inputConsole.setFont(new Font("Monospaced", Font.PLAIN, 20));
		panel.add(inputConsole, BorderLayout.CENTER);
		inputConsole.setColumns(10);
		
		JButton btnSend = new JButton("Send");
		panel.add(btnSend, BorderLayout.EAST);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String command = inputConsole.getText() ;
				SourceConsole.append(command+ "\r\n");
				xeon.getDataProcessor().upPropagate(command,xeon.getDataProcessor().getLocalSource());
				inputConsole.setText("");
				
			}
		});
		
		this.getRootPane().setDefaultButton(btnSend);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.7);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		JPanel panel_5 = new JPanel();
		splitPane.setLeftComponent(panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_5.add(panel_7, BorderLayout.NORTH);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JLabel lblRemoteSource = new JLabel(" Remote source : ");
		panel_7.add(lblRemoteSource, BorderLayout.WEST);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				RemoteConsole.setText("");
				
			}
		});
		panel_7.add(btnClear, BorderLayout.EAST);
		
		JScrollPane scrollPane = new JScrollPane();
		panel_5.add(scrollPane, BorderLayout.CENTER);
		
		RemoteConsole = new JTextArea();
		RemoteConsole.setEditable(false);
		RemoteConsole.setFont(new Font("Monospaced", Font.PLAIN, 20));
		scrollPane.setViewportView(RemoteConsole);
		
		JPanel panel_6 = new JPanel();
		splitPane.setRightComponent(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_8 = new JPanel();
		panel_6.add(panel_8, BorderLayout.NORTH);
		panel_8.setLayout(new BorderLayout(0, 0));
		
		JLabel lblLocalSource = new JLabel(" Local source : ");
		panel_8.add(lblLocalSource, BorderLayout.WEST);
		
		JButton button = new JButton("Clear");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SourceConsole.setText("");
				
			}
		});
		panel_8.add(button, BorderLayout.EAST);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_6.add(scrollPane_1, BorderLayout.CENTER);
		
		SourceConsole = new JTextArea();
		SourceConsole.setEditable(false);
		SourceConsole.setFont(new Font("Monospaced", Font.PLAIN, 20));
		scrollPane_1.setViewportView(SourceConsole);
		
		DefaultCaret caret = (DefaultCaret) RemoteConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        xeon.getDataProcessor().setGlobalConsole(null);
		    }
		});
		
	}
	
	public void addRemoteBroadcast(String s){
		
		s = Tools.filterNewLine(s);
		
		if(RemoteConsole != null){
		
			RemoteConsole.append(s);
			
		}
		
	}

}
