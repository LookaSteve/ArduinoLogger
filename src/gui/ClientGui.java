package gui;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;

import Modules.NetPayload;
import Modules.NetSystem;
import core.LocalConfig;
import core.Xeon;

import java.io.IOException;

import javax.swing.JOptionPane;

import javax.swing.SwingConstants;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClientGui extends InternalFrame {

	private Xeon xeon;
	
	private Client client;
	private NetSystem netSystem;
	
	private JLabel lblConnected;
	private JButton btnDisconnect;
	
	private String name;
	private String address;
	private int port;
	private int timeout;
	private boolean isConnected = false;
	private boolean autoConnect = true;
	
	
	public ClientGui(final Xeon xeon,String name,String address,int port,int timeout,boolean autoConnect) {
		
		super(xeon,0,"DATACLIENT");
		
		this.xeon = xeon;
		this.name = name;
		this.address = address;
		this.port = port;
		this.timeout = timeout;
		this.autoConnect = autoConnect;
		
		netSystem = new NetSystem(xeon);
		
		setBounds(100, 100, 628, 300);
		
		JLabel lblServer = new JLabel("To server " + address);
		lblServer.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblServer, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		lblConnected = new JLabel("Disconnected");
		lblConnected.setForeground(new Color(128, 0, 0));
		panel.add(lblConnected, BorderLayout.CENTER);
		lblConnected.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblConnected.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnDisconnect = new JButton("Connect");
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(isConnected){
					disconnect();
				}else{
					connect();
				}
				
			}
		});
		panel_1.add(btnDisconnect);
		
		if(autoConnect){
			connect();
		}
		
	}
	
	@Override
	public void windowClosed(){
		
		disconnect();
		
	}
	
	public void connect(){
		
		//Client setup
	    client = new Client();
	    LocalConfig.registerKryo(client.getKryo());
	    client.start();
	    
	    try {
            client.connect(timeout, address, port, port);
            isConnected = true;
            showConnected();
        } catch (IOException e) {
            isConnected = false;
            showDisconnected();
            JOptionPane.showMessageDialog(this, "Could not connect to " + address, "Connection Error", JOptionPane.ERROR_MESSAGE);
        }
		
	    client.addListener(new Listener(){
	        public void received (Connection connection, Object object){
	           if (object instanceof NetPayload){
	               NetPayload payload = (NetPayload)object;
	               processData(payload);
	           }
	        }
	        public void connected (Connection connection){
	            
	        }
	        public void disconnected (Connection connection){
	            isConnected = false;
	            showDisconnected();
	        }
	     });
		
	}
	
	public void processData(NetPayload netPayload){
		
		//Interpret header
		if(netSystem.interpretHeader(netPayload)){
			
			//Send data to dataprocessor
			xeon.getDataProcessor().processData(netPayload.getPayload(),netPayload.getOriginSource());
			
		}

	}
	
	public void sendData(String data,String source){
		
		if(client != null){
			
		    //Preparing payload
		    NetPayload payload = new NetPayload(xeon.getDataProcessor().getLocalSource(), source, data);
			
			//Sending data
			client.sendTCP(payload);
			
		}
		
	}
	
	public void disconnect(){
		
		if(client == null)return;
		
		client.close();
		
		client = null;
		
		isConnected = false;
		
	}
	
	public void showConnected(){
		
		lblConnected.setText("Connected");
		lblConnected.setForeground(new Color(0, 128, 0));
		btnDisconnect.setText("Disconnect");
		
	}
	
	public void showDisconnected(){
		
		lblConnected.setText("Disconnected");
		lblConnected.setForeground(new Color(128, 0, 0));
		btnDisconnect.setText("Connect");
		
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAutoConnect() {
		return autoConnect;
	}

	public void setAutoConnect(boolean autoConnect) {
		this.autoConnect = autoConnect;
	}

	
	
}
