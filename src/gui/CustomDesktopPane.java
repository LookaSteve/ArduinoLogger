package gui;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JDesktopPane;

import SuperScale.SuperRectangle;
import SuperScale.SuperScaler;
import core.Xeon;

public class CustomDesktopPane extends JDesktopPane{
	
	private BufferedImage img = null;
	private SuperScaler superScaler = new SuperScaler();
	private SuperRectangle childRect;
	
	private Boolean displayLogo = true;
	
	private Xeon xeon;
	
	public CustomDesktopPane(Xeon xeon){
		super();
		
		this.xeon = xeon;
		
		loadLogo();
		
	}
	
	public void loadLogo(){
		
		try {
		    img = ImageIO.read(xeon.getLogoFile());
		    childRect = new SuperRectangle(img.getWidth(), img.getHeight());
		} catch (IOException e) {
		}
		
		repaint();
		
	}

	@Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        if(img != null && displayLogo){
            
            Graphics2D g2d = (Graphics2D)g;
            
            Composite bac = g2d.getComposite();
            float logoOpacityAsFloat = xeon.getLogoOpacity();
            AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,logoOpacityAsFloat/100);
            g2d.setComposite(ac);
            
        	SuperRectangle parentRect = new SuperRectangle(this.getWidth(), this.getHeight());
        	SuperRectangle scaledRect = superScaler.scaleDown(parentRect, childRect,(double)(xeon.getLogoSize()/100.0));
        	g2d.drawImage(img, scaledRect.getX(), scaledRect.getY(),scaledRect.getW(),scaledRect.getH(), this);
        	
        	g2d.setComposite(bac);
        	
        }
        
    }

	public Boolean getDisplayLogo() {
		return displayLogo;
	}

	public void setDisplayLogo(Boolean displayLogo) {
		this.displayLogo = displayLogo;
	}
	
	
	
}
