package gui;

import core.Xeon;

public class NumericGraph extends InternalFrame{
	
	private CrossHairChart CrossHairChart;
	
	public NumericGraph(Xeon xeon,CrossHairChart CrossHairChart,int dataIndex){
		
		super(xeon,dataIndex,"NUMGRAPH");
		this.CrossHairChart = CrossHairChart;
		
	}
	
	@Override
	public void addData(String s){
		
		
		
		float data = 0;
		
		try {
			
			data = Float.parseFloat(s);
			
	    }catch (NumberFormatException e) {
	    	
	        return;
	        
	    }
        
        try {
			
        	CrossHairChart.getDataset().advanceTime();
            CrossHairChart.getDataset().appendData(new float[] { data });
            //CrossHairChart.setLastValue(data);
			
	    }catch (Exception e) {
	    	
	    	System.out.println("An exception has been caught.");
	    	
	        return;
	        
	    }
        
        
		
	}

	public CrossHairChart getCrossHairChart() {
		return CrossHairChart;
	}

	public void setCrossHairChart(CrossHairChart crossHairChart) {
		CrossHairChart = crossHairChart;
	}
	
	

}
