package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.Font;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;

public class About extends JFrame {

	private JPanel contentPane;
	
	private DenebUI denebUI;

	/**
	 * Create the frame.
	 */
	public About(DenebUI denebUI) {
		setResizable(false);
		
		this.denebUI = denebUI;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 650, 350);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		setTitle("About");
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblAboutDenebRc = new JLabel("Deneb RC Edition");
		lblAboutDenebRc.setFont(new Font("Tahoma", Font.PLAIN, 25));
		panel.add(lblAboutDenebRc);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		panel_1.add(btnClose);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.CENTER);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{112, 0};
		gbl_panel_2.rowHeights = new int[]{20, 27, 0, 0, 22, 0, 20, 22, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel label = new JLabel("   ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel_2.add(label, gbc_label);
		
		JLabel lblDenebRcEdition = new JLabel(denebUI.getDenebVersion());
		lblDenebRcEdition.setHorizontalAlignment(SwingConstants.CENTER);
		lblDenebRcEdition.setFont(new Font("Tahoma", Font.PLAIN, 22));
		GridBagConstraints gbc_lblDenebRcEdition = new GridBagConstraints();
		gbc_lblDenebRcEdition.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblDenebRcEdition.insets = new Insets(0, 0, 5, 0);
		gbc_lblDenebRcEdition.gridx = 0;
		gbc_lblDenebRcEdition.gridy = 1;
		panel_2.add(lblDenebRcEdition, gbc_lblDenebRcEdition);
		
		JLabel lblDeveloppedByJosselin = new JLabel("Developped by Josselin S.");
		lblDeveloppedByJosselin.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeveloppedByJosselin.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblDeveloppedByJosselin = new GridBagConstraints();
		gbc_lblDeveloppedByJosselin.anchor = GridBagConstraints.WEST;
		gbc_lblDeveloppedByJosselin.insets = new Insets(0, 0, 5, 0);
		gbc_lblDeveloppedByJosselin.gridx = 0;
		gbc_lblDeveloppedByJosselin.gridy = 2;
		panel_2.add(lblDeveloppedByJosselin, gbc_lblDeveloppedByJosselin);
		
		JLabel lblChartsByJfreecharts = new JLabel("Charts by JFreeCharts");
		lblChartsByJfreecharts.setHorizontalAlignment(SwingConstants.CENTER);
		lblChartsByJfreecharts.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblChartsByJfreecharts = new GridBagConstraints();
		gbc_lblChartsByJfreecharts.anchor = GridBagConstraints.WEST;
		gbc_lblChartsByJfreecharts.insets = new Insets(0, 0, 5, 0);
		gbc_lblChartsByJfreecharts.gridx = 0;
		gbc_lblChartsByJfreecharts.gridy = 3;
		panel_2.add(lblChartsByJfreecharts, gbc_lblChartsByJfreecharts);
		
		JLabel lblDesktopscrollpaneByPedro = new JLabel("DesktopScrollPane by Pedro Duque Vieira");
		lblDesktopscrollpaneByPedro.setHorizontalAlignment(SwingConstants.CENTER);
		lblDesktopscrollpaneByPedro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblDesktopscrollpaneByPedro = new GridBagConstraints();
		gbc_lblDesktopscrollpaneByPedro.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblDesktopscrollpaneByPedro.insets = new Insets(0, 0, 5, 0);
		gbc_lblDesktopscrollpaneByPedro.gridx = 0;
		gbc_lblDesktopscrollpaneByPedro.gridy = 4;
		panel_2.add(lblDesktopscrollpaneByPedro, gbc_lblDesktopscrollpaneByPedro);
		
		JLabel lblKryonetByEsotericsoftware = new JLabel("KryoNet by EsotericSoftware");
		lblKryonetByEsotericsoftware.setHorizontalAlignment(SwingConstants.CENTER);
		lblKryonetByEsotericsoftware.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblKryonetByEsotericsoftware = new GridBagConstraints();
		gbc_lblKryonetByEsotericsoftware.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblKryonetByEsotericsoftware.insets = new Insets(0, 0, 5, 0);
		gbc_lblKryonetByEsotericsoftware.gridx = 0;
		gbc_lblKryonetByEsotericsoftware.gridy = 5;
		panel_2.add(lblKryonetByEsotericsoftware, gbc_lblKryonetByEsotericsoftware);
		
		JLabel label_1 = new JLabel("   ");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 6;
		panel_2.add(label_1, gbc_label_1);
		
		JLabel lblLastBuild = new JLabel("Last build: " + denebUI.getDenebLastBuild());
		lblLastBuild.setHorizontalAlignment(SwingConstants.CENTER);
		lblLastBuild.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblLastBuild = new GridBagConstraints();
		gbc_lblLastBuild.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblLastBuild.gridx = 0;
		gbc_lblLastBuild.gridy = 7;
		panel_2.add(lblLastBuild, gbc_lblLastBuild);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new EmptyBorder(0, 20, 0, 30));
		contentPane.add(panel_3, BorderLayout.WEST);

        try {
            BufferedImage myPicture = ImageIO.read(new File("res/logo.png"));
            JLabel picLabel = new JLabel(new ImageIcon(new ImageIcon(myPicture).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)));
            panel_3.add(picLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		
	}

}
