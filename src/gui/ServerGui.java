package gui;

import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;

import core.LocalConfig;
import core.Xeon;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import Modules.NetPayload;
import Modules.NetSystem;
import Modules.Tools;

public class ServerGui extends InternalFrame {

	private Xeon xeon;
	
	private Server server;
	private NetSystem netSystem;
	
	private JTextArea textArea;
	private JLabel lblEvents;
	private JButton btnAdd;
	private DefaultListModel<String> eventListModel;
	private JList<String> list_1;
	
	private String name;
	private int port = 12073;
	private boolean isOnline = false;
	private boolean isQueued = false;
	private boolean autoConnect = true;
	
	public ServerGui(final Xeon xeon,String name,int port,boolean autoConnect) {
		
		super(xeon,0,"DATASERVER");
		
		this.xeon = xeon;
		this.name = name;
		this.port = port;
		this.autoConnect = autoConnect;
		
		netSystem = new NetSystem(xeon);
		
		setBounds(100, 100, 628, 300);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setResizeWeight(0.5);
		getContentPane().add(splitPane_1, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		splitPane_1.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.NORTH);
		
		lblEvents = new JLabel("Server : OFF");
		lblEvents.setForeground(new Color(128, 0, 0));
		lblEvents.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_2.add(lblEvents);
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.CENTER);
		
		btnAdd = new JButton("Put online");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(isOnline){
					disconnect();
				}else{
					connect();
				}
				
				refreshAfterDelay();
				
			}
		});
		panel_3.add(btnAdd);
		
		JButton btnEdit = new JButton("Kick");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int index = list_1.getSelectedIndex();
				
				Connection[] connections = server.getConnections();
				
				if(index != -1 && server != null && isOnline == true){
					if(index < connections.length){
					    connections[index].close();
					}
				}
				
				refreshAfterDelay();
				
			}
		});
		panel_3.add(btnEdit);
		
		eventListModel = new DefaultListModel<String>();
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		list_1 = new JList(eventListModel);
		scrollPane.setViewportView(list_1);
		
		JPanel panel_10 = new JPanel();
		splitPane_1.setRightComponent(panel_10);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_9 = new JPanel();
		panel_10.add(panel_9, BorderLayout.NORTH);
		panel_9.setLayout(new BorderLayout(0, 0));
		
		JLabel lblConsoleLog = new JLabel(" Console Log");
		lblConsoleLog.setFont(new Font("Tahoma", Font.PLAIN, 17));
		panel_9.add(lblConsoleLog, BorderLayout.WEST);
		
		JButton btnX = new JButton("Clear console");
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textArea.setText("");
				
			}
		});
		panel_9.add(btnX, BorderLayout.EAST);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_10.add(scrollPane_1, BorderLayout.CENTER);
		
		textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);
		textArea.setEditable(false);
		
		if(autoConnect){
			connect();
		}

	}
	
	@Override
	public void windowClosed(){
		
		disconnect();
		
	}

	public void sendData(String data,String source){
		
		if(server != null){
			
		    //Create payload
		    NetPayload payload = new NetPayload(xeon.getDataProcessor().getLocalSource(), source, data);
		    
		    //Send to all clients
			server.sendToAllTCP(payload);
			
		}
		
	}
	
	public void connect(){
		
	    try {
	        server = new Server();
	        LocalConfig.registerKryo(server.getKryo());
	        server.start();
            server.bind(port, port);
            isOnline = true;
            showOnline();
        } catch (IOException e){
            isOnline = false;
            showOffline();
            JOptionPane.showMessageDialog(this, "Port " + port + " might be already used by another server.", "Server Error", JOptionPane.ERROR_MESSAGE);
        }
	    
        server.addListener(new Listener() {
            public void received (Connection connection, Object object) {
               if (object instanceof NetPayload) {
                  NetPayload payload = (NetPayload)object;
                  consoleLog("[DATA SERVER] - " + payload.getPayload());
                  processData(payload);
               }
            }
            public void connected (Connection connection) {
                consoleLog("[SERVER EVENT] - " + connection.getID() + " CONNECTED");
                refreshAfterDelay();
            }
            public void disconnected (Connection connection) {
                consoleLog("[SERVER EVENT] - " + connection.getID() + " DISCONNECTED");
                refreshAfterDelay();
            }
         });
		
	}
	
	public void processData(NetPayload netPayload){
		
		System.out.println(netPayload.getPayload());
		
		if(netSystem.interpretHeader(netPayload)){

			//Send data to dataprocessor
			xeon.getDataProcessor().upPropagate(netPayload.getPayload(),netPayload.getOriginSource());
			
		}

	}
	
	public void disconnect(){
		
		if(server == null)return;
		
		server.close();
		
		server = null;
		
		isOnline = false;
		
		showOffline();
		
	}
	
	public void showOnline(){
		
		lblEvents.setText("Server : ON");
		lblEvents.setForeground(new Color(0, 128, 0));
		btnAdd.setText("Disconnect");
		
	}
	
	public void showOffline(){
		
		lblEvents.setText("Server : OFF");
		lblEvents.setForeground(new Color(128, 0, 0));
		btnAdd.setText("Put online");
		
	}
	
	public void refreshAfterDelay(){
		
		if(isQueued){
			return;
		}
		
	    Timer timer = new Timer();

	    TimerTask action = new TimerTask() {
	        public void run() {
	            paintList();
	        }

	    };

	    timer.schedule(action, 500); //this starts the task
		
	    isQueued = true;
	}
	
	public void paintList(){
		
		eventListModel.clear();
		
		if(server == null){
			isQueued = false;
			return;
		}
		
		Connection[] connections = server.getConnections();
		
		for(int i = 0; i < connections.length; i ++){
			
			eventListModel.addElement(connections[i].getID() + " - " + connections[i].getRemoteAddressTCP());
			
		}
		
		isQueued = false;
		
	}
	
	public void consoleLog(String s){
		
		s = Tools.filterNewLine(s);
		
		textArea.append(s);
		
	}

	public Xeon getXeon() {
		return xeon;
	}

	public void setXeon(Xeon xeon) {
		this.xeon = xeon;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isAutoConnect() {
		return autoConnect;
	}

	public void setAutoConnect(boolean autoConnect) {
		this.autoConnect = autoConnect;
	}
	
	
	
}
