package gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.ColorButton;
import Modules.NumericField;
import Modules.Tools;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Panel;

public class NumericGraphCreation extends JFrame {

	private DenebUI denui;
	private JPanel contentPane;
	private JTextField fieldWindowTitle;
	private NumericField fieldRefIndex;
	private JTextField fieldGraphT;
	private JTextField fieldYaxis;
	private NumericGraphAdvanced nga;
	private ColorButton btnLineColor;
	private ColorButton btnBackColor;
	private ColorButton btnGridColor;

	/**
	 * Create the frame.
	 */
	public NumericGraphCreation(final DenebUI denui) {
		this.denui = denui;
		setResizable(false);
		setTitle("New Numeric Graph");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 536, 375);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		this.addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	        	 if(nga != null){
	        		 if(nga.isVisible()){
	        			 nga.dispose();
	        		 }
	        	 }
	           	dispose();
	         }        
	      });    
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewNumericGraph = new JLabel("New Numeric Graph");
		lblNewNumericGraph.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblNewNumericGraph);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] {114, 138, 30, 30, 0};
		gbl_panel_1.rowHeights = new int[]{26, 26, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblWindowTitle = new JLabel("Window Title ");
		GridBagConstraints gbc_lblWindowTitle = new GridBagConstraints();
		gbc_lblWindowTitle.anchor = GridBagConstraints.EAST;
		gbc_lblWindowTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblWindowTitle.gridx = 0;
		gbc_lblWindowTitle.gridy = 1;
		panel_1.add(lblWindowTitle, gbc_lblWindowTitle);
		
		fieldWindowTitle = new JTextField();
		fieldWindowTitle.setText("New Numeric Graph");
		GridBagConstraints gbc_fieldWindowTitle = new GridBagConstraints();
		gbc_fieldWindowTitle.insets = new Insets(0, 0, 5, 5);
		gbc_fieldWindowTitle.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldWindowTitle.gridx = 1;
		gbc_fieldWindowTitle.gridy = 1;
		panel_1.add(fieldWindowTitle, gbc_fieldWindowTitle);
		fieldWindowTitle.setColumns(10);
		
		JLabel lblGraphTitle = new JLabel("Graph Title ");
		GridBagConstraints gbc_lblGraphTitle = new GridBagConstraints();
		gbc_lblGraphTitle.anchor = GridBagConstraints.EAST;
		gbc_lblGraphTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblGraphTitle.gridx = 0;
		gbc_lblGraphTitle.gridy = 2;
		panel_1.add(lblGraphTitle, gbc_lblGraphTitle);
		
		fieldGraphT = new JTextField();
		fieldGraphT.setText("New Numeric Graph");
		GridBagConstraints gbc_fieldGraphT = new GridBagConstraints();
		gbc_fieldGraphT.insets = new Insets(0, 0, 5, 5);
		gbc_fieldGraphT.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldGraphT.gridx = 1;
		gbc_fieldGraphT.gridy = 2;
		panel_1.add(fieldGraphT, gbc_fieldGraphT);
		fieldGraphT.setColumns(10);
		
		JLabel lblYAxisTitle = new JLabel("Y Axis Title ");
		GridBagConstraints gbc_lblYAxisTitle = new GridBagConstraints();
		gbc_lblYAxisTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblYAxisTitle.anchor = GridBagConstraints.EAST;
		gbc_lblYAxisTitle.gridx = 0;
		gbc_lblYAxisTitle.gridy = 3;
		panel_1.add(lblYAxisTitle, gbc_lblYAxisTitle);
		
		fieldYaxis = new JTextField();
		fieldYaxis.setText("Value");
		GridBagConstraints gbc_fieldYaxis = new GridBagConstraints();
		gbc_fieldYaxis.insets = new Insets(0, 0, 5, 5);
		gbc_fieldYaxis.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldYaxis.gridx = 1;
		gbc_fieldYaxis.gridy = 3;
		panel_1.add(fieldYaxis, gbc_fieldYaxis);
		fieldYaxis.setColumns(10);
		
		JLabel lblReferenceIndex = new JLabel("  Channel Index ");
		GridBagConstraints gbc_lblReferenceIndex = new GridBagConstraints();
		gbc_lblReferenceIndex.anchor = GridBagConstraints.EAST;
		gbc_lblReferenceIndex.insets = new Insets(0, 0, 5, 5);
		gbc_lblReferenceIndex.gridx = 0;
		gbc_lblReferenceIndex.gridy = 4;
		panel_1.add(lblReferenceIndex, gbc_lblReferenceIndex);
		
		fieldRefIndex = new NumericField();
		fieldRefIndex.setValue(0);
		GridBagConstraints gbc_fieldRefIndex = new GridBagConstraints();
		gbc_fieldRefIndex.insets = new Insets(0, 0, 5, 5);
		gbc_fieldRefIndex.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldRefIndex.gridx = 1;
		gbc_fieldRefIndex.gridy = 4;
		panel_1.add(fieldRefIndex, gbc_fieldRefIndex);
		fieldRefIndex.setColumns(10);
		
		JButton button = fieldRefIndex.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 2;
		gbc_button.gridy = 4;
		panel_1.add(button, gbc_button);
		
		JButton button_1 = fieldRefIndex.generatePlusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 0);
		gbc_button_1.gridx = 3;
		gbc_button_1.gridy = 4;
		panel_1.add(button_1, gbc_button_1);
		
		JLabel lblLineColor = new JLabel("Line Color ");
		GridBagConstraints gbc_lblLineColor = new GridBagConstraints();
		gbc_lblLineColor.anchor = GridBagConstraints.EAST;
		gbc_lblLineColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblLineColor.gridx = 0;
		gbc_lblLineColor.gridy = 5;
		panel_1.add(lblLineColor, gbc_lblLineColor);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 5);
		gbc_panel_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 5;
		panel_1.add(panel_3, gbc_panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		btnLineColor = new ColorButton(this,Color.red,"Choose Line Color");
		panel_3.add(btnLineColor);
		
		JLabel lblBackgroundColor = new JLabel("Background Color");
		GridBagConstraints gbc_lblBackgroundColor = new GridBagConstraints();
		gbc_lblBackgroundColor.anchor = GridBagConstraints.EAST;
		gbc_lblBackgroundColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBackgroundColor.gridx = 0;
		gbc_lblBackgroundColor.gridy = 6;
		panel_1.add(lblBackgroundColor, gbc_lblBackgroundColor);
		
		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 5, 5);
		gbc_panel_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_4.gridx = 1;
		gbc_panel_4.gridy = 6;
		panel_1.add(panel_4, gbc_panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		btnBackColor = new ColorButton(this,Color.LIGHT_GRAY,"Choose Background Color");
		panel_4.add(btnBackColor, BorderLayout.CENTER);
		
		JLabel lblGridColor = new JLabel("Grid Color");
		GridBagConstraints gbc_lblGridColor = new GridBagConstraints();
		gbc_lblGridColor.anchor = GridBagConstraints.EAST;
		gbc_lblGridColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblGridColor.gridx = 0;
		gbc_lblGridColor.gridy = 7;
		panel_1.add(lblGridColor, gbc_lblGridColor);
		
		Panel panel_5 = new Panel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_5.insets = new Insets(0, 0, 5, 5);
		gbc_panel_5.gridx = 1;
		gbc_panel_5.gridy = 7;
		panel_1.add(panel_5, gbc_panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		btnGridColor = new ColorButton(this,Color.WHITE,"Choose Grid Color");
		panel_5.add(btnGridColor, BorderLayout.CENTER);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(Tools.cleanInputInt(fieldRefIndex.getText()) < 0){
					
					JOptionPane.showMessageDialog(null, "You cannot use a negative number for an index.", "Warning !" , JOptionPane.WARNING_MESSAGE);
					
					return;
					
				}
				
				boolean isXAxisVIsible = true;
				boolean isYAxisVisible = true;
				boolean isRangeAuto = true;
				int range = 60;
				
				if(nga != null){
					
					isXAxisVIsible = nga.getIsXDisplay();
					isYAxisVisible = nga.getIsYDisplay();
					isRangeAuto = nga.getIsRangeAuto();
					range = nga.getRange();
					
				}
				
				denui.getInternalFrameManager().addNewGraphWindow(fieldWindowTitle.getText(),fieldRefIndex.getValue(),btnLineColor.getColor(),btnBackColor.getColor(),btnGridColor.getColor(),fieldGraphT.getText(),fieldYaxis.getText(),isXAxisVIsible,isYAxisVisible,isRangeAuto,range,null);
				dispose();
				
			}
		});
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JButton btnAdvancedSettings = new JButton("Advanced Settings");
		btnAdvancedSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(nga != null){
					if(!nga.isVisible()){
						nga = new NumericGraphAdvanced();
						nga.setVisible(true);
					}else{
						nga.requestFocus();
					}
				}else{
					nga = new NumericGraphAdvanced();
					nga.setVisible(true);
				}

			}
		});
		panel_2.add(btnAdvancedSettings, BorderLayout.WEST);
		panel_2.add(btnNewButton, BorderLayout.EAST);
	}

}
