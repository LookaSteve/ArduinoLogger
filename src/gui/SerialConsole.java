package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

import Modules.Tools;
import card.Card;
import core.Xeon;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JCheckBox;

public class SerialConsole extends JFrame {

	private Xeon xeon;
	
	private JPanel contentPane;
	private JTextField inputConsole;
	private JTextArea SourceConsole;
	private JTextArea RemoteConsole;
	private JComboBox cardCombo;
	
	private Card connectedCard = null;
	private JLabel lblWarningNoCard;
	
	private Boolean cardComboSelectorActive = true;
	private JCheckBox chckbxTimestamps;

	public SerialConsole(Xeon xeon) {
		
		this.xeon = xeon;
		
		setTitle("Serial console");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 679, 453);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		inputConsole = new JTextField();
		inputConsole.setFont(new Font("Monospaced", Font.PLAIN, 20));
		panel.add(inputConsole, BorderLayout.CENTER);
		inputConsole.setColumns(10);
		
		JButton btnSend = new JButton("Send");
		panel.add(btnSend, BorderLayout.EAST);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			    //Get command from the text field
				String command = inputConsole.getText();
				
				//String that will be display on the serial console
				String displayCommand = command + "\r\n";
				
				//If we have to add a timestamp
				if(chckbxTimestamps.isSelected()){
				    //Add a timstamp
				    displayCommand = Tools.getFormatedTime()+ " -> " + displayCommand;
				}
				
				//Display the command on the serial console
				SourceConsole.append(displayCommand);
				
				//If a card is paired
				if(connectedCard != null){
				    //Write data to that card
					connectedCard.writeData(command);
				}
				
				//Clear input field
				inputConsole.setText("");
				
			}
		});
		
		this.getRootPane().setDefaultButton(btnSend);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.EAST);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				loadConnectedCards();
				
			}
		});
		panel_2.add(btnRefresh);
		
		JLabel lblSelectedCard = new JLabel("Selected card : ");
		panel_2.add(lblSelectedCard);
		
		cardCombo = new JComboBox();
		cardCombo.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		        //If the event is not disabled
		        if(cardComboSelectorActive){
		            //Attempt to subscribe to the selected card
		            if(!subscribeCard(cardCombo.getSelectedIndex())){
		                //If the subscription failed the card was already used by another console
		                JOptionPane.showMessageDialog(null,
                                "A similar console is already opened.",
                                "Serial warning",
                                JOptionPane.WARNING_MESSAGE);
	                }
		        }
		    }
		});
		panel_2.add(cardCombo);
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4, BorderLayout.WEST);
		
		chckbxTimestamps = new JCheckBox("Timestamps");
		panel_4.add(chckbxTimestamps);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.7);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		JPanel panel_5 = new JPanel();
		splitPane.setLeftComponent(panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_5.add(panel_7, BorderLayout.NORTH);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JLabel lblRemoteSource = new JLabel(" Card incoming data : ");
		panel_7.add(lblRemoteSource, BorderLayout.WEST);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				RemoteConsole.setText("");
				
			}
		});
		panel_7.add(btnClear, BorderLayout.EAST);
		
		lblWarningNoCard = new JLabel("Warning no card selected");
		lblWarningNoCard.setForeground(Color.RED);
		lblWarningNoCard.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblWarningNoCard.setHorizontalAlignment(SwingConstants.CENTER);
		panel_7.add(lblWarningNoCard, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		panel_5.add(scrollPane, BorderLayout.CENTER);
		
		RemoteConsole = new JTextArea();
		RemoteConsole.setEditable(false);
		RemoteConsole.setFont(new Font("Monospaced", Font.PLAIN, 20));
		scrollPane.setViewportView(RemoteConsole);
		
		JPanel panel_6 = new JPanel();
		splitPane.setRightComponent(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_8 = new JPanel();
		panel_6.add(panel_8, BorderLayout.NORTH);
		panel_8.setLayout(new BorderLayout(0, 0));
		
		JLabel lblLocalSource = new JLabel(" Local commands : ");
		panel_8.add(lblLocalSource, BorderLayout.WEST);
		
		JButton button = new JButton("Clear");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SourceConsole.setText("");
				
			}
		});
		panel_8.add(button, BorderLayout.EAST);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_6.add(scrollPane_1, BorderLayout.CENTER);
		
		SourceConsole = new JTextArea();
		SourceConsole.setEditable(false);
		SourceConsole.setFont(new Font("Monospaced", Font.PLAIN, 20));
		scrollPane_1.setViewportView(SourceConsole);
		
		DefaultCaret caret = (DefaultCaret) RemoteConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE); 
		
		loadConnectedCards();
		
		subscribeCard(0);
		
		//When the console is closed
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        //Unsubscribe any paired cards before closing
		        unsubscribeCard();
		    }
		});
		
	}
	
	//Fill the list of the combo box with the connected cards
	public void loadConnectedCards(){
	    
	    //Temporary disable the card selection combo event 
	    cardComboSelectorActive = false;
		
	    //Clear combo items
		cardCombo.removeAllItems();
		
		//For all connected cards
		for(int i = 0; i < xeon.getCardManager().getConnectedCards().size(); i++){
			
		    //Add card to the combo list
			cardCombo.addItem(xeon.getCardManager().getConnectedCards().get(i).getPort());
			
			//If the serial console already has a connected card
			if(connectedCard != null){
			
			    //If the current card is equal to the console's card
				if(connectedCard.equals(xeon.getCardManager().getConnectedCards().get(i))){
					
				    //Set the selected index of the combo list
					cardCombo.setSelectedIndex(i);
					
				}
				
			}
			
		}
		
		//Once the combo list has been refreshed we can enable again the action listener
		cardComboSelectorActive = true;
		
	}
	
	//Pair a card to the serial console
	public boolean subscribeCard(int index){
		
		//If a card is currently subscribed to this console
		if(connectedCard != null){
		    //Unsubscrive any previous card
		    unsubscribeCard();
		}
		
		//If the index of the card to pair is not beyond what is available
		if(xeon.getCardManager().getConnectedCards().size() > index){
		
		    //If that card is already paired
			if(xeon.getCardManager().getConnectedCards().get(index).getListeningConsole() != null){
				
			    //Return false
				return false;
				
			}
			
			//Set selected item in the combo box
			cardCombo.setSelectedIndex(index);
			
			//Store the reference of the card in the serial console object
			connectedCard = xeon.getCardManager().getConnectedCards().get(index);
			
			//Give the card the reference of the serial console
			connectedCard.setListeningConsole(this);
			
			//Make the no card warning invisible
			lblWarningNoCard.setVisible(false);
			
			//Return true
			return true;
			
		}
		
		//Return false
		return false;

	}
	
	//Remove the pairing between the console and a card
	public void unsubscribeCard(){
		
	    //If a card is paired
		if(connectedCard != null){
			
		    //Set the no card connected message to visible
			lblWarningNoCard.setVisible(true);
			
			//Remove the pairing on the card's side
			connectedCard.setListeningConsole(null);
			
			//Remove the pairing on the console's side
			connectedCard = null;
			
		}

	}
	
	//Method used to display data comming from the card
	public void addRemoteBroadcast(String s){
		
	    //Add a newline to the string if none already exists
		s = Tools.filterNewLine(s);
		
		//If we need to display a timestamp on each line
		if(chckbxTimestamps.isSelected()){
		    //Add a timestamp to the string
		    s = Tools.getFormatedTime() + " -> " + s;
		}
		
		//If the console text area is available
		if(RemoteConsole != null){
		
		    //Append new line
			RemoteConsole.append(s);
			
		}
		
	}

}
