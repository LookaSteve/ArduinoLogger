package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;

import Logger.DataSet;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class ParsingLogReader extends JFrame {

    private JPanel contentPane;
    private DataSet dataSet;
    private JTextPane textPane;

    public ParsingLogReader(DataSet dataSet) {
        this.dataSet = dataSet;
        setTitle("Parsing Log");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 500);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        JScrollPane scrollPane = new JScrollPane();
        contentPane.add(scrollPane, BorderLayout.CENTER);
        
        textPane = new JTextPane();
        scrollPane.setViewportView(textPane);
        
        displayLog();
        
    }
    
    public void displayLog(){
        
        String text = "";
        
        for(int i = 0; i < dataSet.getLogParser().getParsingLog().size(); i++){
            
            text += dataSet.getLogParser().getParsingLog().get(i) + System.lineSeparator();
            
        }
        
        textPane.setText(text);
        
    }

}
