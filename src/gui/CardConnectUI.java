package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.NumericField;
import card.Card;
import card.CardManager;
import core.Xeon;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CardConnectUI extends JFrame {

    private Xeon xeon;
    private Card card;
    
    private JPanel contentPane;
    private NumericField baudRateField;
    private JTextField cardNameField;

    public CardConnectUI(Xeon xeon,Card card,int baudRate) {
        setTitle("Card connection");
        
        this.xeon = xeon;
        this.card = card;
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 250);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        JButton btnConnect = new JButton("Connect");
        btnConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                int selectedBaudRate = baudRateField.getValue();
                if(selectedBaudRate <= 0){
                    JOptionPane.showMessageDialog(null,
                            "Baud rate must be positive and not zero.",
                            "Invalid Baud Rate",
                            JOptionPane.ERROR_MESSAGE);
                }else{
                    
                    //Set card nickname
                    card.setDisplayName(cardNameField.getText());
                    
                    //Open card
                    if(card.open(selectedBaudRate)){
                        CardManager cardManager = xeon.getCardManager();
                        cardManager.getDetectedCards().remove(card);
                        cardManager.getConnectedCards().add(card);
                        xeon.getMainWindow().refreshConnectedCardList();
                        xeon.getMainWindow().refreshDetectedCardList();
                        dispose();
                    }else{
                        JOptionPane.showMessageDialog(null,
                                "Port is busy.",
                                "Could not connect",
                                JOptionPane.ERROR_MESSAGE);
                    }
                    
                }
                
                
            }
        });
        panel.add(btnConnect);
        
        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		dispose();
        	}
        });
        panel.add(btnCancel);
        
        JLabel lblConnectToPort = new JLabel("Connect to port " + card.getPort());
        lblConnectToPort.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblConnectToPort.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblConnectToPort, BorderLayout.NORTH);
        
        JPanel panel_1 = new JPanel();
        contentPane.add(panel_1, BorderLayout.CENTER);
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);
        
        JLabel lblNewLabel = new JLabel("      ");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 0;
        panel_1.add(lblNewLabel, gbc_lblNewLabel);
        
        JLabel label = new JLabel("      ");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 0);
        gbc_label.gridx = 5;
        gbc_label.gridy = 0;
        panel_1.add(label, gbc_label);
        
        JLabel label_1 = new JLabel("      ");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 5, 5);
        gbc_label_1.gridx = 0;
        gbc_label_1.gridy = 1;
        panel_1.add(label_1, gbc_label_1);
        
        JLabel lblNickname = new JLabel("Display name");
        GridBagConstraints gbc_lblNickname = new GridBagConstraints();
        gbc_lblNickname.anchor = GridBagConstraints.EAST;
        gbc_lblNickname.insets = new Insets(0, 0, 5, 5);
        gbc_lblNickname.gridx = 2;
        gbc_lblNickname.gridy = 2;
        panel_1.add(lblNickname, gbc_lblNickname);
        
        cardNameField = new JTextField(card.getDefaultNickname());
        GridBagConstraints gbc_cardNameField = new GridBagConstraints();
        gbc_cardNameField.insets = new Insets(0, 0, 5, 5);
        gbc_cardNameField.fill = GridBagConstraints.HORIZONTAL;
        gbc_cardNameField.gridx = 3;
        gbc_cardNameField.gridy = 2;
        panel_1.add(cardNameField, gbc_cardNameField);
        cardNameField.setColumns(10);
        
        JLabel label_2 = new JLabel("            ");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.insets = new Insets(0, 0, 0, 5);
        gbc_label_2.gridx = 1;
        gbc_label_2.gridy = 3;
        panel_1.add(label_2, gbc_label_2);
        
        JLabel lblBaudRate = new JLabel("Baud rate ");
        GridBagConstraints gbc_lblBaudRate = new GridBagConstraints();
        gbc_lblBaudRate.insets = new Insets(0, 0, 0, 5);
        gbc_lblBaudRate.anchor = GridBagConstraints.EAST;
        gbc_lblBaudRate.gridx = 2;
        gbc_lblBaudRate.gridy = 3;
        panel_1.add(lblBaudRate, gbc_lblBaudRate);
        
        baudRateField = new NumericField();
        baudRateField.setValue(baudRate);
        GridBagConstraints gbc_baudRateField = new GridBagConstraints();
        gbc_baudRateField.insets = new Insets(0, 0, 0, 5);
        gbc_baudRateField.fill = GridBagConstraints.HORIZONTAL;
        gbc_baudRateField.gridx = 3;
        gbc_baudRateField.gridy = 3;
        panel_1.add(baudRateField, gbc_baudRateField);
        baudRateField.setColumns(10);
        
        JLabel label_3 = new JLabel("            ");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.insets = new Insets(0, 0, 0, 5);
        gbc_label_3.gridx = 4;
        gbc_label_3.gridy = 3;
        panel_1.add(label_3, gbc_label_3);
        
        getRootPane().setDefaultButton(btnConnect);
        
    }

}
