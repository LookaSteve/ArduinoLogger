package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;

import Modules.NumericField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TimeSetter extends JFrame {

	private JPanel contentPane;
	private NumericField textField;
	private NumericField textField_1;
	private EventScheduler eventScheduler;

	public TimeSetter(final EventScheduler eventScheduler) {
		this.eventScheduler = eventScheduler;
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 262);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{179, 119, 39, 45, 0};
		gbl_panel.rowHeights = new int[]{44, 29, 29, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblSecondsBeforeT = new JLabel("Seconds Before T - 0 : ");
		GridBagConstraints gbc_lblSecondsBeforeT = new GridBagConstraints();
		gbc_lblSecondsBeforeT.anchor = GridBagConstraints.EAST;
		gbc_lblSecondsBeforeT.insets = new Insets(0, 0, 5, 5);
		gbc_lblSecondsBeforeT.gridx = 0;
		gbc_lblSecondsBeforeT.gridy = 1;
		panel.add(lblSecondsBeforeT, gbc_lblSecondsBeforeT);
		
		textField = new NumericField();
		textField.setValue(0);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton button = textField.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.anchor = GridBagConstraints.NORTHWEST;
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 2;
		gbc_button.gridy = 1;
		panel.add(button, gbc_button);
		
		JButton button_2 = textField.generatePlusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.anchor = GridBagConstraints.NORTHWEST;
		gbc_button_2.insets = new Insets(0, 0, 5, 0);
		gbc_button_2.gridx = 3;
		gbc_button_2.gridy = 1;
		panel.add(button_2, gbc_button_2);
		
		JLabel lblMillisecondsBeforeT = new JLabel("Milliseconds Before T - 0 : ");
		GridBagConstraints gbc_lblMillisecondsBeforeT = new GridBagConstraints();
		gbc_lblMillisecondsBeforeT.anchor = GridBagConstraints.EAST;
		gbc_lblMillisecondsBeforeT.insets = new Insets(0, 0, 0, 5);
		gbc_lblMillisecondsBeforeT.gridx = 0;
		gbc_lblMillisecondsBeforeT.gridy = 2;
		panel.add(lblMillisecondsBeforeT, gbc_lblMillisecondsBeforeT);
		
		textField_1 = new NumericField();
		textField_1.setValue(0);
		textField_1.setColumns(10);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 0, 5);
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		panel.add(textField_1, gbc_textField_1);
		
		JButton button_1 = textField_1.generateMinusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_button_1.insets = new Insets(0, 0, 0, 5);
		gbc_button_1.gridx = 2;
		gbc_button_1.gridy = 2;
		panel.add(button_1, gbc_button_1);
		
		JButton button_3 = textField_1.generatePlusButton();
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.anchor = GridBagConstraints.NORTHWEST;
		gbc_button_3.gridx = 3;
		gbc_button_3.gridy = 2;
		panel.add(button_3, gbc_button_3);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblSetCountdownValue = new JLabel("Set Countdown Value");
		lblSetCountdownValue.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblSetCountdownValue);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JButton btnSet = new JButton("Set");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				long time = (textField.getValue() * 1000) + textField_1.getValue();
				
				eventScheduler.setTimeCount(time);
				eventScheduler.setBackTimeCount(time);
				eventScheduler.setNegative(true);
				eventScheduler.setBackIsNegative(true);
				eventScheduler.paintCountDown();
				
				dispose();
				
			}
		});
		panel_2.add(btnSet, BorderLayout.EAST);
	}

}
