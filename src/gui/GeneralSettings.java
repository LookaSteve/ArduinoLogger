package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Text;

import Modules.NumericField;
import Modules.OpacitySliderListener;
import Modules.SizeSliderListener;
import Modules.Tools;
import core.Xeon;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.TextField;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JCheckBox;

public class GeneralSettings extends JFrame {

	private JPanel contentPane;
	private NumericField textField;
	
	
	private Xeon xeon;
	private JTextField textField_1;
	private JList list;
	private DefaultListModel<String> model;
	private GeneralSettings thisObj;
	
	private NumericField textField_2;
	private NumericField textField_3;
	private JTextField textFieldLogoPath;
	private JSlider opacitySlider;
	private JSlider sizeSlider;


	public GeneralSettings(final Xeon xeon) {
		setTitle("General Settings");
		setResizable(false);
		
		thisObj = this;
		this.xeon = xeon;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 900, 570);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblGeneralSettings = new JLabel("General Settings");
		lblGeneralSettings.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblGeneralSettings);
		
		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3, BorderLayout.WEST);
		panel_3.setPreferredSize(new Dimension(850, 800));
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);
		
		JScrollPane panelScroll = new JScrollPane(panel_3);
		contentPane.add(panelScroll, BorderLayout.CENTER);
		panelScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		model = new DefaultListModel<>();
		
		
		
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel label_5 = new JLabel("      ");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 0;
		gbc_label_5.gridy = 0;
		panel_1.add(label_5, gbc_label_5);
		
		JLabel label_1 = new JLabel(" ");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 0;
		panel_1.add(label_1, gbc_label_1);
		
		JLabel label_6 = new JLabel("      ");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.insets = new Insets(0, 0, 5, 0);
		gbc_label_6.gridx = 5;
		gbc_label_6.gridy = 0;
		panel_1.add(label_6, gbc_label_6);
		
		JLabel lblDefaultCardSettings = new JLabel("Default Card Settings");
		lblDefaultCardSettings.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblDefaultCardSettings = new GridBagConstraints();
		gbc_lblDefaultCardSettings.insets = new Insets(0, 0, 5, 5);
		gbc_lblDefaultCardSettings.gridx = 1;
		gbc_lblDefaultCardSettings.gridy = 1;
		panel_1.add(lblDefaultCardSettings, gbc_lblDefaultCardSettings);
		
		JLabel lblDefaultBaudRate = new JLabel("Baud Rate");
		GridBagConstraints gbc_lblDefaultBaudRate = new GridBagConstraints();
		gbc_lblDefaultBaudRate.anchor = GridBagConstraints.EAST;
		gbc_lblDefaultBaudRate.insets = new Insets(0, 0, 5, 5);
		gbc_lblDefaultBaudRate.gridx = 1;
		gbc_lblDefaultBaudRate.gridy = 2;
		panel_1.add(lblDefaultBaudRate, gbc_lblDefaultBaudRate);
		
		textField = new NumericField();
		textField.setValue(xeon.getDefaultBaudRate());
		textField.setColumns(10);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 2;
		panel_1.add(textField, gbc_textField);
		textField.setText(xeon.getDefaultBaudRate()+"");
		textField.getDocument().addDocumentListener(new DocumentListener() {
		    @Override
		    public void insertUpdate(DocumentEvent e) {
		    	update();
		    }
		    @Override
		    public void removeUpdate(DocumentEvent e) {
		    	update();
		    }
		    @Override
		    public void changedUpdate(DocumentEvent e) {
		    	update();
		    }
		    public void update(){
		    	try{
		    		int intValue = Integer.parseInt(textField.getText());
		    		xeon.setDefaultBaudRate(intValue);
		    		xeon.getDefaultConfigManager().saveConfiguration();
		    	}catch(NumberFormatException e){
		    		e.printStackTrace();
		    	}
		    }
		});
		
		JButton button = textField.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 3;
		gbc_button.gridy = 2;
		panel_1.add(button, gbc_button);
		
		JButton button_2 = textField.generatePlusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.insets = new Insets(0, 0, 5, 5);
		gbc_button_2.gridx = 4;
		gbc_button_2.gridy = 2;
		panel_1.add(button_2, gbc_button_2);
		
		JLabel lblBannedSerialPorts = new JLabel("Banned Serial ports");
		GridBagConstraints gbc_lblBannedSerialPorts = new GridBagConstraints();
		gbc_lblBannedSerialPorts.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblBannedSerialPorts.insets = new Insets(0, 0, 5, 5);
		gbc_lblBannedSerialPorts.gridx = 1;
		gbc_lblBannedSerialPorts.gridy = 3;
		panel_1.add(lblBannedSerialPorts, gbc_lblBannedSerialPorts);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 3;
		panel_1.add(scrollPane, gbc_scrollPane);
		list = new JList(model);
		scrollPane.setViewportView(list);
		
		JPanel panel_4 = new JPanel();
		scrollPane.setRowHeaderView(panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[]{45, 0};
		gbl_panel_4.rowHeights = new int[]{29, 0, 0};
		gbl_panel_4.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_4.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_4.setLayout(gbl_panel_4);
		
		JButton btnRemove = new JButton("+");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			    
			    CreateBannedPort createBannedPort = new CreateBannedPort(xeon,thisObj);
			    createBannedPort.setVisible(true);
			    
			}
		});
		GridBagConstraints gbc_btnRemove = new GridBagConstraints();
		gbc_btnRemove.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRemove.anchor = GridBagConstraints.NORTH;
		gbc_btnRemove.insets = new Insets(0, 0, 5, 0);
		gbc_btnRemove.gridx = 0;
		gbc_btnRemove.gridy = 0;
		panel_4.add(btnRemove, gbc_btnRemove);
		
		JButton btnAddPort = new JButton("-");
		btnAddPort.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        
		        if(list.getSelectedIndex()>=0){
		            int dialogResult = JOptionPane.showConfirmDialog (null, "Remove port " + xeon.getCardManager().getScanner().getBannedPorts().get(list.getSelectedIndex()) + " from banned port list ?","Port Removal",JOptionPane.YES_NO_OPTION);
		            if(dialogResult == JOptionPane.YES_OPTION){
		                xeon.getCardManager().getScanner().getBannedPorts().remove(list.getSelectedIndex());
		                xeon.getDefaultConfigManager().saveConfiguration();
		                loadBannedPorts();
		            }
		        }
		        
		    }
		});
		GridBagConstraints gbc_btnAddPort = new GridBagConstraints();
		gbc_btnAddPort.fill = GridBagConstraints.BOTH;
		gbc_btnAddPort.gridx = 0;
		gbc_btnAddPort.gridy = 1;
		panel_4.add(btnAddPort, gbc_btnAddPort);
		
		JLabel label = new JLabel(" ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 4;
		panel_1.add(label, gbc_label);
		
		JLabel lblDefaultPath = new JLabel("Default Paths");
		lblDefaultPath.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblDefaultPath = new GridBagConstraints();
		gbc_lblDefaultPath.anchor = GridBagConstraints.EAST;
		gbc_lblDefaultPath.insets = new Insets(0, 0, 5, 5);
		gbc_lblDefaultPath.gridx = 1;
		gbc_lblDefaultPath.gridy = 5;
		panel_1.add(lblDefaultPath, gbc_lblDefaultPath);
		
		JLabel lblSavePath = new JLabel("Configuration path");
		GridBagConstraints gbc_lblSavePath = new GridBagConstraints();
		gbc_lblSavePath.anchor = GridBagConstraints.EAST;
		gbc_lblSavePath.insets = new Insets(0, 0, 5, 5);
		gbc_lblSavePath.gridx = 1;
		gbc_lblSavePath.gridy = 6;
		panel_1.add(lblSavePath, gbc_lblSavePath);
		
		textField_1 = new JTextField();
		textField_1.setText(xeon.getDefaultSavePath().toString());
		textField_1.setEditable(false);
		textField_1.setColumns(10);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 6;
		panel_1.add(textField_1, gbc_textField_1);
		
		JButton button_3 = new JButton("...");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser(); 
				
				chooser.setCurrentDirectory(xeon.getDefaultSavePath());
			    chooser.setDialogTitle("Choose default configuration path");
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    chooser.setAcceptAllFileFilterUsed(false);
			    
			    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
			    	
			    	File f = chooser.getSelectedFile();
			    	
			    	if(f.exists() && f.isDirectory()){
			    		
			    		xeon.setDefaultSavePath(f);
			    		
			    		textField_1.setText(f.toString());
			    		
			    		xeon.getDefaultConfigManager().saveConfiguration();
			    		
			    	}
		        }
				
			}
		});
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.insets = new Insets(0, 0, 5, 5);
		gbc_button_3.gridx = 3;
		gbc_button_3.gridy = 6;
		panel_1.add(button_3, gbc_button_3);
		
		JLabel label_2 = new JLabel(" ");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 7;
		panel_1.add(label_2, gbc_label_2);
		
		JLabel lblWorkspace = new JLabel("Workspace");
		lblWorkspace.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblWorkspace = new GridBagConstraints();
		gbc_lblWorkspace.anchor = GridBagConstraints.EAST;
		gbc_lblWorkspace.insets = new Insets(0, 0, 5, 5);
		gbc_lblWorkspace.gridx = 1;
		gbc_lblWorkspace.gridy = 8;
		panel_1.add(lblWorkspace, gbc_lblWorkspace);
		
		JLabel lblColor = new JLabel("Color");
		GridBagConstraints gbc_lblColor = new GridBagConstraints();
		gbc_lblColor.anchor = GridBagConstraints.EAST;
		gbc_lblColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblColor.gridx = 1;
		gbc_lblColor.gridy = 9;
		panel_1.add(lblColor, gbc_lblColor);
		
		final JButton button_4 = new JButton("Choose Color");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Color newColor = JColorChooser.showDialog(thisObj,"Choose Grid Color",Color.red);
				if(newColor == null){
					return;
				}
				button_4.setBackground(newColor);
				button_4.setForeground(Tools.getContrastColor(newColor));
				xeon.getMainWindow().getDesktopPane().setBackground(newColor);
				xeon.setBackgroundColor(newColor);
				xeon.getDefaultConfigManager().saveConfiguration();
				
			}
		});
		Color desktopPaneBGC = xeon.getMainWindow().getDesktopPane().getBackground();
		Color filteredColor = new Color(desktopPaneBGC.getRed(), desktopPaneBGC.getGreen(), desktopPaneBGC.getBlue());
		button_4.setBackground(filteredColor);
		button_4.setForeground(Tools.getContrastColor(filteredColor));
		GridBagConstraints gbc_button_4 = new GridBagConstraints();
		gbc_button_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_button_4.insets = new Insets(0, 0, 5, 5);
		gbc_button_4.gridx = 2;
		gbc_button_4.gridy = 9;
		panel_1.add(button_4, gbc_button_4);
		
		JLabel label_4 = new JLabel(" ");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 1;
		gbc_label_4.gridy = 10;
		panel_1.add(label_4, gbc_label_4);
		
		JLabel lblNetworking = new JLabel("Networking");
		lblNetworking.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblNetworking = new GridBagConstraints();
		gbc_lblNetworking.anchor = GridBagConstraints.EAST;
		gbc_lblNetworking.insets = new Insets(0, 0, 5, 5);
		gbc_lblNetworking.gridx = 1;
		gbc_lblNetworking.gridy = 11;
		panel_1.add(lblNetworking, gbc_lblNetworking);
		
		JLabel lblLoopProtection = new JLabel("Loop protection");
		GridBagConstraints gbc_lblLoopProtection = new GridBagConstraints();
		gbc_lblLoopProtection.anchor = GridBagConstraints.EAST;
		gbc_lblLoopProtection.insets = new Insets(0, 0, 5, 5);
		gbc_lblLoopProtection.gridx = 1;
		gbc_lblLoopProtection.gridy = 12;
		panel_1.add(lblLoopProtection, gbc_lblLoopProtection);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Enable Loop protection");
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox.gridx = 2;
		gbc_chckbxNewCheckBox.gridy = 12;
		panel_1.add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);
		chckbxNewCheckBox.setSelected(xeon.isLoopProtection());
		
		ChangeListener changeListenerLoop = new ChangeListener() {
		      public void stateChanged(ChangeEvent changeEvent) {
		        AbstractButton abstractButton = (AbstractButton)changeEvent.getSource();
		        ButtonModel buttonModel = abstractButton.getModel();
		        boolean selected = buttonModel.isSelected();
		        if(!selected){
		        	if(xeon.isLoopProtection()){
		        		xeon.setLoopProtection(false);
		        		xeon.getDefaultConfigManager().saveConfiguration();
		        	}
		        }else{
		        	if(!xeon.isLoopProtection()){
		        		xeon.setLoopProtection(true);
		        		xeon.getDefaultConfigManager().saveConfiguration();
		        	}
		        }
		      }
		    };
		   
		chckbxNewCheckBox.addChangeListener(changeListenerLoop);
		
		JLabel lblCommunicationPort = new JLabel("Communication Port");
		GridBagConstraints gbc_lblCommunicationPort = new GridBagConstraints();
		gbc_lblCommunicationPort.anchor = GridBagConstraints.EAST;
		gbc_lblCommunicationPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblCommunicationPort.gridx = 1;
		gbc_lblCommunicationPort.gridy = 13;
		panel_1.add(lblCommunicationPort, gbc_lblCommunicationPort);
		
		textField_2 = new NumericField();
		textField_2.setValue(xeon.getServerCommunicationPort());
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 2;
		gbc_textField_2.gridy = 13;
		panel_1.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		textField_2.setText(xeon.getServerCommunicationPort()+"");
		textField_2.getDocument().addDocumentListener(new DocumentListener() {
		    @Override
		    public void insertUpdate(DocumentEvent e) {
		    	update();
		    }
		    @Override
		    public void removeUpdate(DocumentEvent e) {
		    	update();
		    }
		    @Override
		    public void changedUpdate(DocumentEvent e) {
		    	update();
		    }
		    public void update(){
		    	try{
		    		int intValue = Integer.parseInt(textField_2.getText());
		    		xeon.setServerCommunicationPort(intValue);
		    		xeon.getDefaultConfigManager().saveConfiguration();
		    	}catch(NumberFormatException e){
		    		
		    	}
		    }
		});
		
		JButton button_7 = textField_2.generateMinusButton();
		GridBagConstraints gbc_button_7 = new GridBagConstraints();
		gbc_button_7.insets = new Insets(0, 0, 5, 5);
		gbc_button_7.gridx = 3;
		gbc_button_7.gridy = 13;
		panel_1.add(button_7, gbc_button_7);
		
		JButton button_8 = textField_2.generatePlusButton();
		GridBagConstraints gbc_button_8 = new GridBagConstraints();
		gbc_button_8.insets = new Insets(0, 0, 5, 5);
		gbc_button_8.gridx = 4;
		gbc_button_8.gridy = 13;
		panel_1.add(button_8, gbc_button_8);
		
		JLabel lblCommunicationTimeOut = new JLabel("Timeout");
		GridBagConstraints gbc_lblCommunicationTimeOut = new GridBagConstraints();
		gbc_lblCommunicationTimeOut.anchor = GridBagConstraints.EAST;
		gbc_lblCommunicationTimeOut.insets = new Insets(0, 0, 5, 5);
		gbc_lblCommunicationTimeOut.gridx = 1;
		gbc_lblCommunicationTimeOut.gridy = 14;
		panel_1.add(lblCommunicationTimeOut, gbc_lblCommunicationTimeOut);
		
		textField_3 = new NumericField();
		textField_3.setValue(xeon.getServerTimeout());
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 2;
		gbc_textField_3.gridy = 14;
		panel_1.add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);
		textField_3.setText(xeon.getServerTimeout()+"");
		textField_3.getDocument().addDocumentListener(new DocumentListener() {
		    @Override
		    public void insertUpdate(DocumentEvent e) {
		    	update();
		    }
		    @Override
		    public void removeUpdate(DocumentEvent e) {
		    	update();
		    }
		    @Override
		    public void changedUpdate(DocumentEvent e) {
		    	update();
		    }
		    public void update(){
		    	try{
		    		int intValue = Integer.parseInt(textField_3.getText());
		    		xeon.setServerTimeout(intValue);
		    		xeon.getDefaultConfigManager().saveConfiguration();
		    	}catch(NumberFormatException e){
		    		
		    	}
		    }
		});
		
		JButton button_5 = textField_3.generateMinusButton();
		GridBagConstraints gbc_button_5 = new GridBagConstraints();
		gbc_button_5.insets = new Insets(0, 0, 5, 5);
		gbc_button_5.gridx = 3;
		gbc_button_5.gridy = 14;
		panel_1.add(button_5, gbc_button_5);
		
		JButton button_6 = textField_3.generatePlusButton();
		GridBagConstraints gbc_button_6 = new GridBagConstraints();
		gbc_button_6.insets = new Insets(0, 0, 5, 5);
		gbc_button_6.gridx = 4;
		gbc_button_6.gridy = 14;
		panel_1.add(button_6, gbc_button_6);
		
		JLabel lblSpacer = new JLabel(" ");
		GridBagConstraints gbc_lblSpacer = new GridBagConstraints();
		gbc_lblSpacer.anchor = GridBagConstraints.EAST;
		gbc_lblSpacer.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpacer.gridx = 1;
		gbc_lblSpacer.gridy = 15;
		panel_1.add(lblSpacer, gbc_lblSpacer);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblLogo = new GridBagConstraints();
		gbc_lblLogo.anchor = GridBagConstraints.EAST;
		gbc_lblLogo.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogo.gridx = 1;
		gbc_lblLogo.gridy = 16;
		panel_1.add(lblLogo, gbc_lblLogo);
		
		JLabel lblLogoPath = new JLabel("Logo path");
		GridBagConstraints gbc_lblLogoPath = new GridBagConstraints();
		gbc_lblLogoPath.anchor = GridBagConstraints.EAST;
		gbc_lblLogoPath.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogoPath.gridx = 1;
		gbc_lblLogoPath.gridy = 17;
		panel_1.add(lblLogoPath, gbc_lblLogoPath);
		
		textFieldLogoPath = new JTextField(xeon.getLogoFile().getPath());
		textFieldLogoPath.setEditable(false);
		textFieldLogoPath.setColumns(10);
		GridBagConstraints gbc_textFieldLogoPath = new GridBagConstraints();
		gbc_textFieldLogoPath.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldLogoPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldLogoPath.gridx = 2;
		gbc_textFieldLogoPath.gridy = 17;
		panel_1.add(textFieldLogoPath, gbc_textFieldLogoPath);
		
		JButton button_LogoPath = new JButton("...");
		button_LogoPath.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser filesave = new JFileChooser();
				filesave.setCurrentDirectory(xeon.getLogoFile().getAbsoluteFile().getParentFile());
				filesave.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png", "gif", "bmp"));
		        int returnVal = filesave.showOpenDialog(thisObj);
		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		                File file = filesave.getSelectedFile();
		                if(file.exists() && !file.isDirectory()){
		                	xeon.setLogoFile(file);
			                xeon.getMainWindow().getDesktopPane().loadLogo();
			                textFieldLogoPath.setText(file.getPath());
			                xeon.getDefaultConfigManager().saveConfiguration();
		                }
		        }
				
				
			}
		});
		GridBagConstraints gbc_button_LogoPath = new GridBagConstraints();
		gbc_button_LogoPath.insets = new Insets(0, 0, 5, 5);
		gbc_button_LogoPath.gridx = 3;
		gbc_button_LogoPath.gridy = 17;
		panel_1.add(button_LogoPath, gbc_button_LogoPath);
		
		JLabel lblLogoSize = new JLabel("Size");
		GridBagConstraints gbc_lblLogoSize = new GridBagConstraints();
		gbc_lblLogoSize.anchor = GridBagConstraints.EAST;
		gbc_lblLogoSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogoSize.gridx = 1;
		gbc_lblLogoSize.gridy = 18;
		panel_1.add(lblLogoSize, gbc_lblLogoSize);
		
		sizeSlider = new JSlider(0, 100, xeon.getLogoSize());
		sizeSlider.addChangeListener(new SizeSliderListener(xeon));
		GridBagConstraints gbc_sizeSlider = new GridBagConstraints();
		gbc_sizeSlider.anchor = GridBagConstraints.EAST;
		gbc_sizeSlider.insets = new Insets(0, 0, 5, 5);
		gbc_sizeSlider.gridx = 2;
		gbc_sizeSlider.gridy = 18;
		panel_1.add(sizeSlider, gbc_sizeSlider);
		
		JLabel lblLogoOpacity = new JLabel("Opacity");
        GridBagConstraints gbc_lblLogoOpacity = new GridBagConstraints();
        gbc_lblLogoOpacity.anchor = GridBagConstraints.EAST;
        gbc_lblLogoOpacity.insets = new Insets(0, 0, 5, 5);
        gbc_lblLogoOpacity.gridx = 1;
        gbc_lblLogoOpacity.gridy = 19;
        panel_1.add(lblLogoOpacity, gbc_lblLogoOpacity);
        
        opacitySlider = new JSlider(0, 100, xeon.getLogoOpacity());
        opacitySlider.addChangeListener(new OpacitySliderListener(xeon));
        //opacitySlider.setPaintTicks(true);
        //opacitySlider.setMajorTickSpacing(100);
        //opacitySlider.setPaintLabels(true);
        GridBagConstraints gbc_opacitySlider = new GridBagConstraints();
        gbc_opacitySlider.anchor = GridBagConstraints.EAST;
        gbc_opacitySlider.insets = new Insets(0, 0, 5, 5);
        gbc_opacitySlider.gridx = 2;
        gbc_opacitySlider.gridy = 19;
        panel_1.add(opacitySlider, gbc_opacitySlider);
        
        JButton button_9 = new JButton("Reset background logo");
        button_9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                xeon.setLogoFile(new File(xeon.getDefaultLogoPath()));
                xeon.getMainWindow().getDesktopPane().loadLogo();
                xeon.setLogoSize(50);
                xeon.setLogoOpacity(100);
                sizeSlider.setValue(50);
                opacitySlider.setValue(100);
                xeon.getMainWindow().getDesktopPane().repaint();
                xeon.getDefaultConfigManager().saveConfiguration();
                
            }
        });
        GridBagConstraints gbc_button_9 = new GridBagConstraints();
        gbc_button_9.fill = GridBagConstraints.HORIZONTAL;
        gbc_button_9.insets = new Insets(0, 0, 5, 5);
        gbc_button_9.gridx = 2;
        gbc_button_9.gridy = 20;
        panel_1.add(button_9, gbc_button_9);
		
		JLabel label_7 = new JLabel(" ");
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.insets = new Insets(0, 0, 0, 5);
		gbc_label_7.gridx = 1;
		gbc_label_7.gridy = 21;
		panel_1.add(label_7, gbc_label_7);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton button_1 = new JButton("Close");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		panel_2.add(button_1);
		
		loadBannedPorts();
		
	}
	
	public void loadBannedPorts(){
		
	    model.clear();
	    
		List<String> listOfBannedPorts = xeon.getCardManager().getScanner().getBannedPorts();
		
		for(int i = 0; i < listOfBannedPorts.size(); i ++){

			model.addElement(listOfBannedPorts.get(i));
			
		}
		
	}

}
