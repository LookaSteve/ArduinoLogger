package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;

import Logger.DataSet;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.JList;

public class RawDataExplorer extends JFrame {

    private JPanel contentPane;
    private DataSet dataSet;
    private JList list;
    private DefaultListModel listModel;

    public RawDataExplorer(DataSet dataSet) {
        this.dataSet = dataSet;
        setTitle("Raw Data Explorer");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 400, 500);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        JScrollPane scrollPane = new JScrollPane();
        contentPane.add(scrollPane, BorderLayout.CENTER);
        
        listModel = new DefaultListModel();
        list = new JList(listModel);
        scrollPane.setViewportView(list);
        
        displayData();
        
    }
    
    public void displayData(){
        
        listModel.clear();
        
        for(int i = 0; i < dataSet.getDataLines().size(); i++){
            
            String line = " " + i + "      " + dataSet.getDataLines().get(i).getTime() + "   " + dataSet.getDataLines().get(i).getRawData() + System.lineSeparator();
            
            if(dataSet.getDataLines().get(i).getTime() != null){
                
                line = " " + i + " |  [" + dataSet.getDataLines().get(i).getTime() + "]  " + dataSet.getDataLines().get(i).getRawData() + System.lineSeparator();
                
            }else{
                
                line = " " + i + " |    " + dataSet.getDataLines().get(i).getRawData() + System.lineSeparator();
                
            }
            
            listModel.addElement(line);
            
        }
        
    }

}
