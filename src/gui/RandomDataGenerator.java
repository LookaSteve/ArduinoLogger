package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.NumericField;
import Modules.Tools;
import core.Xeon;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RandomDataGenerator extends JFrame {

	private JPanel contentPane;
	private NumericField textField;
	private NumericField textField_1;
	private NumericField textField_2;
	
	private Xeon xeon;
	private NumericField textField_3;

	public RandomDataGenerator(final Xeon xeon) {
		this.xeon = xeon;
		setResizable(false);
		setTitle("Random Data Generator");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 375);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		final JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{96, 225, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{29, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblRange = new JLabel("Minimum index ");
		GridBagConstraints gbc_lblRange = new GridBagConstraints();
		gbc_lblRange.anchor = GridBagConstraints.EAST;
		gbc_lblRange.insets = new Insets(0, 0, 5, 5);
		gbc_lblRange.gridx = 0;
		gbc_lblRange.gridy = 1;
		panel.add(lblRange, gbc_lblRange);
		
		textField = new NumericField();
		textField.setValue(0);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton button = textField.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 2;
		gbc_button.gridy = 1;
		panel.add(button, gbc_button);
		
		JButton button_2 = textField.generatePlusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.insets = new Insets(0, 0, 5, 0);
		gbc_button_2.gridx = 3;
		gbc_button_2.gridy = 1;
		panel.add(button_2, gbc_button_2);
		
		JLabel lblMaximumIndex = new JLabel("Maximum index ");
		GridBagConstraints gbc_lblMaximumIndex = new GridBagConstraints();
		gbc_lblMaximumIndex.anchor = GridBagConstraints.EAST;
		gbc_lblMaximumIndex.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaximumIndex.gridx = 0;
		gbc_lblMaximumIndex.gridy = 2;
		panel.add(lblMaximumIndex, gbc_lblMaximumIndex);
		
		textField_1 = new NumericField();
		textField_1.setValue(1);
		textField_1.setColumns(10);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		panel.add(textField_1, gbc_textField_1);
		
		JButton button_1 = textField_1.generateMinusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 2;
		gbc_button_1.gridy = 2;
		panel.add(button_1, gbc_button_1);
		
		JButton button_3 = textField_1.generatePlusButton();
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.insets = new Insets(0, 0, 5, 0);
		gbc_button_3.gridx = 3;
		gbc_button_3.gridy = 2;
		panel.add(button_3, gbc_button_3);
		
		JButton btnGenerateStringsOn = new JButton("Generate strings");
		btnGenerateStringsOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int minRange = textField.getValue();
				
				int maxRange = textField_1.getValue();
				
				String fakeDataString = "";
				
				if(minRange > maxRange){
					
					JOptionPane.showMessageDialog(panel, "Incorrect index range", "Error", JOptionPane.ERROR_MESSAGE);
					
					return;
					
				}
				
				if(textField_3.getValue() > textField_2.getValue()){
					
					JOptionPane.showMessageDialog(panel, "Incorrect value range", "Error", JOptionPane.ERROR_MESSAGE);
					
					return;
					
				}
				
				if(minRange > 0){
					
					for(int i = 0; i <= minRange; i ++){
						
						fakeDataString+=xeon.getDataSeparator();
						
					}
					
				}
				
				for(int i = minRange; i <= maxRange; i ++){
					
					fakeDataString+=Tools.randString()+xeon.getDataSeparator();
					
				}
				
				xeon.getDataProcessor().processData(fakeDataString + "\r\n",xeon.getDataProcessor().getLocalSource());
				
			}
		});
		
		JButton btnGenerateDoubles = new JButton("Generate doubles");
		btnGenerateDoubles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int minRange = textField.getValue();
				
				int maxRange = textField_1.getValue();
				
				String fakeDataString = "";
				
				if(minRange > maxRange){
					
					JOptionPane.showMessageDialog(panel, "Incorrect index range", "Error", JOptionPane.ERROR_MESSAGE);
					
					return;
					
				}
				
				if(textField_3.getValue() > textField_2.getValue()){
					
					JOptionPane.showMessageDialog(panel, "Incorrect value range", "Error", JOptionPane.ERROR_MESSAGE);
					
					return;
					
				}
				
				if(minRange > 0){
					
					for(int i = 0; i <= minRange; i ++){
						
						fakeDataString+=xeon.getDataSeparator();
						
					}
					
				}
				
				for(int i = minRange; i <= maxRange; i ++){
					
					fakeDataString+=Tools.randDouble(textField_3.getValue(),textField_2.getValue() + 1)+xeon.getDataSeparator();
					
				}
				
				xeon.getDataProcessor().processData(fakeDataString+ "\r\n",xeon.getDataProcessor().getLocalSource());
				
			}
		});
		
		JButton btnGenerateIntegersOn = new JButton("Generate integers");
		btnGenerateIntegersOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int minRange = textField.getValue();
				
				int maxRange = textField_1.getValue();
				
				String fakeDataString = "";
				
				if(minRange > maxRange){
					
					JOptionPane.showMessageDialog(panel, "Incorrect index range", "Error", JOptionPane.ERROR_MESSAGE);
					
					return;
					
				}
				
				if(textField_3.getValue() > textField_2.getValue()){
					
					JOptionPane.showMessageDialog(panel, "Incorrect value range", "Error", JOptionPane.ERROR_MESSAGE);
					
					return;
					
				}
				
				if(minRange > 0){
					
					for(int i = 0; i <= minRange; i ++){
						
						fakeDataString+=xeon.getDataSeparator();
						
					}
					
				}
				
				for(int i = minRange; i <= maxRange; i ++){
					
					fakeDataString+=Tools.randInt(textField_3.getValue(),textField_2.getValue() + 1)+xeon.getDataSeparator();
					
				}
				
				xeon.getDataProcessor().processData(fakeDataString+ "\r\n",xeon.getDataProcessor().getLocalSource());
				
			}
		});
		
		textField_2 = new NumericField();
		textField_2.setValue(100);
		textField_2.setColumns(10);
		
		JButton button_4 = textField_2.generateMinusButton();
		
		JLabel lblMinimumValue = new JLabel("Minimum value ");
		GridBagConstraints gbc_lblMinimumValue = new GridBagConstraints();
		gbc_lblMinimumValue.anchor = GridBagConstraints.EAST;
		gbc_lblMinimumValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinimumValue.gridx = 0;
		gbc_lblMinimumValue.gridy = 3;
		panel.add(lblMinimumValue, gbc_lblMinimumValue);
		
		textField_3 = new NumericField();
		textField_3.setValue(0);
		textField_3.setColumns(10);
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 3;
		panel.add(textField_3, gbc_textField_3);
		
		JButton button_6 = textField_3.generateMinusButton();
		GridBagConstraints gbc_button_6 = new GridBagConstraints();
		gbc_button_6.insets = new Insets(0, 0, 5, 5);
		gbc_button_6.gridx = 2;
		gbc_button_6.gridy = 3;
		panel.add(button_6, gbc_button_6);
		
		JButton button_7 = textField_3.generatePlusButton();
		GridBagConstraints gbc_button_7 = new GridBagConstraints();
		gbc_button_7.insets = new Insets(0, 0, 5, 0);
		gbc_button_7.gridx = 3;
		gbc_button_7.gridy = 3;
		panel.add(button_7, gbc_button_7);
		
		JLabel lblMaximumValue = new JLabel("Maximum value ");
		GridBagConstraints gbc_lblMaximumValue = new GridBagConstraints();
		gbc_lblMaximumValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaximumValue.anchor = GridBagConstraints.EAST;
		gbc_lblMaximumValue.gridx = 0;
		gbc_lblMaximumValue.gridy = 4;
		panel.add(lblMaximumValue, gbc_lblMaximumValue);
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 4;
		panel.add(textField_2, gbc_textField_2);
		GridBagConstraints gbc_button_4 = new GridBagConstraints();
		gbc_button_4.insets = new Insets(0, 0, 5, 5);
		gbc_button_4.gridx = 2;
		gbc_button_4.gridy = 4;
		panel.add(button_4, gbc_button_4);
		
		JButton button_5 = textField_2.generatePlusButton();
		GridBagConstraints gbc_button_5 = new GridBagConstraints();
		gbc_button_5.insets = new Insets(0, 0, 5, 0);
		gbc_button_5.gridx = 3;
		gbc_button_5.gridy = 4;
		panel.add(button_5, gbc_button_5);
		GridBagConstraints gbc_btnGenerateIntegersOn = new GridBagConstraints();
		gbc_btnGenerateIntegersOn.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGenerateIntegersOn.insets = new Insets(0, 0, 5, 5);
		gbc_btnGenerateIntegersOn.anchor = GridBagConstraints.NORTH;
		gbc_btnGenerateIntegersOn.gridx = 1;
		gbc_btnGenerateIntegersOn.gridy = 5;
		panel.add(btnGenerateIntegersOn, gbc_btnGenerateIntegersOn);
		GridBagConstraints gbc_btnGenerateDoubles = new GridBagConstraints();
		gbc_btnGenerateDoubles.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGenerateDoubles.insets = new Insets(0, 0, 5, 5);
		gbc_btnGenerateDoubles.gridx = 1;
		gbc_btnGenerateDoubles.gridy = 6;
		panel.add(btnGenerateDoubles, gbc_btnGenerateDoubles);
		GridBagConstraints gbc_btnGenerateStringsOn = new GridBagConstraints();
		gbc_btnGenerateStringsOn.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGenerateStringsOn.insets = new Insets(0, 0, 0, 5);
		gbc_btnGenerateStringsOn.gridx = 1;
		gbc_btnGenerateStringsOn.gridy = 7;
		panel.add(btnGenerateStringsOn, gbc_btnGenerateStringsOn);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblRandomDataGenerator = new JLabel("Random Data Generator");
		lblRandomDataGenerator.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
		panel_1.add(lblRandomDataGenerator);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		panel_2.add(btnClose);
	}

}
