package gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.geom.Rectangle2D;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.Minute;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

import Logger.DataSet;
import Modules.Tools;
import core.Xeon;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ReviewGraph extends JFrame implements ChartMouseListener {

	private JPanel graphPanel;
	
	private ChartPanel chartPanel;
	
    private Crosshair xCrosshair;

    private Crosshair yCrosshair;
	
	private JPanel contentPane;
	
	private DataSet dataSet;
	
	private Xeon xeon;

	public ReviewGraph(final Xeon xeon,DataSet ds, final int index) {
		this.xeon = xeon;
		this.dataSet = ds;
		if(index < ds.getHeaders().size()){
		    setTitle(ds.getHeaders().get(index)+" (Channel " + index + ")");
		}else{
		    setTitle("Channel " + index + " graph");
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
        final XYDataset dataset = createDataset(ds,index);
        final JFreeChart chart = createChart(ds,dataset,index);
        final ChartPanel chartPanel = new ChartPanel(chart);
        this.chartPanel = chartPanel;
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        chartPanel.setMouseZoomable(true, false);
        chartPanel.addChartMouseListener(this);
        CrosshairOverlay crosshairOverlay = new CrosshairOverlay();
        this.xCrosshair = new Crosshair(Double.NaN, Color.GRAY, new BasicStroke(0f));
        this.xCrosshair.setLabelVisible(true);
        this.yCrosshair = new Crosshair(Double.NaN, Color.GRAY, new BasicStroke(0f));
        this.yCrosshair.setLabelVisible(true);
        crosshairOverlay.addDomainCrosshair(xCrosshair);
        crosshairOverlay.addRangeCrosshair(yCrosshair);
        chartPanel.addOverlay(crosshairOverlay);
        graphPanel = new JPanel();
        graphPanel.setLayout(new BorderLayout(0, 0));
        graphPanel.add(chartPanel);
        setContentPane(graphPanel);
        
        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        graphPanel.add(panel, BorderLayout.SOUTH);
        panel.setLayout(new BorderLayout(0, 0));
        
        JButton btnSendToWorkspace = new JButton("Send to workspace");
        btnSendToWorkspace.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		JInternalFrame jif = new JInternalFrame();
        		if(index < ds.getHeaders().size()){
                    jif.setTitle(ds.getHeaders().get(index)+" (Channel " + index + ")");
                }else{
                    jif.setTitle("Channel " + index + " graph");
                }
        		jif.setResizable(true);
        		jif.setMaximizable(true);
        		jif.setIconifiable(true);
        		jif.setClosable(true);
        		jif.setBounds(15, 16, 500, 400);
        		xeon.getMainWindow().getDesktopPane().add(jif);
        		jif.getContentPane().add(chartPanel);
        		jif.setVisible(true);
        		dispose();
        		
        	}
        });
        panel.add(btnSendToWorkspace, BorderLayout.EAST);
        
        JButton btnShowRawData = new JButton("Show raw data");
        btnShowRawData.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		
        		RawDataWindow rdw = new RawDataWindow(dataSet, index);
				rdw.setVisible(true);
        		
        	}
        });
        panel.add(btnShowRawData, BorderLayout.WEST);

    }
	
	private JFreeChart createChart(DataSet ds,final XYDataset dataset,int index) {

	    String chartTitle = "";
	    String value = "Value";
	    
	    if(index < ds.getHeaders().size()){
	        chartTitle = ds.getHeaders().get(index) + " graph";
	        value = ds.getHeaders().get(index);
        }else{
            chartTitle = "Channel " + index + " graph";
        }
	    
        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
            chartTitle,
            "Time", 
            value,
            dataset,
            true,
            true,
            false
        );

        chart.setBackgroundPaint(Color.white);
        
//        final StandardLegend sl = (StandardLegend) chart.getLegend();
  //      sl.setDisplaySeriesShapes(true);

        final XYPlot plot = chart.getXYPlot();
        //plot.setOutlinePaint(null);
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(false);
        
        final XYItemRenderer renderer = plot.getRenderer();
        if (renderer instanceof StandardXYItemRenderer) {
            final StandardXYItemRenderer rr = (StandardXYItemRenderer) renderer;
            //rr.setPlotShapes(true);
            rr.setShapesFilled(true);
            renderer.setSeriesStroke(0, new BasicStroke(2.0f));
            renderer.setSeriesStroke(1, new BasicStroke(2.0f));
           }
        
        final DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("hh:mm ss.SSS"));
        
        return chart;

    }
    
    /**
     * Creates a sample dataset.
     *
     * @return the dataset.
     */
    private XYDataset createDataset(DataSet ds,int index) {

        final TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.setDomainIsPointsInTime(true);
        
        String dataSetTitle = "";
        
        if(index < ds.getHeaders().size()){
            dataSetTitle = ds.getHeaders().get(index);
        }else{
            dataSetTitle = "Channel " + index;
        }
        
        final TimeSeries s1 = new TimeSeries(dataSetTitle, Millisecond.class);
        
        for(int i = 0 ; i < dataSet.getDataLines().size(); i ++){
        	
            //Get time
            String timeStr = dataSet.getDataLines().get(i).getTime();
            
            Millisecond millis = null;
            //If no timestring is provided
            if(timeStr == null){
                //Use LOG start date if available, else will use 1970 default value
                Calendar cal = Calendar.getInstance();
                cal.setTime(dataSet.getLogDate());
                cal.add(Calendar.MILLISECOND, i);
                millis = new Millisecond(cal.getTime());
                //System.out.println(dataSet.getDataLines().get(i).getRawData() + " - HAD A NULL AT INDEX - " + i);
            }
            else if(Tools.isInt(timeStr)){
                //If we are looking at EMS like data
                millis = new Millisecond(new Date(Tools.cleanInputInt(dataSet.getDataLines().get(i).getTime())));
            }else{
                //If a correct LOG compatible string is provided
                int [] timeArray = Tools.parseTime(dataSet.getDataLines().get(i).getTime());
                Calendar cal = Calendar.getInstance();
                cal.setTime(dataSet.getLogDate());
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                int day = cal.get(Calendar.DAY_OF_MONTH);
                millis = new Millisecond(timeArray[3],timeArray[2],timeArray[1],timeArray[0],day, month, year);
            }
			
			double dataDouble = 0;
			try {
				
				dataDouble = Double.parseDouble(dataSet.getDataLines().get(i).safeGetDataList(index));
				
		    }catch (NumberFormatException e) {
		        
		    }
			
			//System.out.println(millis.getStart().getTime());
			
			s1.addOrUpdate(millis, dataDouble);
		
        }
        
        dataset.addSeries(s1);

        return dataset;

    }
    
    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
        // ignore
    }

    @Override
    public void chartMouseMoved(ChartMouseEvent event) {
        Rectangle2D dataArea = chartPanel.getScreenDataArea();
        JFreeChart chart = event.getChart();
        XYPlot plot = (XYPlot) chart.getPlot();
        ValueAxis xAxis = plot.getDomainAxis();
        double x = xAxis.java2DToValue(event.getTrigger().getX(), dataArea, 
                RectangleEdge.BOTTOM);
        double y = DatasetUtilities.findYValue(plot.getDataset(), 0, x);
        this.xCrosshair.setValue(x);
        this.yCrosshair.setValue(y);
    }


}
