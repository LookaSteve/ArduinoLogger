package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.NumericField;
import Modules.TimerEvent;
import Modules.Tools;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TimeEventEditor extends JFrame {

	private JPanel contentPane;
	private JTextField txtNewEvent;
	private NumericField textField_1;
	private JTextField textField_2;
	private NumericField textField_3;

	private Color eventColor = Color.white;
	private TimeEventEditor thisObj = this;
	
	private EventScheduler es;
	private JCheckBox checkBox;
	
	private int idList = -1;
	
	public TimeEventEditor(final EventScheduler es) {
		
		this.es = es;
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 343);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		initInterface("New Event");
		
	}
	
	public TimeEventEditor(final EventScheduler es, int idList,String name, long executionTime, String command, Color color, boolean isPostZero) {
		
		this.es = es;
		this.idList = idList;
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 343);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		eventColor = color;
		
		initInterface(name);
		
		textField_2.setText(command);
		checkBox.setSelected(isPostZero);
		
		double seconds =  executionTime / 1000.0;
		double miliseconds = executionTime - (((int)(seconds)) * 1000);
		
		textField_1.setText((int)(seconds) + "");
		textField_3.setText((int)(miliseconds) + "");
		
	}
	
	private void initInterface(String eventName){
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewEvent = new JLabel(eventName);
		lblNewEvent.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblNewEvent);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(idList == -1){
					
					long time = (Tools.cleanInputInt(textField_1.getText()) * 1000) + Tools.cleanInputInt(textField_3.getText());
					TimerEvent te = new TimerEvent(es.getXeon(),es,txtNewEvent.getText(), time, textField_2.getText(), getEventColor(),checkBox.isSelected());
					es.getCellRenderer().getListOfEvents().add(te);
					
				}else{
					if(es.getEventListModel().size()-1 >= idList){
						
						long time = (Tools.cleanInputInt(textField_1.getText()) * 1000) + Tools.cleanInputInt(textField_3.getText());
						TimerEvent te = new TimerEvent(es.getXeon(),es,txtNewEvent.getText(), time, textField_2.getText(), getEventColor(),checkBox.isSelected());
						es.getCellRenderer().getListOfEvents().set(idList, te);
					}
				}
				
				es.sortEvents();
				dispose();
				
			}
		});
		panel_1.add(btnAdd, BorderLayout.EAST);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.CENTER);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel label = new JLabel(" ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel_2.add(label, gbc_label);
		
		JLabel lblName = new JLabel(" Name ");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 1;
		panel_2.add(lblName, gbc_lblName);
		
		txtNewEvent = new JTextField();
		txtNewEvent.setText(eventName);
		GridBagConstraints gbc_txtNewEvent = new GridBagConstraints();
		gbc_txtNewEvent.insets = new Insets(0, 0, 5, 5);
		gbc_txtNewEvent.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNewEvent.gridx = 1;
		gbc_txtNewEvent.gridy = 1;
		panel_2.add(txtNewEvent, gbc_txtNewEvent);
		txtNewEvent.setColumns(10);
		
		JLabel lblExectionTime = new JLabel(" Execution time (s) ");
		GridBagConstraints gbc_lblExectionTime = new GridBagConstraints();
		gbc_lblExectionTime.insets = new Insets(0, 0, 5, 5);
		gbc_lblExectionTime.anchor = GridBagConstraints.EAST;
		gbc_lblExectionTime.gridx = 0;
		gbc_lblExectionTime.gridy = 2;
		panel_2.add(lblExectionTime, gbc_lblExectionTime);
		
		textField_1 = new NumericField();
		textField_1.setValue(0);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		panel_2.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		JButton button = textField_1.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 2;
		gbc_button.gridy = 2;
		panel_2.add(button, gbc_button);
		
		JButton button_2 = textField_1.generatePlusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.insets = new Insets(0, 0, 5, 0);
		gbc_button_2.gridx = 3;
		gbc_button_2.gridy = 2;
		panel_2.add(button_2, gbc_button_2);
		
		JLabel lblExectionTimems = new JLabel(" Execution time (ms) ");
		GridBagConstraints gbc_lblExectionTimems = new GridBagConstraints();
		gbc_lblExectionTimems.anchor = GridBagConstraints.EAST;
		gbc_lblExectionTimems.insets = new Insets(0, 0, 5, 5);
		gbc_lblExectionTimems.gridx = 0;
		gbc_lblExectionTimems.gridy = 3;
		panel_2.add(lblExectionTimems, gbc_lblExectionTimems);
		
		textField_3 = new NumericField();
		textField_3.setValue(0);
		textField_3.setColumns(10);
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 3;
		panel_2.add(textField_3, gbc_textField_3);
		
		JButton button_1 = textField_3.generateMinusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 2;
		gbc_button_1.gridy = 3;
		panel_2.add(button_1, gbc_button_1);
		
		JButton button_3 = textField_3.generatePlusButton();
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.insets = new Insets(0, 0, 5, 0);
		gbc_button_3.gridx = 3;
		gbc_button_3.gridy = 3;
		panel_2.add(button_3, gbc_button_3);
		
		JLabel lblAfterT = new JLabel("After T - 0");
		GridBagConstraints gbc_lblAfterT = new GridBagConstraints();
		gbc_lblAfterT.insets = new Insets(0, 0, 5, 5);
		gbc_lblAfterT.gridx = 0;
		gbc_lblAfterT.gridy = 4;
		panel_2.add(lblAfterT, gbc_lblAfterT);
		
		checkBox = new JCheckBox("");
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.anchor = GridBagConstraints.WEST;
		gbc_checkBox.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox.gridx = 1;
		gbc_checkBox.gridy = 4;
		panel_2.add(checkBox, gbc_checkBox);
		
		JLabel lblSerialCommand = new JLabel(" Serial command ");
		GridBagConstraints gbc_lblSerialCommand = new GridBagConstraints();
		gbc_lblSerialCommand.insets = new Insets(0, 0, 5, 5);
		gbc_lblSerialCommand.anchor = GridBagConstraints.EAST;
		gbc_lblSerialCommand.gridx = 0;
		gbc_lblSerialCommand.gridy = 5;
		panel_2.add(lblSerialCommand, gbc_lblSerialCommand);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 5;
		panel_2.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		JLabel lblEventColor = new JLabel("Event Color ");
		GridBagConstraints gbc_lblEventColor = new GridBagConstraints();
		gbc_lblEventColor.anchor = GridBagConstraints.EAST;
		gbc_lblEventColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblEventColor.gridx = 0;
		gbc_lblEventColor.gridy = 6;
		panel_2.add(lblEventColor, gbc_lblEventColor);
		
		final JButton colorBtn = new JButton("Choose Color");
		colorBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Color newColor = JColorChooser.showDialog(thisObj,"Choose Background Color",Color.red);
				if(newColor == null){
					newColor = Color.red;
				}
				colorBtn.setBackground(newColor);
				colorBtn.setForeground(Tools.getContrastColor(newColor));
				thisObj.setEventColor(newColor);
				
			}
		});
		colorBtn.setForeground(Tools.getContrastColor(thisObj.getEventColor()));
		colorBtn.setBackground(thisObj.getEventColor());
		GridBagConstraints gbc_button_color = new GridBagConstraints();
		gbc_button_color.fill = GridBagConstraints.HORIZONTAL;
		gbc_button_color.insets = new Insets(0, 0, 5, 5);
		gbc_button_color.gridx = 1;
		gbc_button_color.gridy = 6;
		panel_2.add(colorBtn, gbc_button_color);
		
	}

	public synchronized Color getEventColor() {
		return eventColor;
	}

	public synchronized void setEventColor(Color eventColor) {
		this.eventColor = eventColor;
	}
	
	

}
