package gui;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import java.awt.Toolkit;
import javax.swing.JSplitPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import Logger.DataSet;
import Modules.DesktopScrollPane;
import Modules.TimerEvent;
import card.Card;
import card.CardManager;
import core.Xeon;

import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.InputEvent;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JList;


public class DenebUI {
	
    private Xeon xeon;
	private DenebUI thisObj = this;
	
	private String denebVersion = "Beta 0.4.2";
	private String denebLastBuild = "24/11/2019";
	
	private InternalFrameManager internalFrameManager;
	
	
	//Windows
	
	private ConfigSettings configSettings;
	private GeneralSettings generalSettings;
	private About about;
	private JLabel labelStatus;
	private JLabel labelSave;
	private JSplitPane splitPane;
	private CustomDesktopPane desktopPane;
    public JFrame frmDenebRcEdition;
    private DefaultListModel listModelDetectedCards = new DefaultListModel();
    private DefaultListModel listModelConnectedCards = new DefaultListModel();
	
	public DenebUI(Xeon xeon) {
		
		this.xeon = xeon;
		
		desktopPane = new CustomDesktopPane(xeon);
		
		internalFrameManager = new InternalFrameManager(this,desktopPane); 
		
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmDenebRcEdition = new JFrame();
		frmDenebRcEdition.setTitle("Deneb RC Edition");
		frmDenebRcEdition.setBounds(0, 0, 700, 500);
		frmDenebRcEdition.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		frmDenebRcEdition.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		try {
			frmDenebRcEdition.setIconImage(ImageIO.read(new File("res/logo_small.png")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		frmDenebRcEdition.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        
		    	if(doesHaveActions()){
					if(confirmSave()){
						System.exit(0);
					}
				}else{
					System.exit(0);
				}
		    	
		    }
		});
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmDenebRcEdition.setLocation(dim.width/2-frmDenebRcEdition.getSize().width/2, dim.height/2-frmDenebRcEdition.getSize().height/2);
		
		JMenuBar menuBar = new JMenuBar();
		frmDenebRcEdition.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("Configuration");
		menuBar.add(mnFile);
		
		JMenuItem mntmNewConfiguration = new JMenuItem("New configuration");
		mntmNewConfiguration.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmNewConfiguration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(doesHaveActions()){
					if(confirmSave()){
						clearUI();
					}
				}else{
					clearUI();
				}

			}
		});
		mnFile.add(mntmNewConfiguration);
		
		JMenuItem mntmOpenConfiguration = new JMenuItem("Open configuration");
		mntmOpenConfiguration.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mntmOpenConfiguration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(doesHaveActions()){
					if(confirmSave()){
						xeon.getSaveManager().promptLoad();
					}
				}else{
					xeon.getSaveManager().promptLoad();
				}
				
				//Post load asynchronous events activation
				Thread animationThread = new Thread(){
				    public void run(){
					    try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					    for(int i = 0 ; i < internalFrameManager.getListOfIF().size(); i++){
					    	  
					    	internalFrameManager.getListOfIF().get(i).setAreActionsDeclared(true);
					    	  
					    }
				    }
				};

				animationThread.start();

			}
		});
		mnFile.add(mntmOpenConfiguration);
		
		JMenuItem mntmSaveCurrentConfiguration = new JMenuItem("Save configuration");
		mntmSaveCurrentConfiguration.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mntmSaveCurrentConfiguration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(xeon.getSaveManager().getCurrentFile() != null){
					if(xeon.getSaveManager().getCurrentFile().exists()){
						xeon.getSaveManager().silentSave();
					}else{
						xeon.getSaveManager().promptSave("Save");
					}
				}else{
					xeon.getSaveManager().promptSave("Save");
				}

			}
		});
		mnFile.add(mntmSaveCurrentConfiguration);
		
		JMenuItem mntmSaveAsNew = new JMenuItem("Save configuration as");
		mntmSaveAsNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mntmSaveAsNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				xeon.getSaveManager().promptSave("Save as");

			}
		});
		mnFile.add(mntmSaveAsNew);
		
		mnFile.addSeparator();
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(doesHaveActions()){
					if(confirmSave()){
						System.exit(0);
					}
				}else{
					System.exit(0);
				}

			}
		});
		mnFile.add(mntmExit);
		
		JMenu mnData = new JMenu("Data");
		menuBar.add(mnData);
		
		JMenuItem mntmLoadLog = new JMenuItem("Load data file (log, csv...)");
		mntmLoadLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                DataSet ds = new DataSet(xeon);
                if(!ds.promptOpenFile("Choose data file")){
                    return;
                }
                LogManagementUI lmu = new LogManagementUI(xeon,ds);
                lmu.setVisible(true);

            }
        });
        mnData.add(mntmLoadLog);
		
		mnData.addSeparator();
		
		JMenuItem mntmSetLogPath = new JMenuItem("Set log path");
		mntmSetLogPath.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				xeon.getLogManager().changeLogPath();

			}
		});
		mnData.add(mntmSetLogPath);
		
		JMenuItem mntmStartCapture = new JMenuItem("Start logging");
		mntmStartCapture.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				xeon.getLogManager().setDoLog(true);
				labelStatus.setText("   Status : Logging   ");

			}
		});
		mnData.add(mntmStartCapture);
		
		JMenuItem mntmStopLogging = new JMenuItem("Stop logging");
		mntmStopLogging.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				xeon.getLogManager().setDoLog(false);
				xeon.getLogManager().getLogWritter().close();
				labelStatus.setText("   Status : Idle   ");

			}
		});
		mnData.add(mntmStopLogging);
		
		mnData.addSeparator();
		
		JMenuItem mntmRandDatGen = new JMenuItem("Random data generator");
		mntmRandDatGen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.openRandGen();

			}
		});
		mnData.add(mntmRandDatGen);
		
		JMenuItem mntmConsole = new JMenuItem("Serial console");
		mntmConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SerialConsole console = new SerialConsole(xeon);
				console.setVisible(true);

			}
		});
		mnData.add(mntmConsole);
		
		JMenuItem mntmGlobConsole = new JMenuItem("Global console");
		mntmGlobConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				if(xeon.getDataProcessor().getGlobalConsole() != null){
					JOptionPane.showMessageDialog(desktopPane,
						    "Another global console is already opened.",
						    "Warning",
						    JOptionPane.WARNING_MESSAGE);
					return;
				}
				GlobalConsole console = new GlobalConsole(xeon);
				console.setVisible(true);

			}
		});
		mnData.add(mntmGlobConsole);
		
		JMenu mnWindows = new JMenu("Add");
		menuBar.add(mnWindows);
		
		JMenu mnAdd = new JMenu("Windows");
		mnWindows.add(mnAdd);
		
		JMenuItem mntmNumericGraph = new JMenuItem("Numeric graph");
		mntmNumericGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.promptNumericGraph();

			}
		});
		mnAdd.add(mntmNumericGraph);
		
		JMenuItem mntmStatusIndicator = new JMenuItem("Status indicator");
		mntmStatusIndicator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.promptStatusIndicator();

			}
		});
		mnAdd.add(mntmStatusIndicator);
		
		JMenuItem mntmGauge = new JMenuItem("Gauge");
		mntmGauge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.promptGauge();

			}
		});
		mnAdd.add(mntmGauge);
		
		JMenuItem mntmInputDataDisplay = new JMenuItem("Timeout Alarm");
		mntmInputDataDisplay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                internalFrameManager.addNewTimeOutAlarm(2,true,null,false);

            }
        });
		mnAdd.add(mntmInputDataDisplay);
		
		JMenuItem mntmEventScheduler = new JMenuItem("Event scheduler");
		mntmEventScheduler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.addNewEventSheduler("Event Sheduler",new ArrayList<TimerEvent>(),null);

			}
		});
		mnAdd.add(mntmEventScheduler);
		
		JMenu mnNetwork = new JMenu("Network");
		mnWindows.add(mnNetwork);
		
		JMenuItem mntmServer = new JMenuItem("Server");
		mntmServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.promptServer();

			}
		});
		mnNetwork.add(mntmServer);
		
		JMenuItem mntmClient = new JMenuItem("Client");
		mntmClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				internalFrameManager.promptClient();

			}
		});
		mnNetwork.add(mntmClient);
		
		JMenu mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);
		
		JMenuItem mntmEditConfigurationSettings = new JMenuItem("Configuration settings");
		mntmEditConfigurationSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(configSettings != null){
					if(!configSettings.isVisible()){
						configSettings = new ConfigSettings(xeon);
						configSettings.setVisible(true);
					}else{
						configSettings.requestFocus();
					}
				}else{
					configSettings = new ConfigSettings(xeon);
					configSettings.setVisible(true);
				}

			}
		});
		mnSettings.add(mntmEditConfigurationSettings);
		
		JMenuItem mntmGeneralSettings = new JMenuItem("General settings");
		mntmGeneralSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(generalSettings != null){
					if(!generalSettings.isVisible()){
						generalSettings = new GeneralSettings(xeon);
						generalSettings.setVisible(true);
					}else{
						generalSettings.requestFocus();
					}
				}else{
					generalSettings = new GeneralSettings(xeon);
					generalSettings.setVisible(true);
				}
				
			}
		});
		mnSettings.add(mntmGeneralSettings);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(about != null){
					if(!about.isVisible()){
						about = new About(thisObj);
						about.setVisible(true);
					}else{
						about.requestFocus();
					}
				}else{
					about = new About(thisObj);
					about.setVisible(true);
				}
				
			}
		});
		mnSettings.add(mntmAbout);
		
		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.9);
		frmDenebRcEdition.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.5);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setRightComponent(splitPane_1);
		
		JPanel cardsPanel = new JPanel();
		splitPane_1.setLeftComponent(cardsPanel);
		cardsPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel cardPanelMenu = new JPanel();
		cardsPanel.add(cardPanelMenu, BorderLayout.NORTH);
		cardPanelMenu.setLayout(new BorderLayout(0, 0));
		
		JLabel lblListOfCards = new JLabel("   Available Cards   ");
		cardPanelMenu.add(lblListOfCards, BorderLayout.CENTER);
		lblListOfCards.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		
		JScrollPane scrollPane = new JScrollPane();
		cardsPanel.add(scrollPane, BorderLayout.CENTER);
		
		JList listDetectedCards = new JList(listModelDetectedCards);
		listDetectedCards.addMouseListener(new MouseAdapter(){
		    public void mouseClicked(MouseEvent evt){
		        JList list = (JList)evt.getSource();
		        if (evt.getClickCount() == 2) {
		            // Double-click detected
		            int index = list.locationToIndex(evt.getPoint());
		            promptConnectCard(getXeon().getCardManager().getDetectedCards().get(index));
		        } else if (evt.getClickCount() == 3){
		            // Triple-click detected
		            int index = list.locationToIndex(evt.getPoint());
		            promptConnectCard(getXeon().getCardManager().getDetectedCards().get(index));
		        }
		    }
		});
		scrollPane.setViewportView(listDetectedCards);
		
		JButton btnCardsConnect = new JButton("Connect");
        btnCardsConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                if(listDetectedCards.getSelectedIndex() >= 0){
                    promptConnectCard(getXeon().getCardManager().getDetectedCards().get(listDetectedCards.getSelectedIndex()));
                }
                
            }
        });
        cardPanelMenu.add(btnCardsConnect, BorderLayout.EAST);
        
		
		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.9);
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setRightComponent(splitPane_2);
		
		JPanel networkPanel = new JPanel();
		splitPane_2.setLeftComponent(networkPanel);
		networkPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel networkPanelMenu = new JPanel();
		networkPanel.add(networkPanelMenu, BorderLayout.NORTH);
		networkPanelMenu.setLayout(new BorderLayout(0, 0));
		
		JLabel lblClients = new JLabel("   Connected Cards   ");
		lblClients.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		networkPanelMenu.add(lblClients);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		networkPanel.add(scrollPane_1, BorderLayout.CENTER);
		
		JList listConectedCards = new JList(listModelConnectedCards);
		listConectedCards.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent evt){
                JList list = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = list.locationToIndex(evt.getPoint());
                    promptDisconnectCard(getXeon().getCardManager().getConnectedCards().get(index));
                } else if (evt.getClickCount() == 3){
                    // Triple-click detected
                    int index = list.locationToIndex(evt.getPoint());
                    promptDisconnectCard(getXeon().getCardManager().getConnectedCards().get(index));
                }
            }
        });
		scrollPane_1.setViewportView(listConectedCards);
        
        JPanel panel = new JPanel();
        networkPanelMenu.add(panel, BorderLayout.EAST);
        
        JButton btnManage = new JButton("Manage");
        btnManage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                if(listConectedCards.getSelectedIndex() >= 0){
                    CardManageUI cardManageUI = new CardManageUI(xeon, getXeon().getCardManager().getConnectedCards().get(listConectedCards.getSelectedIndex()));
                    cardManageUI.setVisible(true);
                }
                
            }
        });
        panel.setLayout(new BorderLayout(0, 0));
        panel.add(btnManage, BorderLayout.WEST);
        
        JButton buttonDisconnectCard = new JButton("Disconnect");
        panel.add(buttonDisconnectCard, BorderLayout.EAST);
        buttonDisconnectCard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                if(listConectedCards.getSelectedIndex() >= 0){
                    promptDisconnectCard(getXeon().getCardManager().getConnectedCards().get(listConectedCards.getSelectedIndex()));
                }
                
            }
        });
		
		JPanel statusPanel = new JPanel();
		splitPane_2.setRightComponent(statusPanel);
		statusPanel.setLayout(new BorderLayout(0, 0));
		
		labelStatus = new JLabel("   Status : Idle   ");
		labelStatus.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		statusPanel.add(labelStatus,BorderLayout.WEST);
		
		labelSave = new JLabel("");
		labelSave.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		statusPanel.add(labelSave,BorderLayout.EAST);
		
		DesktopScrollPane dsp = new DesktopScrollPane(desktopPane);
		splitPane.setLeftComponent(dsp);
		
		desktopPane.setBackground(xeon.getBackgroundColor());
		
		
	}
	
public void refreshDetectedCardList(){
        
        ArrayList<Card> listOfCards = xeon.getCardManager().getDetectedCards();
        
        if(listOfCards == null || listModelDetectedCards == null){
            
            return;
            
        }
        
        listModelDetectedCards.clear();
        
        for(int i = 0; i < listOfCards.size(); i ++){
            
            listModelDetectedCards.addElement("PORT : " + listOfCards.get(i).getPort());
            
        }
        
    }
    
    public void refreshConnectedCardList(){
        
        ArrayList<Card> listOfCards = xeon.getCardManager().getConnectedCards();
        
        if(listOfCards == null || listModelConnectedCards == null){
            
            return;
            
        }
        
        listModelConnectedCards.clear();
        
        for(int i = 0; i < listOfCards.size(); i ++){
            
            Card card = listOfCards.get(i);
            
            if(card.getDisplayName().equals(card.getDefaultNickname())){
                listModelConnectedCards.addElement(card.getDisplayName() + " - (" + card.getDataRate() + " Bauds)");
            }else if(card.getDisplayName().equals("")){
                listModelConnectedCards.addElement("PORT : " + card.getPort() + " (" + card.getDataRate() + " Bauds)");
            }else{
                listModelConnectedCards.addElement(card.getDisplayName()  + " (Port " + card.getPort() +", "+ card.getDataRate() + " Bauds)");
            }
            
        }
        
    }
	
	public void promptConnectCard(Card card){
	    
	    CardConnectUI cardConnectUI = new CardConnectUI(xeon, card, xeon.getDefaultBaudRate());
	    cardConnectUI.setVisible(true);
	    
	}
	
	public void promptDisconnectCard(Card card){
	    
	    int result = yesno("Disconnect " + card.getPort() + " ?");
	    if(result == JOptionPane.YES_OPTION){
            
	        //Open card
            if(card.close()){
                CardManager cardManager = getXeon().getCardManager();
                cardManager.getDetectedCards().add(card);
                cardManager.getConnectedCards().remove(card);
                refreshConnectedCardList();
                refreshDetectedCardList();
            }else{
                System.out.println("PORT  " + card.getPort() + " IS BUSY.");
            }
            
        }
        
    }
	
	public boolean confirmSave(){
		
		int result = yesnocancel("Save the current configuration ?");
		if(result == JOptionPane.YES_OPTION){
		    
		    //Check for pre loaded config
	        if(xeon.getSaveManager().getCurrentFile() != null){
	            if(xeon.getSaveManager().getCurrentFile().exists()){
	                xeon.getSaveManager().silentSave();
	                return true;
	            }
	        }
			
			return xeon.getSaveManager().promptSave("Save");
			
		}else if(result == JOptionPane.NO_OPTION){
			
			return true;
			
		}else{
			
			return false;
			
		}
		
	}
	
	public boolean doesHaveActions(){
		
		if(xeon.getNumberOfActions()==0){
			
			return false;
			
		}
		
		return true;
		
	}
	
	public void clearUI(){
		
		xeon.clearActions();
		xeon.getSaveManager().setCurrentFile(null);
		desktopPane.removeAll();
		for(int i = 0; i < internalFrameManager.getListOfIF().size(); i++){
			internalFrameManager.getListOfIF().get(i).windowClosed();
		}
		internalFrameManager.getListOfIF().clear();
		desktopPane.repaint();
		xeon.setLogPath(xeon.getDefaultSavePath());
		xeon.setLogHeader(xeon.getLogHeader());
		xeon.getPreProcessor().resetConfig();
		xeon.getSaveManager().newConfig();
		
	}
	
	public static int yesnocancel(String theMessage) {
	    int result = JOptionPane.showConfirmDialog(null, theMessage,"Warning", JOptionPane.YES_NO_CANCEL_OPTION);
	    return result;
	}
	
	public static int yesno(String theMessage) {
        int result = JOptionPane.showConfirmDialog(null, theMessage,"Warning", JOptionPane.YES_NO_OPTION);
        return result;
    }
	
	public CustomDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public void setDesktopPane(CustomDesktopPane desktopPane) {
		this.desktopPane = desktopPane;
	}

	public String getDenebVersion() {
		return denebVersion;
	}

	public void setDenebVersion(String denebVersion) {
		this.denebVersion = denebVersion;
	}

	public InternalFrameManager getInternalFrameManager() {
		return internalFrameManager;
	}

	public void setInternalFrameManager(InternalFrameManager internalFrameManager) {
		this.internalFrameManager = internalFrameManager;
	}

	public Xeon getXeon() {
		return xeon;
	}

	public void setXeon(Xeon xeon) {
		this.xeon = xeon;
	}

	public ConfigSettings getConfigSettings() {
		return configSettings;
	}

	public void setConfigSettings(ConfigSettings configSettings) {
		this.configSettings = configSettings;
	}

	public JLabel getLabelStatus() {
		return labelStatus;
	}

	public void setLabelStatus(JLabel labelStatus) {
		this.labelStatus = labelStatus;
	}

	public JLabel getLabelSave() {
		return labelSave;
	}

	public void setLabelSave(JLabel labelSave) {
		this.labelSave = labelSave;
	}

    public String getDenebLastBuild() {
        return denebLastBuild;
    }

    public void setDenebLastBuild(String denebLastBuild) {
        this.denebLastBuild = denebLastBuild;
    }

    public JSplitPane getSplitPane() {
        return splitPane;
    }

    public void setSplitPane(JSplitPane splitPane) {
        this.splitPane = splitPane;
    }
	
}
