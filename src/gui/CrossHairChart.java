package gui;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleEdge;

/**
 * A demo showing crosshairs that follow the data points on an XYPlot.
 */
public class CrossHairChart implements ChartMouseListener {

    private ChartPanel chartPanel;

    private Crosshair xCrosshair;

    private Crosshair yCrosshair;
    
    private DynamicTimeSeriesCollection dataset;

    private float lastValue;
    
    private String graphTitle;
    
    private Color lineColor;
    
    private String yAxisTitle;
    
    private Color backGroundColor;
    
    private Color GridColor;
    
    private boolean isXAxisVisible;
    
    private boolean isYAxisVisible;
    
    private boolean isRangeAuto;
    
    private int range;
    
    public CrossHairChart(String graphTitle,Color lineColor,Color backGroundColor,Color GridColor,String yAxisTitle,boolean isXAxisVisible,boolean isYAxisVisible,boolean isRangeAuto, int range) {
    	this.graphTitle = graphTitle;
    	this.lineColor = lineColor;
    	this.yAxisTitle = yAxisTitle;
    	this.backGroundColor = backGroundColor;
    	this.GridColor = GridColor;
    	this.isXAxisVisible = isXAxisVisible;
    	this.isYAxisVisible = isYAxisVisible;
    	this.isRangeAuto = isRangeAuto;
    	this.range = range;
    	createContent();
    }

    private void createContent() {
    	//DATA
    	dataset = new DynamicTimeSeriesCollection(
    	           1, 600, new Second()
    	        );
        dataset.setTimeBase(new Second());
        dataset.addSeries(new float[] { 0.0f }, 0, graphTitle);
        //System.out.println("Series count = " + dataset.getSeriesCount());
        //OBJ INIT
        JFreeChart chart = createChart(dataset);
        chart.removeLegend();
        this.chartPanel = new ChartPanel(chart);
        this.chartPanel.addChartMouseListener(this);
        CrosshairOverlay crosshairOverlay = new CrosshairOverlay();
        this.xCrosshair = new Crosshair(Double.NaN, Color.GRAY, new BasicStroke(0f));
        this.xCrosshair.setLabelVisible(true);
        this.yCrosshair = new Crosshair(Double.NaN, Color.GRAY, new BasicStroke(0f));
        this.yCrosshair.setLabelVisible(true);
        crosshairOverlay.addDomainCrosshair(xCrosshair);
        crosshairOverlay.addRangeCrosshair(yCrosshair);
        chartPanel.addOverlay(crosshairOverlay);

    }

    private JFreeChart createChart(final XYDataset dataset) {
        
    	//Chart
    	final JFreeChart result = ChartFactory.createTimeSeriesChart(graphTitle, "Time", yAxisTitle, dataset, true, true, false); 
        
        //Plot
        final XYPlot plot = result.getXYPlot();
        
        //GraphLine color
        plot.getRenderer().setSeriesPaint(0, lineColor);
        
        //Background color
        plot.setBackgroundPaint(backGroundColor);
        
        //X axis color
        plot.setRangeGridlinePaint(GridColor);
        plot.setRangeMinorGridlinePaint(GridColor);
        
        //Y axis color
        plot.setDomainMinorGridlinePaint(GridColor);
        plot.setDomainGridlinePaint(GridColor);
        
        //Range & axis setup
        
        //X Axis
        ValueAxis Xaxis = plot.getDomainAxis();
        if(isRangeAuto){
        	 Xaxis.setAutoRange(true);
        }else{
        	 Xaxis.setFixedAutoRange(range);
        }
        if(!isXAxisVisible){
        	Xaxis.setVisible(false);
        }
        
        //Y Axis
        ValueAxis Yaxis = plot.getRangeAxis();
        //axis.setRange(-200.0, 200.0);
        if(!isYAxisVisible){
        	Yaxis.setVisible(false);
        }
        return result;
     }

    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
        // ignore
    }

    @Override
    public void chartMouseMoved(ChartMouseEvent event) {
        Rectangle2D dataArea = this.chartPanel.getScreenDataArea();
        JFreeChart chart = event.getChart();
        XYPlot plot = (XYPlot) chart.getPlot();
        ValueAxis xAxis = plot.getDomainAxis();
        double x = xAxis.java2DToValue(event.getTrigger().getX(), dataArea, 
                RectangleEdge.BOTTOM);
        double y = DatasetUtilities.findYValue(plot.getDataset(), 0, x);
        this.xCrosshair.setValue(x);
        this.yCrosshair.setValue(y);
    }

	public ChartPanel getChartPanel() {
		return chartPanel;
	}

	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}

	public DynamicTimeSeriesCollection getDataset() {
		return dataset;
	}

	public void setDataset(DynamicTimeSeriesCollection dataset) {
		this.dataset = dataset;
	}

	public float getLastValue() {
		return lastValue;
	}

	public void setLastValue(float lastValue) {
		this.lastValue = lastValue;
	}

	public String getGraphTitle() {
		return graphTitle;
	}

	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}

	public Color getLineColor() {
		return lineColor;
	}

	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
	}

	public String getyAxisTitle() {
		return yAxisTitle;
	}

	public void setyAxisTitle(String yAxisTitle) {
		this.yAxisTitle = yAxisTitle;
	}

	public Color getBackGroundColor() {
		return backGroundColor;
	}

	public void setBackGroundColor(Color backGroundColor) {
		this.backGroundColor = backGroundColor;
	}

	public Color getGridColor() {
		return GridColor;
	}

	public void setGridColor(Color gridColor) {
		GridColor = gridColor;
	}

	public boolean isXAxisVisible() {
		return isXAxisVisible;
	}

	public void setXAxisVisible(boolean isXAxisVisible) {
		this.isXAxisVisible = isXAxisVisible;
	}

	public boolean isYAxisVisible() {
		return isYAxisVisible;
	}

	public void setYAxisVisible(boolean isYAxisVisible) {
		this.isYAxisVisible = isYAxisVisible;
	}

	public boolean isRangeAuto() {
		return isRangeAuto;
	}

	public void setRangeAuto(boolean isRangeAuto) {
		this.isRangeAuto = isRangeAuto;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}
    
    

}