package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.GridBagConstraints;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.Color;

public class HelpHeaderSelection extends JFrame {

    private JPanel contentPane;
    private JTextPane helpTextPane;

    public HelpHeaderSelection() {
        setTitle("Help");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        JScrollPane scrollPane = new JScrollPane();
        contentPane.add(scrollPane, BorderLayout.CENTER);
        
        helpTextPane = new JTextPane();
        helpTextPane.setEditable(false);
        scrollPane.setViewportView(helpTextPane);
        
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 0, 5, 0));
        panel.setBackground(Color.WHITE);
        scrollPane.setColumnHeaderView(panel);
        panel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblDataFileInterpretation = new JLabel("Data File Interpretation");
        panel.add(lblDataFileInterpretation);
        lblDataFileInterpretation.setBackground(Color.WHITE);
        lblDataFileInterpretation.setHorizontalAlignment(SwingConstants.CENTER);
        lblDataFileInterpretation.setFont(new Font("Tahoma", Font.PLAIN, 20));
        
        showJump();
        showTitle("Principle");
        showJump();
        showLine("Because different types of files can be parsed by the parser the file type must be known to handle the data properly.");
        showJump();
        showLine("In normal conditions the file type will be automatically guessed and you won't have to do anything.");
        showLine("But if the file type is not recognized or wrong the parsing way be done using the wrong method.");
        showJump();
        showLine("The parsing of the data itslef is done by splitting it with the data separator specified in your current configuration.");
        showLine("If the data separator is wrong or unspecified the lines will be read as a single channel and won't be graphable nor injectable.");
        showJump();
        showTitle("Bad parsing");
        showJump();
        showLine("If the data file is incorrectly parsed the data might look wrong in the \"Available channels\" pane or a parsing error might occur.");
        showLine("To parse the data file properly simply select in the drop down menu the correct file type you are trying to import.");
        showJump();
        showLine("Another cause might be the data separator, so check that the data separator in the data file is the same as the one set in DenebRC.");
        showLine("To do that go in \"Settings\">\"Configuration Settings\" and change the separator to the character or string used by your data file.");
        showJump();
        showTitle("How to recognize your file type ?");
        showJump();
        showLine("Open up the file with a text editor.");
        showLine("If the first line contains the title of the columns of data you are probably looking at a CSV file.");
        showLine("If every line is prefaced by a time in the \"hh:mm:ss:sss\" format then this is likely a native DenebRC LOG file.");
        showLine("If every line is prefaced by a time in miliseconds (or just an integer) then it's an EMS file (Elapsed Milisecond since Start).");
        
        helpTextPane.setCaretPosition(0);
        
    }
    
    // Show a empty line
    public void showJump(){
        Document doc = helpTextPane.getDocument();
        try {
          doc.insertString(doc.getLength(), "\n", null);
        } catch (BadLocationException ex) { ex.printStackTrace(); }
    }
    
    // Show an title message
    public void showTitle(String msg) {
      SimpleAttributeSet attrs = new SimpleAttributeSet();
      StyleConstants.setFontSize(attrs, 19);
      StyleConstants.setBold(attrs, true);
      StyleConstants.setForeground(attrs, Color.BLACK);
      showMsg(msg, attrs);
    }
    
    // Show an informational message
    public void showLine(String msg) {
      SimpleAttributeSet attrs = new SimpleAttributeSet();
      StyleConstants.setForeground(attrs, Color.BLACK);
      showMsg(msg, attrs);
    }

    
    // Show a text message using the specified AttributeSet
    protected void showMsg(String msg, AttributeSet attrs) {
      Document doc = helpTextPane.getDocument();
      msg += "\n";
      try {
        doc.insertString(doc.getLength(), msg, attrs);
      } catch (BadLocationException ex) { ex.printStackTrace(); }
    }

}
