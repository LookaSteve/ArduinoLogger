package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JPanel;

import Modules.NumericField;
import Modules.Tools;
import core.Xeon;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JTextArea;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class TimeoutAlarm extends InternalFrame{
    private NumericField timeOutField;
    private JPanel panelState;
    private JLabel lblState;
    private JCheckBox chckbxAutoStartOn;
    
    private Thread watcherThread = null;
    
    private long lastData = 0;
    private int timeoutSecond = 2;
    private boolean enabled = false;
    private int IO = 0;
    private int IOS = 0;
    private boolean isLOS = false;
    private boolean isMuted = false;
    
    private JButton btnActivate;
    private JPanel panel_2;
    private JLabel lblEnabledState;
    private JPanel panelEnableState;
    private JPanel panelIOS;
    private JLabel lblIOS;
    private JPanel panelSetTimeout;
    private JPanel panelSondControl;
    private JButton button;
	
	public TimeoutAlarm(Xeon xeon,int timeout,boolean autoenable,boolean muted){

		super(xeon,-1,"TIMEOUTALARM");

		timeoutSecond = timeout;
		isMuted = muted;
		
		JPanel panelFooter = new JPanel();
		getContentPane().add(panelFooter, BorderLayout.SOUTH);
		panelFooter.setLayout(new BorderLayout(0, 0));
		
		panel_2 = new JPanel();
		panelFooter.add(panel_2, BorderLayout.CENTER);
		
		btnActivate = new JButton("Enable");
		panel_2.add(btnActivate);
		
		chckbxAutoStartOn = new JCheckBox("Auto enable on data received");
		panel_2.add(chckbxAutoStartOn);
		chckbxAutoStartOn.setSelected(autoenable);
		
		panelEnableState = new JPanel();
		panelEnableState.setBackground(new Color(183,28,28));
		panelFooter.add(panelEnableState, BorderLayout.WEST);
		panelEnableState.setLayout(new BorderLayout(0, 0));
		
		lblEnabledState = new JLabel("   Disabled   ");
		lblEnabledState.setForeground(Color.WHITE);
		panelEnableState.add(lblEnabledState);
		lblEnabledState.setBackground(Color.WHITE);
		
		panelIOS = new JPanel();
		panelIOS.setBackground(new Color(183,28,28));
		panelFooter.add(panelIOS, BorderLayout.EAST);
		panelIOS.setLayout(new BorderLayout(0, 0));
		
		lblIOS = new JLabel("   0 IO/s   ");
		lblIOS.setForeground(Color.WHITE);
		panelIOS.add(lblIOS);
		chckbxAutoStartOn.addItemListener(new ItemListener() {
		    public void itemStateChanged(ItemEvent e) {
		        xeon.action();
		    }
		});
		btnActivate.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		        
		        if(enabled){
		            disableWatcher();
		        }else{
		            enableWatcher();
		        }
		        
		    }
		});
		
		JPanel panelHeader = new JPanel();
		getContentPane().add(panelHeader, BorderLayout.NORTH);
		panelHeader.setLayout(new BorderLayout(0, 0));
		
		panelSetTimeout = new JPanel();
		panelHeader.add(panelSetTimeout, BorderLayout.CENTER);
		GridBagLayout gbl_panelSetTimeout = new GridBagLayout();
		gbl_panelSetTimeout.columnWidths = new int[]{75, 146, 75, 0};
		gbl_panelSetTimeout.rowHeights = new int[]{26, 0};
		gbl_panelSetTimeout.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panelSetTimeout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panelSetTimeout.setLayout(gbl_panelSetTimeout);
		
		JLabel lblTimeout = new JLabel("Timeout : ");
		GridBagConstraints gbc_lblTimeout = new GridBagConstraints();
		gbc_lblTimeout.anchor = GridBagConstraints.EAST;
		gbc_lblTimeout.fill = GridBagConstraints.VERTICAL;
		gbc_lblTimeout.insets = new Insets(0, 0, 0, 5);
		gbc_lblTimeout.gridx = 0;
		gbc_lblTimeout.gridy = 0;
		panelSetTimeout.add(lblTimeout, gbc_lblTimeout);
		
		timeOutField = new NumericField();
		GridBagConstraints gbc_timeOutField = new GridBagConstraints();
		gbc_timeOutField.anchor = GridBagConstraints.WEST;
		gbc_timeOutField.fill = GridBagConstraints.VERTICAL;
		gbc_timeOutField.insets = new Insets(0, 0, 0, 5);
		gbc_timeOutField.gridx = 1;
		gbc_timeOutField.gridy = 0;
		panelSetTimeout.add(timeOutField, gbc_timeOutField);
		timeOutField.setValue(timeoutSecond);
		timeOutField.setColumns(10);
		timeOutField.getDocument().addDocumentListener(new DocumentListener() {
		    public void changedUpdate(DocumentEvent e) {
		        update();
		      }
		      public void removeUpdate(DocumentEvent e) {
		        update();
		      }
		      public void insertUpdate(DocumentEvent e) {
		        update();
		      }

		      public void update() {
		         if(timeOutField.getValue()>0){  
		           timeoutSecond = timeOutField.getValue();
		           xeon.action();
		         }
		      }
		    });
		
		JLabel lblSeconds = new JLabel(" Second(s)");
		GridBagConstraints gbc_lblSeconds = new GridBagConstraints();
		gbc_lblSeconds.anchor = GridBagConstraints.WEST;
		gbc_lblSeconds.fill = GridBagConstraints.VERTICAL;
		gbc_lblSeconds.gridx = 2;
		gbc_lblSeconds.gridy = 0;
		panelSetTimeout.add(lblSeconds, gbc_lblSeconds);
		
		panelSondControl = new JPanel();
		panelHeader.add(panelSondControl, BorderLayout.EAST);
		if(isMuted){
		    button = new JButton(new ImageIcon("data/Speaker_Icon.png"));
		}else{
		    button = new JButton(new ImageIcon("data/Mute_Icon.png"));
		}
		button.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		        if(isMuted){
		            button.setIcon(new ImageIcon("data/Mute_Icon.png"));
		            button.setFocusPainted(false);
		            isMuted = false;
		        }else{
		            button.setIcon(new ImageIcon("data/Speaker_Icon.png"));
		            button.setFocusPainted(false);
		            isMuted = true;
		        }
		        xeon.action();
		    }
		});
		panelSondControl.add(button);
		
		panelState = new JPanel();
		getContentPane().add(panelState, BorderLayout.CENTER);
		panelState.setLayout(new BorderLayout(0, 0));
		
		lblState = new JLabel("");
		lblState.setFont(new Font("Tahoma", Font.BOLD, 25));
		
		lblState.setHorizontalAlignment(SwingConstants.CENTER);
		panelState.add(lblState);
	
		printIdle();
	
	}
	
	@Override
	public void addData(String s){
		
	    //Auto enable
	    if(!enabled && chckbxAutoStartOn.isSelected()){
	        enableWatcher();
	    }
	    
	    if(enabled){
	        //Set last data
	        lastData = new Date().getTime();
	        
	        IO++;
	        
	    }
	    
	}
	
	@Override
    public void windowClosed(){
	    
	    disableWatcher();
	    
	}
	
	private void enableWatcher(){
	    
	    enabled = true;
	    
	    btnActivate.setText("Disable");
	    lblEnabledState.setText("   Enabled   ");
	    panelEnableState.setBackground(new Color(0, 139, 0));
	    
	    //Reset last received
	    lastData = 0;
	    
	    //Start watcher thread
	    watcherThread = new Thread() {
	        public void run() {
	            
	            int rateMark = 1;
	            
	            while(true){
	                if(lastData == 0){
	                    printAcq();
	                }else{
	                    long currentTime = new Date().getTime();
	                    if(Math.abs(currentTime-lastData)>(timeoutSecond*1000)){
	                        printLoss();
	                        isLOS = true;
	                    }else{
	                        printOk();
	                        isLOS = false;
	                    }
	                    
	                }
	                
	                try {
	                    Thread.sleep(100);
	                } catch (InterruptedException e) {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
	                }
	                
	                if(rateMark == 10){
                        IOS = IO;
                        refreshIOS();
                        IO=0;
                        rateMark = 0;
                        //If we are in LOS and we aren't muted
                        if(isLOS && !isMuted){
                            getXeon().getSoundManager().play("triple");
                        }
                    }
	                rateMark++;
	                
	            }
	        }  
	    };

	    watcherThread.start();
	    
	}
	
	private void disableWatcher(){
	    
	    if(enabled){
            
            enabled = false;
            
            btnActivate.setText("Enable");
            lblEnabledState.setText("   Disabled   ");
            panelEnableState.setBackground(new Color(183,28,28));
            
            if(watcherThread != null){
                
                watcherThread.stop();
                
                watcherThread = null;
                
            }
            
            printIdle();
            
        }
	    
	}
	
	private void refreshIOS(){
	    
	    lblIOS.setText("   "+IOS+" IO/s   ");
	    if(IOS > 0){
	        panelIOS.setBackground(new Color(0, 139, 0));
	    }else{
	        panelIOS.setBackground(new Color(183,28,28));
	    }
	    
	}
	
	private void printOk(){
	    
	    lblState.setText("OK");
	    lblState.setForeground(Color.WHITE);
	    panelState.setBackground(new Color(0, 139, 0));
	    
	}
	
	private void printIdle(){
        
        lblState.setText("IDLE");
        lblState.setForeground(Color.WHITE);
        panelState.setBackground(new Color(0,145,234));
        
    }
	
	private void printLoss(){
        
        lblState.setText("LOSS OF SIGNAL");
        lblState.setForeground(Color.WHITE);
        panelState.setBackground(new Color(183,28,28));
        
    }
	
	private void printAcq(){
        
        lblState.setText("WAITING FOR SIGNAL");
        lblState.setForeground(Color.WHITE);
        panelState.setBackground(new Color(149,117,205));
        
    }

	public boolean isAutoenable(){
	    return chckbxAutoStartOn.isSelected();
	}
	
    public int getTimeoutSecond() {
        return timeoutSecond;
    }

    public void setTimeoutSecond(int timeoutSecond) {
        this.timeoutSecond = timeoutSecond;
    }

    public boolean isMuted() {
        return isMuted;
    }

    public void setMuted(boolean isMuted) {
        this.isMuted = isMuted;
    }
	
}
