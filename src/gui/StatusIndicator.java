package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JPanel;

import Modules.StatusObject;
import Modules.Tools;
import core.Xeon;

public class StatusIndicator extends InternalFrame{

	private ArrayList<StatusObject> statusList = new ArrayList<StatusObject>();
	
	private StatusPanel statusPanel;
	
	private String title;
	
	private int defaultIndex;
	
	public StatusIndicator(Xeon xeon,String title,int dataIndex,int defaultIndex,ArrayList<StatusObject> statusList){
		
		super(xeon,dataIndex,"STATUSINDIC");
		
		this.title = title;
		
		this.statusList = statusList;
		
		this.defaultIndex = defaultIndex;
		
		statusPanel = new StatusPanel(title);
		
		this.getContentPane().add(statusPanel);
		
		addData(defaultIndex + "");
		
	}
	
	@Override
	public void addData(String s){
		
		int data = Tools.cleanInputInt(s);
		
		String status = "No data";
		
		Color color = Color.lightGray;
		
		for(int i = 0; i < statusList.size(); i ++){
			
			if(statusList.get(i).getStatusId() == data){
				
				status = statusList.get(i).getStatusName();
				
				color = statusList.get(i).getStatusColor();
				
			}
			
		}
		
		statusPanel.setStatus(status);
		
		statusPanel.setBackgroundColor(color);
		
		statusPanel.repaint();
		
	}

	public ArrayList<StatusObject> getStatusList() {
		return statusList;
	}

	public void setStatusList(ArrayList<StatusObject> statusList) {
		this.statusList = statusList;
	}
	
	static class StatusPanel extends JPanel {
		
		String title = "New Status Indicator";
		
		String status = "No data";
		
		Color backgroundColor = Color.lightGray;
		
		public StatusPanel(String title) {
			
			this.title = title;
			
		}
		
		@Override
		public void paintComponent(Graphics gn){
			
			Graphics2D g = (Graphics2D) gn;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		            RenderingHints.VALUE_ANTIALIAS_ON);
			
			int w = this.getWidth();
			int h = this.getHeight();
			
			g.setColor(backgroundColor);
			
			g.fillRect(0, 0, w, h);
			
			g.setColor(Tools.getContrastColor(backgroundColor));
			
			g.drawString(title, 5, 15);
			
			g.setFont(g.getFont().deriveFont(20.0f));
			
			AffineTransform afftf = new AffineTransform();
			FontRenderContext frc =  new FontRenderContext(afftf, true, true);
			int txtw = (int)(g.getFont().getStringBounds(status, frc).getWidth());
			int txth = (int)(g.getFont().getStringBounds(status, frc).getHeight());
			g.drawString(status,w/2-txtw/2-2, h/2+txth/3);
			
			
			
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Color getBackgroundColor() {
			return backgroundColor;
		}

		public void setBackgroundColor(Color backgroundColor) {
			this.backgroundColor = backgroundColor;
		}
		
		
        
    }

	public String getStatusIndicTitle() {
		return title;
	}

	public void setStatusIndicTitle(String title) {
		this.title = title;
	}

	public StatusPanel getStatusPanel() {
		return statusPanel;
	}

	public void setStatusPanel(StatusPanel statusPanel) {
		this.statusPanel = statusPanel;
	}

	public int getDefaultIndex() {
		return defaultIndex;
	}

	public void setDefaultIndex(int defaultIndex) {
		this.defaultIndex = defaultIndex;
	}
	
	
	
}
