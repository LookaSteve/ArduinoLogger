package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Modules.NumericField;
import Modules.StatusObject;
import Modules.Tools;

import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.GridLayout;
import java.awt.Component;

public class StatusIndicatorCreation extends JFrame {

	private JPanel contentPane;

	private DenebUI denui;
	
	private JTextField fieldWindowTitle;
	private NumericField fieldRefIndex;
	private JTextField fieldGraphT;
	private NumericField textField;
	
	private DefaultListModel listModel;
	
	private StatusEditor statEdit;
	
	private MyListCellRenderer cellRenderer;
	
	public StatusIndicatorCreation(final DenebUI denui) {
		this.denui=denui;
		final StatusIndicatorCreation sic = this;
		setResizable(false);
		setTitle("New Status Indicator");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 536, 413);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		this.addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	        	 if(statEdit != null){
	        		 if(statEdit.isVisible()){
	        			 statEdit.dispose();
	        		 }
	        	 }
	           	dispose();
	         }        
	      });    
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewNumericGraph = new JLabel("New Status Indicator");
		lblNewNumericGraph.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblNewNumericGraph);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] {114, 138, 30, 30, 0};
		gbl_panel_1.rowHeights = new int[]{26, 26, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblWindowTitle = new JLabel("Window Title ");
		GridBagConstraints gbc_lblWindowTitle = new GridBagConstraints();
		gbc_lblWindowTitle.anchor = GridBagConstraints.EAST;
		gbc_lblWindowTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblWindowTitle.gridx = 0;
		gbc_lblWindowTitle.gridy = 1;
		panel_1.add(lblWindowTitle, gbc_lblWindowTitle);
		
		fieldWindowTitle = new JTextField();
		fieldWindowTitle.setText("New Numeric Graph");
		GridBagConstraints gbc_fieldWindowTitle = new GridBagConstraints();
		gbc_fieldWindowTitle.insets = new Insets(0, 0, 5, 5);
		gbc_fieldWindowTitle.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldWindowTitle.gridx = 1;
		gbc_fieldWindowTitle.gridy = 1;
		panel_1.add(fieldWindowTitle, gbc_fieldWindowTitle);
		fieldWindowTitle.setColumns(10);
		
		JLabel lblGraphTitle = new JLabel("Indicator Title ");
		GridBagConstraints gbc_lblGraphTitle = new GridBagConstraints();
		gbc_lblGraphTitle.anchor = GridBagConstraints.EAST;
		gbc_lblGraphTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblGraphTitle.gridx = 0;
		gbc_lblGraphTitle.gridy = 2;
		panel_1.add(lblGraphTitle, gbc_lblGraphTitle);
		
		fieldGraphT = new JTextField();
		fieldGraphT.setText("New Status Indicator");
		GridBagConstraints gbc_fieldGraphT = new GridBagConstraints();
		gbc_fieldGraphT.insets = new Insets(0, 0, 5, 5);
		gbc_fieldGraphT.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldGraphT.gridx = 1;
		gbc_fieldGraphT.gridy = 2;
		panel_1.add(fieldGraphT, gbc_fieldGraphT);
		fieldGraphT.setColumns(10);
		
		JLabel lblReferenceIndex = new JLabel("  Channel Index ");
		GridBagConstraints gbc_lblReferenceIndex = new GridBagConstraints();
		gbc_lblReferenceIndex.anchor = GridBagConstraints.EAST;
		gbc_lblReferenceIndex.insets = new Insets(0, 0, 5, 5);
		gbc_lblReferenceIndex.gridx = 0;
		gbc_lblReferenceIndex.gridy = 3;
		panel_1.add(lblReferenceIndex, gbc_lblReferenceIndex);
		
		fieldRefIndex = new NumericField();
		fieldRefIndex.setValue(0);
		GridBagConstraints gbc_fieldRefIndex = new GridBagConstraints();
		gbc_fieldRefIndex.insets = new Insets(0, 0, 5, 5);
		gbc_fieldRefIndex.fill = GridBagConstraints.HORIZONTAL;
		gbc_fieldRefIndex.gridx = 1;
		gbc_fieldRefIndex.gridy = 3;
		panel_1.add(fieldRefIndex, gbc_fieldRefIndex);
		fieldRefIndex.setColumns(10);
		
		JButton button = fieldRefIndex.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 2;
		gbc_button.gridy = 3;
		panel_1.add(button, gbc_button);
		
		JButton button_1 = fieldRefIndex.generatePlusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 0);
		gbc_button_1.gridx = 3;
		gbc_button_1.gridy = 3;
		panel_1.add(button_1, gbc_button_1);
		
		JLabel lblDefaultIndex = new JLabel("  Default Value ");
		GridBagConstraints gbc_lblDefaultIndex = new GridBagConstraints();
		gbc_lblDefaultIndex.anchor = GridBagConstraints.EAST;
		gbc_lblDefaultIndex.insets = new Insets(0, 0, 5, 5);
		gbc_lblDefaultIndex.gridx = 0;
		gbc_lblDefaultIndex.gridy = 4;
		panel_1.add(lblDefaultIndex, gbc_lblDefaultIndex);
		
		textField = new NumericField();
		textField.setValue(0);
		textField.setColumns(10);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 4;
		panel_1.add(textField, gbc_textField);
		
		JButton button_2 = textField.generateMinusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.insets = new Insets(0, 0, 5, 5);
		gbc_button_2.gridx = 2;
		gbc_button_2.gridy = 4;
		panel_1.add(button_2, gbc_button_2);
		
		JButton button_3 = textField.generatePlusButton();
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.insets = new Insets(0, 0, 5, 0);
		gbc_button_3.gridx = 3;
		gbc_button_3.gridy = 4;
		panel_1.add(button_3, gbc_button_3);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.gridheight = 3;
		gbc_panel_2.insets = new Insets(0, 0, 5, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 5;
		panel_1.add(panel_3, gbc_panel_2);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_3.add(scrollPane, BorderLayout.CENTER);
		
		listModel = new DefaultListModel();
		
		cellRenderer = new MyListCellRenderer();
		
		final JList list = new JList(listModel);
		list.setCellRenderer(cellRenderer);
		scrollPane.setViewportView(list);
		
		JPanel panel_4 = new JPanel();
		scrollPane.setColumnHeaderView(panel_4);
		panel_4.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnNewStatus = new JButton("New status");
		btnNewStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(statEdit != null){
					if(!statEdit.isVisible()){
						statEdit = new StatusEditor(sic);
						statEdit.setVisible(true);
					}else{
						statEdit.requestFocus();
					}
				}else{
					statEdit = new StatusEditor(sic);
					statEdit.setVisible(true);
				}
				
				
			}
		});
		panel_4.add(btnNewStatus);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(list.getSelectedIndex() != -1){
					
					if(statEdit != null){
						if(!statEdit.isVisible()){
							statEdit = new StatusEditor(sic,list.getSelectedIndex(),cellRenderer.getStatusList().get(list.getSelectedIndex()).getStatusId(),cellRenderer.getStatusList().get(list.getSelectedIndex()).getStatusName(),cellRenderer.getStatusList().get(list.getSelectedIndex()).getStatusColor());
							statEdit.setVisible(true);
						}else{
							statEdit.requestFocus();
						}
					}else{
						statEdit = new StatusEditor(sic,list.getSelectedIndex(),cellRenderer.getStatusList().get(list.getSelectedIndex()).getStatusId(),cellRenderer.getStatusList().get(list.getSelectedIndex()).getStatusName(),cellRenderer.getStatusList().get(list.getSelectedIndex()).getStatusColor());
						statEdit.setVisible(true);
					}
					
				}
				
			}
		});
		panel_4.add(btnEdit);
		
		JButton btnRemoveStatus = new JButton("Remove");
		btnRemoveStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(list.getSelectedIndex() != -1){
					
					cellRenderer.getStatusList().remove(list.getSelectedIndex());
					
					listModel.remove(list.getSelectedIndex());
					
				}
				
			}
		});
		panel_4.add(btnRemoveStatus);
		
		JLabel lblNewLabel = new JLabel("Status ");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 5;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				denui.getInternalFrameManager().addNewStatusIndicator(fieldWindowTitle.getText(), fieldGraphT.getText(), fieldRefIndex.getValue(), textField.getValue(), cellRenderer.getStatusList(),null);
				dispose();
				
			}
		});
		panel_2.setLayout(new BorderLayout(0, 0));
		panel_2.add(btnNewButton, BorderLayout.EAST);
		
	}

	public DefaultListModel getListModel() {
		return listModel;
	}

	public void setListModel(DefaultListModel listModel) {
		this.listModel = listModel;
	}
	
	public MyListCellRenderer getCellRenderer() {
		return cellRenderer;
	}

	public void setCellRenderer(MyListCellRenderer cellRenderer) {
		this.cellRenderer = cellRenderer;
	}



	static class MyListCellRenderer extends DefaultListCellRenderer {
		
		private ArrayList<StatusObject> statusList = new ArrayList<StatusObject>();
		
        public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus ) {
            
        	Component c = super.getListCellRendererComponent( list, value, index, isSelected, cellHasFocus );
           
        	c.setBackground(statusList.get(index).getStatusColor());
        	
        	c.setForeground(Tools.getContrastColor(statusList.get(index).getStatusColor()));
            
        	return c;
            
        }
        
        public ArrayList<StatusObject> getStatusList() {
    		return statusList;
    	}

    	public void setStatusList(ArrayList<StatusObject> statusList) {
    		this.statusList = statusList;
    	}
        
    }

}
