package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.JPanel;

import Modules.Tools;
import core.Xeon;

public class Gauge extends InternalFrame{
	
	private GaugePanel gaugePanel;
	
	private String title;
	
	private Color lowCol;
	private Color highCol;
	private Color backCol;
	
	private int minVal;
	private int maxVal;
	
	private double value;
	
	public Gauge(Xeon xeon,String title,int dataIndex,Color lowCol,Color highCol,Color backCol,int minVal,int maxVal){
		
		super(xeon,dataIndex,"GAUGE");
		
		this.title = title;
		
		this.lowCol = lowCol;
		this.highCol = highCol;
		this.backCol = backCol;
		this.minVal = minVal;
		this.maxVal = maxVal;
		
		gaugePanel = new GaugePanel(this);
		
		this.getContentPane().add(gaugePanel);
		
	}
	
	@Override
	public void addData(String s){
		
		double data = Tools.cleanInputDouble(s);
		
		value = data;
		
		gaugePanel.repaint();
		
	}
	
	static class GaugePanel extends JPanel {
		
		private Gauge gauge;
		
		public GaugePanel(Gauge gauge) {
			
			this.gauge = gauge;
			
		}
		
		@Override
		public void paintComponent(Graphics gn){
			
			Graphics2D g = (Graphics2D) gn;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		            RenderingHints.VALUE_ANTIALIAS_ON);
			
			int w = this.getWidth();
			int h = this.getHeight();
			
			g.setColor(gauge.getBackCol());
			
			g.fillRect(0, 0, w, h);
			
			//Gauge indcator
			
			int differenceBetweenMaxAndMin = (int) Tools.differenceBetweenTwoReals(gauge.getMaxVal(),gauge.getMinVal());
			
			if(differenceBetweenMaxAndMin == 0){differenceBetweenMaxAndMin = 1;}
			
			double ratioBetweenValueAndDiff = (double)(gauge.getValue() - gauge.getMinVal()) / (double)(differenceBetweenMaxAndMin);
			
			if(ratioBetweenValueAndDiff > 1){
				
				ratioBetweenValueAndDiff = 1;
				
			}
			
			if(ratioBetweenValueAndDiff < 0){
				
				ratioBetweenValueAndDiff = 0;
				
			}
			
			int blocHeight = (int) (h * ratioBetweenValueAndDiff);
			
			g.setColor(Tools.getTransitionColor(gauge.getLowCol(), gauge.getHighCol(), ratioBetweenValueAndDiff));
			
			g.fillRect(0, h - blocHeight, w, blocHeight);
			
			g.setColor(Tools.getContrastColor(gauge.getBackCol()));
			
			g.drawString(gauge.getTitle(), 5, 15);
			
			g.setFont(g.getFont().deriveFont(20.0f));
			
			AffineTransform afftf = new AffineTransform();
			FontRenderContext frc =  new FontRenderContext(afftf, true, true);
			int txtw = (int)(g.getFont().getStringBounds(gauge.getValue() + "", frc).getWidth());
			int txth = (int)(g.getFont().getStringBounds(gauge.getValue() + "", frc).getHeight());
			g.drawString(gauge.getValue() + "",w/2-txtw/2-2, h/2+txth/3);
			
			
			
		}
        
    }

	public String getGaugeTitle() {
		return title;
	}

	public void setGaugeTitle(String title) {
		this.title = title;
	}

	public Color getLowCol() {
		return lowCol;
	}

	public void setLowCol(Color lowCol) {
		this.lowCol = lowCol;
	}

	public Color getHighCol() {
		return highCol;
	}

	public void setHighCol(Color highCol) {
		this.highCol = highCol;
	}

	public Color getBackCol() {
		return backCol;
	}

	public void setBackCol(Color backCol) {
		this.backCol = backCol;
	}

	public int getMinVal() {
		return minVal;
	}

	public void setMinVal(int minVal) {
		this.minVal = minVal;
	}

	public int getMaxVal() {
		return maxVal;
	}

	public void setMaxVal(int maxVal) {
		this.maxVal = maxVal;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	
	
}
