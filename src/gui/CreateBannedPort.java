package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import core.Xeon;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreateBannedPort extends JFrame {

    private JPanel contentPane;
    private JTextField textField;

    public CreateBannedPort(Xeon xeon,GeneralSettings settings) {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 250);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);
        
        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                if(textField.getText().equals("")){
                    JOptionPane.showMessageDialog(null,
                            "Cannot ban an unnamed port.",
                            "Empty field",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                
                xeon.getCardManager().getScanner().addBannedPort(textField.getText());
                xeon.getDefaultConfigManager().saveConfiguration();
                settings.loadBannedPorts();
                dispose();
                
            }
        });
        panel.add(btnOk);
        
        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                dispose();
                
            }
        });
        panel.add(btnCancel);
        
        JLabel lblAddABanned = new JLabel("Add a banned Serial Port");
        lblAddABanned.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblAddABanned.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblAddABanned, BorderLayout.NORTH);
        
        JPanel panel_1 = new JPanel();
        contentPane.add(panel_1, BorderLayout.CENTER);
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);
        
        JLabel label = new JLabel("    ");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        panel_1.add(label, gbc_label);
        
        JLabel label_1 = new JLabel("    ");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 5, 5);
        gbc_label_1.gridx = 4;
        gbc_label_1.gridy = 0;
        panel_1.add(label_1, gbc_label_1);
        
        JLabel label_5 = new JLabel("    ");
        GridBagConstraints gbc_label_5 = new GridBagConstraints();
        gbc_label_5.insets = new Insets(0, 0, 5, 0);
        gbc_label_5.gridx = 5;
        gbc_label_5.gridy = 0;
        panel_1.add(label_5, gbc_label_5);
        
        JLabel label_2 = new JLabel("    ");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 1;
        panel_1.add(label_2, gbc_label_2);
        
        JLabel label_3 = new JLabel("    ");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.insets = new Insets(0, 0, 5, 5);
        gbc_label_3.gridx = 4;
        gbc_label_3.gridy = 1;
        panel_1.add(label_3, gbc_label_3);
        
        JLabel label_4 = new JLabel("    ");
        GridBagConstraints gbc_label_4 = new GridBagConstraints();
        gbc_label_4.insets = new Insets(0, 0, 5, 5);
        gbc_label_4.gridx = 1;
        gbc_label_4.gridy = 2;
        panel_1.add(label_4, gbc_label_4);
        
        JLabel lblSerialName = new JLabel("Serial Name");
        GridBagConstraints gbc_lblSerialName = new GridBagConstraints();
        gbc_lblSerialName.insets = new Insets(0, 0, 5, 5);
        gbc_lblSerialName.anchor = GridBagConstraints.EAST;
        gbc_lblSerialName.gridx = 2;
        gbc_lblSerialName.gridy = 2;
        panel_1.add(lblSerialName, gbc_lblSerialName);
        
        textField = new JTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.insets = new Insets(0, 0, 5, 5);
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.gridx = 3;
        gbc_textField.gridy = 2;
        panel_1.add(textField, gbc_textField);
        textField.setColumns(10);
        
        JLabel lblcomLptEtc = new JLabel("(COM1, LPT1, etc...)");
        GridBagConstraints gbc_lblcomLptEtc = new GridBagConstraints();
        gbc_lblcomLptEtc.insets = new Insets(0, 0, 0, 5);
        gbc_lblcomLptEtc.gridx = 3;
        gbc_lblcomLptEtc.gridy = 3;
        panel_1.add(lblcomLptEtc, gbc_lblcomLptEtc);
        
        getRootPane().setDefaultButton(btnOk);
        
    }

}
