package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import Modules.NumericField;
import Modules.HintTextField;
import Modules.HintTextField.HintFieldListener;
import core.Xeon;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import java.awt.Color;

public class ConfigSettings extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField logPath;

	private Xeon xeon;
	private JTextField logHeader;
	private HintTextField headToRemove;
	private HintTextField footerToRemove;
	private HintTextField regexField;
	private JLabel lblHTR;
	private JLabel lblFTR;
	private JLabel lblREG;
	private HintTextField replaceRegexField;
	private JLabel lblREP;
	
	public ConfigSettings(final Xeon xeon) {
		setResizable(false);
		setTitle("Configuration Settings");
		this.xeon = xeon;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 460);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblConfigurationSettings = new JLabel("Configuration Settings");
		lblConfigurationSettings.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblConfigurationSettings);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel label = new JLabel("      ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel_1.add(label, gbc_label);
		
		JLabel lblNewLabel = new JLabel(" ");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel label_1 = new JLabel("      ");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 4;
		gbc_label_1.gridy = 0;
		panel_1.add(label_1, gbc_label_1);
		
		JLabel lblDataManagement = new JLabel("Data Management");
		lblDataManagement.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblDataManagement = new GridBagConstraints();
		gbc_lblDataManagement.anchor = GridBagConstraints.EAST;
		gbc_lblDataManagement.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataManagement.gridx = 1;
		gbc_lblDataManagement.gridy = 1;
		panel_1.add(lblDataManagement, gbc_lblDataManagement);
		
		JLabel lblSeparator = new JLabel("Separator ");
		GridBagConstraints gbc_lblSeparator = new GridBagConstraints();
		gbc_lblSeparator.insets = new Insets(0, 0, 5, 5);
		gbc_lblSeparator.anchor = GridBagConstraints.EAST;
		gbc_lblSeparator.gridx = 1;
		gbc_lblSeparator.gridy = 2;
		panel_1.add(lblSeparator, gbc_lblSeparator);
		
		textField = new JTextField(xeon.getDataSeparator());
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 2;
		panel_1.add(textField, gbc_textField);
		textField.setColumns(10);
		textField.getDocument().addDocumentListener(new DocumentListener() {
			  
			  public void changedUpdate(DocumentEvent e) {
			    update();
			  }
			  public void removeUpdate(DocumentEvent e) {
				update();
			  }
			  public void insertUpdate(DocumentEvent e) {
				update();
			  }

			  public void update() {
			     
				  if(textField.getText().length()>0){
					  
					  xeon.setDataSeparator(textField.getText());
					  xeon.action();
					  
				  }
				  
			  }
			  
			});
		
		JLabel lblLogPath = new JLabel("Log path ");
		GridBagConstraints gbc_lblLogPath = new GridBagConstraints();
		gbc_lblLogPath.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogPath.anchor = GridBagConstraints.EAST;
		gbc_lblLogPath.gridx = 1;
		gbc_lblLogPath.gridy = 3;
		panel_1.add(lblLogPath, gbc_lblLogPath);
		
		logPath = new JTextField(xeon.getLogPath().toString());
		logPath.setEditable(false);
		GridBagConstraints gbc_logHeader = new GridBagConstraints();
		gbc_logHeader.insets = new Insets(0, 0, 5, 5);
		gbc_logHeader.fill = GridBagConstraints.HORIZONTAL;
		gbc_logHeader.gridx = 2;
		gbc_logHeader.gridy = 3;
		panel_1.add(logPath, gbc_logHeader);
		logPath.setColumns(10);
		
		JButton btnNewButton = new JButton("...");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				xeon.getLogManager().changeLogPath();
				
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 3;
		panel_1.add(btnNewButton, gbc_btnNewButton);
		
		JLabel lblLogFileHeader = new JLabel("Log title header");
		GridBagConstraints gbc_lblLogFileHeader = new GridBagConstraints();
		gbc_lblLogFileHeader.anchor = GridBagConstraints.EAST;
		gbc_lblLogFileHeader.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogFileHeader.gridx = 1;
		gbc_lblLogFileHeader.gridy = 4;
		panel_1.add(lblLogFileHeader, gbc_lblLogFileHeader);
		
		logHeader = new JTextField(xeon.getLogHeader());
		logHeader.getDocument().addDocumentListener(new DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                update();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                update();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                update();
            }
            public void update(){
                xeon.setLogHeader(logHeader.getText());
                xeon.action();
            }
        });
		logHeader.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyTyped(java.awt.event.KeyEvent evt) {

	            if(!Character.isLetter(evt.getKeyChar()) && !Character.isDigit(evt.getKeyChar()) && !Character.isWhitespace(evt.getKeyChar()) ){
	                   evt.consume();
	               }
	           }
	        
	       });
		GridBagConstraints gbc_logHeader2 = new GridBagConstraints();
		gbc_logHeader2.insets = new Insets(0, 0, 5, 5);
		gbc_logHeader2.fill = GridBagConstraints.HORIZONTAL;
		gbc_logHeader2.gridx = 2;
		gbc_logHeader2.gridy = 4;
		panel_1.add(logHeader, gbc_logHeader2);
		logHeader.setColumns(10);
		
		JLabel label_5 = new JLabel("  ");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 1;
		gbc_label_5.gridy = 5;
		panel_1.add(label_5, gbc_label_5);
		
		JLabel lblProcessing = new JLabel("Processing");
		lblProcessing.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblProcessing = new GridBagConstraints();
		gbc_lblProcessing.anchor = GridBagConstraints.EAST;
		gbc_lblProcessing.insets = new Insets(0, 0, 5, 5);
		gbc_lblProcessing.gridx = 1;
		gbc_lblProcessing.gridy = 6;
		panel_1.add(lblProcessing, gbc_lblProcessing);
		
		JLabel lblEnablePreprocessing = new JLabel("Enable processing");
		GridBagConstraints gbc_lblEnablePreprocessing = new GridBagConstraints();
		gbc_lblEnablePreprocessing.anchor = GridBagConstraints.EAST;
		gbc_lblEnablePreprocessing.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnablePreprocessing.gridx = 1;
		gbc_lblEnablePreprocessing.gridy = 7;
		panel_1.add(lblEnablePreprocessing, gbc_lblEnablePreprocessing);
		
		JCheckBox checkBox = new JCheckBox("");
		checkBox.addItemListener(new ItemListener() {
		      public void itemStateChanged(ItemEvent e){
		          setProcessingVisible(checkBox.isSelected());
		          xeon.getPreProcessor().setEnablePreProcessing(checkBox.isSelected());
		          xeon.action();
		      }
		    });
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.anchor = GridBagConstraints.WEST;
		gbc_checkBox.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox.gridx = 2;
		gbc_checkBox.gridy = 7;
		panel_1.add(checkBox, gbc_checkBox);
		
		lblHTR = new JLabel("Header to remove");
		lblHTR.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_lblHTR = new GridBagConstraints();
		gbc_lblHTR.anchor = GridBagConstraints.EAST;
		gbc_lblHTR.insets = new Insets(0, 0, 5, 5);
		gbc_lblHTR.gridx = 1;
		gbc_lblHTR.gridy = 8;
		panel_1.add(lblHTR, gbc_lblHTR);
		
		headToRemove = new HintTextField(xeon.getPreProcessor().getHeaderToRemove(),"Remove the beggining of incoming data");
		headToRemove.setToolTipText("Remove the beggining of incoming data");
		headToRemove.setEnabled(false);
		headToRemove.setColumns(10);
		headToRemove.addListener(new HintFieldListener() {
            
            @Override
            public void fieldUpdated(String newText) {
                xeon.getPreProcessor().setHeaderToRemove(newText);
                xeon.action();
            }
            
        });
		GridBagConstraints gbc_headToRemove = new GridBagConstraints();
		gbc_headToRemove.insets = new Insets(0, 0, 5, 5);
		gbc_headToRemove.fill = GridBagConstraints.HORIZONTAL;
		gbc_headToRemove.gridx = 2;
		gbc_headToRemove.gridy = 8;
		panel_1.add(headToRemove, gbc_headToRemove);
		
		lblFTR = new JLabel("Footer to remove");
		lblFTR.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_lblFTR = new GridBagConstraints();
		gbc_lblFTR.anchor = GridBagConstraints.EAST;
		gbc_lblFTR.insets = new Insets(0, 0, 5, 5);
		gbc_lblFTR.gridx = 1;
		gbc_lblFTR.gridy = 9;
		panel_1.add(lblFTR, gbc_lblFTR);
		
		footerToRemove = new HintTextField(xeon.getPreProcessor().getFooterToRemove(),"Remove the end of incoming data");
		footerToRemove.setToolTipText("Remove the end of incoming data");
		footerToRemove.setEnabled(false);
		footerToRemove.setColumns(10);
		footerToRemove.addListener(new HintFieldListener() {
            
            @Override
            public void fieldUpdated(String newText) {
                xeon.getPreProcessor().setFooterToRemove(newText);
                xeon.action();
            }
            
        });
		GridBagConstraints gbc_footerToRemove = new GridBagConstraints();
		gbc_footerToRemove.insets = new Insets(0, 0, 5, 5);
		gbc_footerToRemove.fill = GridBagConstraints.HORIZONTAL;
		gbc_footerToRemove.gridx = 2;
		gbc_footerToRemove.gridy = 9;
		panel_1.add(footerToRemove, gbc_footerToRemove);
		
		lblREG = new JLabel("Match with REGEX");
		lblREG.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_lblREG = new GridBagConstraints();
		gbc_lblREG.anchor = GridBagConstraints.EAST;
		gbc_lblREG.insets = new Insets(0, 0, 5, 5);
		gbc_lblREG.gridx = 1;
		gbc_lblREG.gridy = 10;
		panel_1.add(lblREG, gbc_lblREG);
		
		regexField = new HintTextField(xeon.getPreProcessor().getRegexToExecute(),"Filter incomming data with a regular expression");
		regexField.setToolTipText("Filter incomming data with a regular expression");
		regexField.setEnabled(false);
		regexField.setColumns(10);
		regexField.addListener(new HintFieldListener() {
            
            @Override
            public void fieldUpdated(String newText) {
                xeon.getPreProcessor().setRegexToExecute(newText);
                xeon.action();
            }
            
        });
		GridBagConstraints gbc_regexField = new GridBagConstraints();
		gbc_regexField.insets = new Insets(0, 0, 5, 5);
		gbc_regexField.fill = GridBagConstraints.HORIZONTAL;
		gbc_regexField.gridx = 2;
		gbc_regexField.gridy = 10;
		panel_1.add(regexField, gbc_regexField);
		
		lblREP = new JLabel("Replace with");
		lblREP.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_lblREP = new GridBagConstraints();
		gbc_lblREP.anchor = GridBagConstraints.EAST;
		gbc_lblREP.insets = new Insets(0, 0, 5, 5);
		gbc_lblREP.gridx = 1;
		gbc_lblREP.gridy = 11;
		panel_1.add(lblREP, gbc_lblREP);
		
		replaceRegexField = new HintTextField(xeon.getPreProcessor().getRegexToReplace(), "Replace matched pattern by the REGEX");
		replaceRegexField.setToolTipText("Replace matched pattern by the REGEX");
		replaceRegexField.setEnabled(false);
		replaceRegexField.setColumns(10);
		replaceRegexField.addListener(new HintFieldListener() {
            
            @Override
            public void fieldUpdated(String newText) {
                xeon.getPreProcessor().setRegexToReplace(newText);
                xeon.action();
            }
            
        });
		GridBagConstraints gbc_replaceRegexField = new GridBagConstraints();
		gbc_replaceRegexField.insets = new Insets(0, 0, 5, 5);
		gbc_replaceRegexField.fill = GridBagConstraints.HORIZONTAL;
		gbc_replaceRegexField.gridx = 2;
		gbc_replaceRegexField.gridy = 11;
		panel_1.add(replaceRegexField, gbc_replaceRegexField);
		
		JLabel label_4 = new JLabel("  ");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.insets = new Insets(0, 0, 0, 5);
		gbc_label_4.gridx = 1;
		gbc_label_4.gridy = 12;
		panel_1.add(label_4, gbc_label_4);
		
		JLabel label_3 = new JLabel("      ");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridx = 5;
		gbc_label_3.gridy = 12;
		panel_1.add(label_3, gbc_label_3);
		
		JPanel panel_2 = new JPanel();
		panel_2.setToolTipText("");
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		panel_2.add(btnClose);
		
		checkBox.setSelected(xeon.getPreProcessor().isEnablePreProcessing());
        setProcessingVisible(xeon.getPreProcessor().isEnablePreProcessing());
		
	}
	
	private void setProcessingVisible(boolean isVisible){
	    if(isVisible){
	        headToRemove.setEnabled(true);
            footerToRemove.setEnabled(true);
            regexField.setEnabled(true);
            replaceRegexField.setEnabled(true);
            lblHTR.setForeground(Color.BLACK);
            lblFTR.setForeground(Color.BLACK);
            lblREG.setForeground(Color.BLACK);
            lblREP.setForeground(Color.BLACK);
	    }else{
	        headToRemove.setEnabled(false);
            footerToRemove.setEnabled(false);
            regexField.setEnabled(false);
            replaceRegexField.setEnabled(false);
            lblHTR.setForeground(Color.LIGHT_GRAY);
            lblFTR.setForeground(Color.LIGHT_GRAY);
            lblREG.setForeground(Color.LIGHT_GRAY);
            lblREP.setForeground(Color.LIGHT_GRAY);
	    }
	}

	public JTextField getLogPath() {
		return logPath;
	}

	public void setLogPath(JTextField logPath) {
		this.logPath = logPath;
	}
	
	

}
