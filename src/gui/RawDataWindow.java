package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logger.DataSet;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.Font;

public class RawDataWindow extends JFrame {

	private JPanel contentPane;
	
	private DefaultListModel listModel;

	public RawDataWindow(DataSet ds, int index) {
	    setTitle("Channel Data Explorer");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 500);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Channel " + index + " data : ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		listModel = new DefaultListModel();
		JList list = new JList(listModel);
		scrollPane.setViewportView(list);
	
		loadData(ds,index);
	
	}
	
	private void loadData(DataSet ds, int index){
		
		for(int i = 0 ; i < ds.getDataLines().size(); i ++){
		    
		    if(ds.getDataLines().get(i).getTime() != null){
		     
		        listModel.addElement(" " + i + " |  [" + ds.getDataLines().get(i).getTime() + "]  " + ds.getDataLines().get(i).safeGetDataList(index));
		        
		    }else{
		        
		        listModel.addElement(" " + i + " |   " + ds.getDataLines().get(i).safeGetDataList(index));
		        
		    }
			
		}
		
	}

}
