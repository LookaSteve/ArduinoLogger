package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JTabbedPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import Logger.DataItem;
import Logger.DataSet;
import Logger.LogParser;
import Modules.CheckCellRenderer;
import Modules.CheckListItem;
import Modules.Tools;
import core.Xeon;
import injector.AutoInjector;
import injector.ManualInjector;
import injector.RealTimeInjector;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class LogManagementUI extends JFrame {

    private LogManagementUI thisObject;
    
    private JPanel contentPane;
    private JList listAvailableChannels;
    private Xeon xeon;
    private DataSet dataSet;
    
    private JLabel logStatus;
    private JTextPane aboutPane;
    
    private DefaultListModel listAvailableChannelsModel;
    private DefaultListModel listAvailableInjectionModel;
    private JList listAvailableChannelInjection;
    
    private AutoInjector autoInjector;
    private ManualInjector manualInjector;
    private RealTimeInjector realTimeInjector;
    private JComboBox fileInterpretationMethodComboBox;
    private JComboBox CSVTimeStampComboBox;
    private ItemListener CSVTimeStampComboListener;
    private JCheckBox chckbxCSVInterpretColumnAsTimestamp;
    private JCheckBox chckbxIgnoreValueWithoutDate;
    
    private int requestCSVTimeStampComboboxSelection = -1;
    private JLabel lblSelectTheTimestamp;

    public LogManagementUI(Xeon xeon,DataSet dataSet){
        
        thisObject = this;
        
        this.xeon = xeon;
        this.dataSet = dataSet;
        
        autoInjector = new AutoInjector(xeon, this);
        manualInjector = new ManualInjector(xeon, this);
        realTimeInjector = new RealTimeInjector(xeon, this);
        
        setTitle("Data File Management");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBounds(100, 100, 750, 550);
        setLocationRelativeTo(null);
        
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                
                autoInjector.onClose();
                manualInjector.onClose();
                realTimeInjector.onClose();
                dispose();
                
            }
        });
        
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        JPanel defaultTopPanel = new JPanel();
        contentPane.add(defaultTopPanel, BorderLayout.NORTH);
        defaultTopPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel bottomDefaultPanel = new JPanel();
        defaultTopPanel.add(bottomDefaultPanel, BorderLayout.SOUTH);
        bottomDefaultPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel innerBottomDefaultPanel = new JPanel();
        innerBottomDefaultPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        bottomDefaultPanel.add(innerBottomDefaultPanel, BorderLayout.WEST);
        innerBottomDefaultPanel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblInterpretFileAs = new JLabel("Interpret file as");
        lblInterpretFileAs.setFont(new Font("Tahoma", Font.PLAIN, 20));
        innerBottomDefaultPanel.add(lblInterpretFileAs, BorderLayout.CENTER);
        
        JPanel innerComboBottomDefaultPanel = new JPanel();
        innerComboBottomDefaultPanel.setBorder(new EmptyBorder(10, 40, 10, 0));
        bottomDefaultPanel.add(innerComboBottomDefaultPanel, BorderLayout.CENTER);
        innerComboBottomDefaultPanel.setLayout(new BorderLayout(0, 0));
        
        fileInterpretationMethodComboBox = new JComboBox();
        innerComboBottomDefaultPanel.add(fileInterpretationMethodComboBox);
        fileInterpretationMethodComboBox.setModel(new DefaultComboBoxModel(LogParser.fileTypes));
        
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        bottomDefaultPanel.add(panel, BorderLayout.EAST);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        JButton btnTest = new JButton("?");
        btnTest.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                HelpHeaderSelection hhs = new HelpHeaderSelection();
                hhs.setVisible(true);
                
            }
        });
        panel.add(btnTest);
        
        JPanel contentPanel = new JPanel();
        contentPane.add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));
        
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        contentPanel.add(tabbedPane);
        
        JPanel dataReviewPanel = new JPanel();
        dataReviewPanel.setBackground(Color.WHITE);
        tabbedPane.addTab("Review Data", null, dataReviewPanel, null);
        dataReviewPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel channelContainerPanel = new JPanel();
        channelContainerPanel.setBackground(Color.WHITE);
        channelContainerPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
        dataReviewPanel.add(channelContainerPanel, BorderLayout.WEST);
        channelContainerPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel channelWrapperPanel = new JPanel();
        channelWrapperPanel.setBackground(Color.WHITE);
        channelWrapperPanel.setBorder(null);
        channelContainerPanel.add(channelWrapperPanel, BorderLayout.CENTER);
        channelWrapperPanel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblAvailableChannels = new JLabel("Available Channels");
        lblAvailableChannels.setHorizontalAlignment(SwingConstants.CENTER);
        lblAvailableChannels.setBackground(Color.WHITE);
        lblAvailableChannels.setFont(new Font("Tahoma", Font.PLAIN, 20));
        channelWrapperPanel.add(lblAvailableChannels, BorderLayout.NORTH);
        
        JScrollPane scrollPane = new JScrollPane();
        channelWrapperPanel.add(scrollPane);
        
        listAvailableChannelsModel = new DefaultListModel();
        listAvailableChannels = new JList(listAvailableChannelsModel);
        listAvailableChannels.setBackground(UIManager.getColor("Button.background"));
        scrollPane.setViewportView(listAvailableChannels);
        
        JPanel dataReviewCenterPanel = new JPanel();
        dataReviewCenterPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
        dataReviewCenterPanel.setBackground(Color.WHITE);
        dataReviewPanel.add(dataReviewCenterPanel, BorderLayout.CENTER);
        dataReviewCenterPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel topOptionPanel = new JPanel();
        topOptionPanel.setBackground(Color.WHITE);
        dataReviewCenterPanel.add(topOptionPanel, BorderLayout.NORTH);
        topOptionPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel innerFirstOptionPanel = new JPanel();
        innerFirstOptionPanel.setBackground(Color.WHITE);
        topOptionPanel.add(innerFirstOptionPanel, BorderLayout.NORTH);
        innerFirstOptionPanel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblSelectedChannel = new JLabel("Selected Channel");
        lblSelectedChannel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        innerFirstOptionPanel.add(lblSelectedChannel, BorderLayout.NORTH);
        
        JPanel selectedChannelButtonPanel = new JPanel();
        selectedChannelButtonPanel.setBackground(Color.WHITE);
        innerFirstOptionPanel.add(selectedChannelButtonPanel, BorderLayout.SOUTH);
        selectedChannelButtonPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel buttonWrapperSelectedChannelPanel = new JPanel();
        buttonWrapperSelectedChannelPanel.setBackground(Color.WHITE);
        selectedChannelButtonPanel.add(buttonWrapperSelectedChannelPanel, BorderLayout.CENTER);
        buttonWrapperSelectedChannelPanel.setLayout(new BoxLayout(buttonWrapperSelectedChannelPanel, BoxLayout.X_AXIS));
        
        JPanel btnSendToGraphWrapperPanel = new JPanel();
        btnSendToGraphWrapperPanel.setBorder(new EmptyBorder(5, 0, 5, 5));
        btnSendToGraphWrapperPanel.setBackground(Color.WHITE);
        buttonWrapperSelectedChannelPanel.add(btnSendToGraphWrapperPanel);
        btnSendToGraphWrapperPanel.setLayout(new BorderLayout(0, 0));
        
        JButton btnSendToGraph = new JButton("Send to graph");
        btnSendToGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                if(listAvailableChannels.getSelectedIndex() != -1){
                    
                    ReviewGraph rg = new ReviewGraph(xeon,dataSet, listAvailableChannels.getSelectedIndex());
                    rg.pack();
                    rg.setVisible(true);
                    
                }
                
            }
        });
        btnSendToGraphWrapperPanel.add(btnSendToGraph);
        
        JPanel btnShowChannelDataWrapperPanel = new JPanel();
        btnShowChannelDataWrapperPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        btnShowChannelDataWrapperPanel.setBackground(Color.WHITE);
        buttonWrapperSelectedChannelPanel.add(btnShowChannelDataWrapperPanel);
        btnShowChannelDataWrapperPanel.setLayout(new BorderLayout(0, 0));
        
        JButton btnShowChannelData = new JButton("Show channel data");
        btnShowChannelData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                if(listAvailableChannels.getSelectedIndex() != -1){
                    
                    RawDataWindow rdw = new RawDataWindow(dataSet, listAvailableChannels.getSelectedIndex());
                    rdw.setVisible(true);
                    
                }
                
            }
        });
        btnShowChannelDataWrapperPanel.add(btnShowChannelData);
        
        JPanel InnerSecondOptionPanel = new JPanel();
        InnerSecondOptionPanel.setBorder(new EmptyBorder(10, 0, 10, 0));
        InnerSecondOptionPanel.setBackground(Color.WHITE);
        topOptionPanel.add(InnerSecondOptionPanel, BorderLayout.SOUTH);
        InnerSecondOptionPanel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblGeneral = new JLabel("General");
        lblGeneral.setFont(new Font("Tahoma", Font.PLAIN, 20));
        InnerSecondOptionPanel.add(lblGeneral, BorderLayout.NORTH);
        
        JPanel generalButtonPanel = new JPanel();
        generalButtonPanel.setBackground(Color.WHITE);
        InnerSecondOptionPanel.add(generalButtonPanel, BorderLayout.SOUTH);
        generalButtonPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel buttonWrapperGeneralPanel = new JPanel();
        buttonWrapperGeneralPanel.setBackground(Color.WHITE);
        generalButtonPanel.add(buttonWrapperGeneralPanel);
        buttonWrapperGeneralPanel.setLayout(new BoxLayout(buttonWrapperGeneralPanel, BoxLayout.X_AXIS));
        
        JPanel btnShowAllDataWrapperPanel = new JPanel();
        btnShowAllDataWrapperPanel.setBorder(new EmptyBorder(5, 0, 5, 5));
        btnShowAllDataWrapperPanel.setBackground(Color.WHITE);
        buttonWrapperGeneralPanel.add(btnShowAllDataWrapperPanel);
        btnShowAllDataWrapperPanel.setLayout(new BorderLayout(0, 0));
        
        JButton btnShowRawData = new JButton("Show all channels");
        btnShowRawData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                RawDataAllWindow rdaw = new RawDataAllWindow(dataSet);
                rdaw.setVisible(true);
                
            }
        });
        btnShowAllDataWrapperPanel.add(btnShowRawData, BorderLayout.NORTH);
        
        JPanel btnShowRawDataWrapperPanel = new JPanel();
        btnShowRawDataWrapperPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        btnShowRawDataWrapperPanel.setBackground(Color.WHITE);
        buttonWrapperGeneralPanel.add(btnShowRawDataWrapperPanel);
        btnShowRawDataWrapperPanel.setLayout(new BorderLayout(0, 0));
        
        JButton btnShowRawData_1 = new JButton("Show raw data");
        btnShowRawData_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                RawDataExplorer rde = new RawDataExplorer(dataSet);
                rde.setVisible(true);
                
            }
        });
        btnShowRawDataWrapperPanel.add(btnShowRawData_1);
        
        JPanel aboutPanel = new JPanel();
        aboutPanel.setBackground(Color.WHITE);
        dataReviewCenterPanel.add(aboutPanel, BorderLayout.CENTER);
        aboutPanel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblAbout = new JLabel("About");
        lblAbout.setFont(new Font("Tahoma", Font.PLAIN, 20));
        aboutPanel.add(lblAbout, BorderLayout.NORTH);
        
        JScrollPane scrollPane_1 = new JScrollPane();
        aboutPanel.add(scrollPane_1, BorderLayout.CENTER);
        
        aboutPane = new JTextPane();
        aboutPane.setEditable(false);
        scrollPane_1.setViewportView(aboutPane);
        
        JPanel dataInjectionPanel = new JPanel();
        tabbedPane.addTab("Inject data", null, dataInjectionPanel, null);
        dataInjectionPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel channelContainerInjectionPanel = new JPanel();
        channelContainerInjectionPanel.setBackground(Color.WHITE);
        channelContainerInjectionPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
        dataInjectionPanel.add(channelContainerInjectionPanel, BorderLayout.WEST);
        channelContainerInjectionPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel channelInjectionWrapperPanel = new JPanel();
        channelInjectionWrapperPanel.setBackground(Color.WHITE);
        channelContainerInjectionPanel.add(channelInjectionWrapperPanel);
        channelInjectionWrapperPanel.setLayout(new BorderLayout(0, 0));
        
        JLabel lblAvailableChannelInjection = new JLabel("Available Channels");
        lblAvailableChannelInjection.setHorizontalAlignment(SwingConstants.CENTER);
        lblAvailableChannelInjection.setFont(new Font("Tahoma", Font.PLAIN, 20));
        channelInjectionWrapperPanel.add(lblAvailableChannelInjection, BorderLayout.NORTH);
        
        JScrollPane scrollPane_2 = new JScrollPane();
        channelInjectionWrapperPanel.add(scrollPane_2, BorderLayout.CENTER);
        
        listAvailableInjectionModel = new DefaultListModel();
        CheckCellRenderer cellRenderer = new CheckCellRenderer();
        
        listAvailableChannelInjection = new JList(listAvailableInjectionModel);
        listAvailableChannelInjection.setBackground(UIManager.getColor("Button.background"));
        
        listAvailableChannelInjection.setCellRenderer(cellRenderer);
                
        listAvailableChannelInjection.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listAvailableChannelInjection.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent event) {
            JList list = (JList) event.getSource();
            int index = list.locationToIndex(event.getPoint());// Get index of item
                                                               // clicked
            CheckListItem item = (CheckListItem) list.getModel()
                .getElementAt(index);
            item.setSelected(!item.isSelected()); // Toggle selected state
            list.repaint(list.getCellBounds(index, index));// Repaint cell
          }
        });
        
        scrollPane_2.setViewportView(listAvailableChannelInjection);
        
        JPanel dataInjectionCenterPanel = new JPanel();
        dataInjectionCenterPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
        dataInjectionCenterPanel.setBackground(Color.WHITE);
        dataInjectionPanel.add(dataInjectionCenterPanel, BorderLayout.CENTER);
        dataInjectionCenterPanel.setLayout(new BorderLayout(0, 0));
        
        JTabbedPane injectionTabbedPane = new JTabbedPane(JTabbedPane.TOP);
        injectionTabbedPane.setBorder(new EmptyBorder(10, 0, 0, 0));
        dataInjectionCenterPanel.add(injectionTabbedPane, BorderLayout.CENTER);
        
        JPanel manualInjectionPanel = new JPanel();
        injectionTabbedPane.addTab("Manual", null, manualInjectionPanel, null);
        manualInjectionPanel.setLayout(new BorderLayout(0, 0));
        
        manualInjectionPanel.add(manualInjector.onDraw(),BorderLayout.CENTER);
        
        JPanel autoInjectionPanel = new JPanel();
        injectionTabbedPane.addTab("Auto", null, autoInjectionPanel, null);
        autoInjectionPanel.setLayout(new BorderLayout(0, 0));
        
        autoInjectionPanel.add(autoInjector.onDraw(),BorderLayout.CENTER);
        
        JPanel realtimeInjectionPanel = new JPanel();
        injectionTabbedPane.addTab("Real-time", null, realtimeInjectionPanel, null);
        realtimeInjectionPanel.setLayout(new BorderLayout(0, 0));
        
        realtimeInjectionPanel.add(realTimeInjector.onDraw(),BorderLayout.CENTER);
        
        JLabel lblInjectionMethod = new JLabel("Injection method");
        lblInjectionMethod.setHorizontalAlignment(SwingConstants.CENTER);
        lblInjectionMethod.setFont(new Font("Tahoma", Font.PLAIN, 20));
        dataInjectionCenterPanel.add(lblInjectionMethod, BorderLayout.NORTH);
        
        JPanel dataOptionPanel = new JPanel();
        dataOptionPanel.setBackground(Color.WHITE);
        tabbedPane.addTab("Options", null, dataOptionPanel, null);
        dataOptionPanel.setLayout(new BorderLayout(0, 0));
        
        JScrollPane optionScrollPane = new JScrollPane();
        dataOptionPanel.add(optionScrollPane);
        
        JPanel optionPanelContainer = new JPanel();
        optionPanelContainer.setBackground(Color.WHITE);
        optionScrollPane.setViewportView(optionPanelContainer);
        GridBagLayout gbl_optionPanelContainer = new GridBagLayout();
        gbl_optionPanelContainer.columnWidths = new int[]{20, 0, 0};
        gbl_optionPanelContainer.rowHeights = new int[]{20, 0, 20, 0, 0, 20, 0, 0, 0, 0, 0};
        gbl_optionPanelContainer.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_optionPanelContainer.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        optionPanelContainer.setLayout(gbl_optionPanelContainer);
        
        JLabel lblOptions = new JLabel("Options");
        GridBagConstraints gbc_lblOptions = new GridBagConstraints();
        gbc_lblOptions.anchor = GridBagConstraints.WEST;
        gbc_lblOptions.insets = new Insets(0, 0, 5, 0);
        gbc_lblOptions.gridx = 1;
        gbc_lblOptions.gridy = 1;
        optionPanelContainer.add(lblOptions, gbc_lblOptions);
        lblOptions.setFont(new Font("Tahoma", Font.PLAIN, 20));
        
        JLabel lblGeneral_1 = new JLabel("General");
        GridBagConstraints gbc_lblGeneral_1 = new GridBagConstraints();
        gbc_lblGeneral_1.anchor = GridBagConstraints.WEST;
        gbc_lblGeneral_1.insets = new Insets(0, 0, 5, 0);
        gbc_lblGeneral_1.gridx = 1;
        gbc_lblGeneral_1.gridy = 3;
        optionPanelContainer.add(lblGeneral_1, gbc_lblGeneral_1);
        lblGeneral_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
        
        chckbxIgnoreValueWithoutDate = new JCheckBox("Ignore values without date (Not for CSV)");
        GridBagConstraints gbc_chckbxIgnoreValueWithoutDate = new GridBagConstraints();
        gbc_chckbxIgnoreValueWithoutDate.anchor = GridBagConstraints.WEST;
        gbc_chckbxIgnoreValueWithoutDate.insets = new Insets(0, 0, 5, 0);
        gbc_chckbxIgnoreValueWithoutDate.gridx = 1;
        gbc_chckbxIgnoreValueWithoutDate.gridy = 4;
        optionPanelContainer.add(chckbxIgnoreValueWithoutDate, gbc_chckbxIgnoreValueWithoutDate);
        chckbxIgnoreValueWithoutDate.setBackground(Color.WHITE);
        chckbxIgnoreValueWithoutDate.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e){
                //If is selected, a csv file is loaded and the csv has some data
                if(chckbxIgnoreValueWithoutDate.isSelected()){
                    dataSet.getLogParser().setIgnoreEmptyTimes(true);
                    //Reparse as current type
                    dataSet.getLogParser().requestReparse(thisObject, dataSet.getLogParser().getDetectedFileType());
                }else{
                    dataSet.getLogParser().setIgnoreEmptyTimes(false);
                    //Reparse as current type
                    dataSet.getLogParser().requestReparse(thisObject, dataSet.getLogParser().getDetectedFileType());
                }
            }
          });
        
        JLabel lblCsv = new JLabel("CSV");
        GridBagConstraints gbc_lblCsv = new GridBagConstraints();
        gbc_lblCsv.anchor = GridBagConstraints.WEST;
        gbc_lblCsv.insets = new Insets(0, 0, 5, 0);
        gbc_lblCsv.gridx = 1;
        gbc_lblCsv.gridy = 6;
        optionPanelContainer.add(lblCsv, gbc_lblCsv);
        lblCsv.setFont(new Font("Tahoma", Font.PLAIN, 18));
        
        chckbxCSVInterpretColumnAsTimestamp = new JCheckBox("Interpret a column as a timestamp");
        GridBagConstraints gbc_chckbxCSVInterpretColumnAsTimestamp = new GridBagConstraints();
        gbc_chckbxCSVInterpretColumnAsTimestamp.anchor = GridBagConstraints.WEST;
        gbc_chckbxCSVInterpretColumnAsTimestamp.insets = new Insets(0, 0, 5, 0);
        gbc_chckbxCSVInterpretColumnAsTimestamp.gridx = 1;
        gbc_chckbxCSVInterpretColumnAsTimestamp.gridy = 7;
        optionPanelContainer.add(chckbxCSVInterpretColumnAsTimestamp, gbc_chckbxCSVInterpretColumnAsTimestamp);
        chckbxCSVInterpretColumnAsTimestamp.setBackground(Color.WHITE);
        chckbxCSVInterpretColumnAsTimestamp.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e){
                //If is selected, a csv file is loaded and the csv has some data
                if(chckbxCSVInterpretColumnAsTimestamp.isSelected() && dataSet.getLogParser().getDetectedFileType() == LogParser.FILETYPE_CSV && dataSet.getDataLines().size()>0){
                    CSVTimeStampComboBox.setEnabled(true);
                    dataSet.getLogParser().requestReparseWithCSVTimeStamp(thisObject,CSVTimeStampComboBox.getSelectedIndex());
                }else{
                    CSVTimeStampComboBox.setEnabled(false);
                    //Reparse as current type
                    dataSet.getLogParser().requestReparse(thisObject, dataSet.getLogParser().getDetectedFileType());
                }
            }
          });
        
        lblSelectTheTimestamp = new JLabel("Select the timestamp column");
        GridBagConstraints gbc_lblSelectTheTimestamp = new GridBagConstraints();
        gbc_lblSelectTheTimestamp.anchor = GridBagConstraints.WEST;
        gbc_lblSelectTheTimestamp.insets = new Insets(0, 0, 5, 0);
        gbc_lblSelectTheTimestamp.gridx = 1;
        gbc_lblSelectTheTimestamp.gridy = 8;
        optionPanelContainer.add(lblSelectTheTimestamp, gbc_lblSelectTheTimestamp);
        
        CSVTimeStampComboBox = new JComboBox();
        GridBagConstraints gbc_CSVTimeStampComboBox = new GridBagConstraints();
        gbc_CSVTimeStampComboBox.anchor = GridBagConstraints.WEST;
        gbc_CSVTimeStampComboBox.gridx = 1;
        gbc_CSVTimeStampComboBox.gridy = 9;
        optionPanelContainer.add(CSVTimeStampComboBox, gbc_CSVTimeStampComboBox);
        
        JPanel defaultBottomPanel = new JPanel();
        defaultBottomPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.add(defaultBottomPanel, BorderLayout.SOUTH);
        defaultBottomPanel.setLayout(new BorderLayout(0, 0));
        
        JPanel innnerTopDefaultPanel = new JPanel();
        defaultBottomPanel.add(innnerTopDefaultPanel, BorderLayout.WEST);
        innnerTopDefaultPanel.setLayout(new BorderLayout(0, 0));
        
        logStatus = new JLabel("File is being parsed, please wait...");
        logStatus.setFont(new Font("Tahoma", Font.PLAIN, 20));
        innnerTopDefaultPanel.add(logStatus);
        
        JButton btnParseLog = new JButton("Parsing Log");
        btnParseLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                ParsingLogReader plr = new ParsingLogReader(dataSet);
                plr.setVisible(true);
                
            }
        });
        defaultBottomPanel.add(btnParseLog, BorderLayout.EAST);
     
        loadDataFile();
        
    }
    
    public void loadDataFile(){
        
        //Load and parse the file
        dataSet.loadFile();
        
        //UI refresh with parsed data
        fullUILoad();
        
        //Set selected index from parsed data
        fileInterpretationMethodComboBox.setSelectedIndex(dataSet.getLogParser().getDetectedFileType());
        
        //Only instanciate listener once we've set the default selected index
        fileInterpretationMethodComboBox.addItemListener(new ItemListener(){
            @Override
            public void itemStateChanged(ItemEvent event) {
                //Only triggers when the state changes
                if(event.getStateChange() == ItemEvent.SELECTED) {
                    //Get selected index
                    int fileTypeIndex = fileInterpretationMethodComboBox.getSelectedIndex();
                    //If selected index exists
                    if(fileTypeIndex>=0){
                        //Edge case for a CSV file with an enabled timstamp transfer
                        if(fileTypeIndex == LogParser.FILETYPE_CSV && chckbxCSVInterpretColumnAsTimestamp.isSelected()){
                            dataSet.getLogParser().requestReparseWithCSVTimeStamp(thisObject,0);
                            return;
                        }
                        //Request reparsing of the data set
                        dataSet.getLogParser().requestReparse(thisObject,fileTypeIndex);
                    }
                }
            }
        });
        
    }
    
    public void fullUILoad(){
        
        loadChannels();
        
        logStatus.setText(dataSet.getLogParser().getParsingReturnStatus());
        
        loadAbout();
        
        autoInjector.onLoad();
        
        manualInjector.onLoad();
        
        realTimeInjector.onLoad();
        
        loadCSVTimeStampSelector();
        
        //Checkbox enable refresh depending on the file type
        if(dataSet.getLogParser().getDetectedFileType() == LogParser.FILETYPE_CSV){
            chckbxIgnoreValueWithoutDate.setEnabled(false);
            chckbxCSVInterpretColumnAsTimestamp.setEnabled(true);
            lblSelectTheTimestamp.setForeground(Color.BLACK);
        }else{
            chckbxIgnoreValueWithoutDate.setEnabled(true);
            chckbxCSVInterpretColumnAsTimestamp.setEnabled(false);
            lblSelectTheTimestamp.setForeground(Color.LIGHT_GRAY);
        }
        
    }
    
    public void loadAbout(){
        
        aboutPane.setText("");
        
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, Color.blue);
        showMsgAbout("File: '" + dataSet.getFile().getAbsolutePath() + "'", attrs);
        showMsgAbout("Lines: " + dataSet.getDataLines().size(), attrs);
        showMsgAbout("Using line separator: '" + dataSet.getLogParser().getDetectedSeparator() + "'", attrs);
        showMsgAbout("Provided headers: " + dataSet.getHeaders().size(), attrs);
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        showMsgAbout("Start date: " + format.format(dataSet.getLogDate()), attrs);
        
    }
    
    public void loadChannels(){
        
        if(dataSet.getDataLines() == null){
            return;
        }
        
        if(dataSet.getDataLines().size()<1){
            return;
        }
        
        listAvailableChannelsModel.clear();
        listAvailableInjectionModel.clear();
        
        for(int i = 0; i < dataSet.getDataLines().get(0).safeGetDataListSize(); i++){
            
            String label = "";
            
            //We have a header
            if(i < dataSet.getHeaders().size()){
                label = " [" + dataSet.getHeaders().get(i) + "]   ( example : " + Tools.limitString(dataSet.getDataLines().get(0).safeGetDataList(i),10) + " )";
            }else{
                label = " [" + i + "]   ( example : " + Tools.limitString(dataSet.getDataLines().get(0).safeGetDataList(i),10) + " )";
            }
            
            listAvailableChannelsModel.addElement(label);
            listAvailableInjectionModel.addElement(new CheckListItem(label));
            
        }
        
    }
    
    //Loads the option to set a column of a CSV file as the timestamp
    private void loadCSVTimeStampSelector(){
        //Remove any old listener from teh combobox
        if(CSVTimeStampComboListener != null){
            CSVTimeStampComboBox.removeItemListener(CSVTimeStampComboListener);   
        }
        //If we have a CSV file
        if(dataSet.getLogParser().getDetectedFileType() == LogParser.FILETYPE_CSV){
            //If header hav been loaded
            if(dataSet.getHeaders().size()>0){
                //Get array
                String[] headers = new String[dataSet.getHeaders().size()];
                headers = dataSet.getHeaders().toArray(headers);
                //Fill combobox
                CSVTimeStampComboBox.setModel(new DefaultComboBoxModel(headers));
            }else{
                //Check that we have some data
                if(dataSet.getDataLines().size()>0){
                    //We need to manually extract the available channels
                    DataItem firstDataItem = dataSet.getDataLines().get(0);
                    //Prepare array
                    String[] headers = new String[firstDataItem.safeGetDataListSize()];
                    //Parse first item
                    for(int i = 0; i < firstDataItem.safeGetDataListSize(); i++){
                        headers[i] = "[Channel "+i+"] ex : " +Tools.limitString(firstDataItem.safeGetDataList(i), 10);
                    }
                    CSVTimeStampComboBox.setModel(new DefaultComboBoxModel(headers));
                }else{
                    //If empty CSV file
                    CSVTimeStampComboBox.setModel(new DefaultComboBoxModel(new String[] {"No channels"}));
                }
               
            }
            //Is the checkbox for this option enabled (And we have data)
            if(chckbxCSVInterpretColumnAsTimestamp.isSelected() && dataSet.getDataLines().size()>0){
                CSVTimeStampComboBox.setEnabled(true);
            }else{
                CSVTimeStampComboBox.setEnabled(false);
            }
        }else{
            //Set as inactive
            CSVTimeStampComboBox.setModel(new DefaultComboBoxModel(new String[] {"No CSV file"}));
            CSVTimeStampComboBox.setEnabled(false);
        }
        //If we have a request to load the combobox preselected at an index
        if(requestCSVTimeStampComboboxSelection>=0){
            //Check that we have enough items
            if(requestCSVTimeStampComboboxSelection<CSVTimeStampComboBox.getItemCount()){
                //Set selected item
                CSVTimeStampComboBox.setSelectedIndex(requestCSVTimeStampComboboxSelection);
                //Absorb request
                requestCSVTimeStampComboboxSelection=-1;
            }
        }
        //Create new item listener for the CSV Timestamp Combobox
        CSVTimeStampComboListener = new ItemListener(){
                @Override
                public void itemStateChanged(ItemEvent event) {
                    //Only triggers when the state changes
                    if(event.getStateChange() == ItemEvent.SELECTED) {
                        //Get selected index
                        int selectedColumn = CSVTimeStampComboBox.getSelectedIndex();
                        //If selected index exists and we are enabled
                        if(selectedColumn>=0 && CSVTimeStampComboBox.isEnabled()){
                            //Reparse with the right column
                            dataSet.getLogParser().requestReparseWithCSVTimeStamp(thisObject,selectedColumn);
                        }
                    }
                }
            };
        //Add new listener
        CSVTimeStampComboBox.addItemListener(CSVTimeStampComboListener);
    }
    
    // Show a text message using the specified AttributeSet
    protected void showMsgAbout(String msg, AttributeSet attrs) {
      Document doc = aboutPane.getDocument();
      msg += "\n";
      try {
        doc.insertString(doc.getLength(), msg, attrs);
      } catch (BadLocationException ex) { ex.printStackTrace(); }
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public AutoInjector getAutoInjector() {
        return autoInjector;
    }

    public void setAutoInjector(AutoInjector autoInjector) {
        this.autoInjector = autoInjector;
    }

    public DefaultListModel getListAvailableInjectionModel() {
        return listAvailableInjectionModel;
    }

    public void setListAvailableInjectionModel(DefaultListModel listAvailableInjectionModel) {
        this.listAvailableInjectionModel = listAvailableInjectionModel;
    }

    public int getRequestCSVTimeStampComboboxSelection() {
        return requestCSVTimeStampComboboxSelection;
    }

    public void setRequestCSVTimeStampComboboxSelection(int requestCSVTimeStampComboboxSelection) {
        this.requestCSVTimeStampComboboxSelection = requestCSVTimeStampComboboxSelection;
    }

}
