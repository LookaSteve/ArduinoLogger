package gui;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;

import Modules.TimerEvent;
import Modules.Tools;
import core.Xeon;
import gui.StatusIndicatorCreation.MyListCellRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JTextArea;

public class EventScheduler extends InternalFrame {

	private Xeon xeon;
	private String title;
	private JLabel lblTime;
	private EventScheduler thisObj;
	private long timeCount = 0;
	private long backTimeCount = 0;
	private Timer countDownThread;
	private Thread visualCountDownThread;
	private boolean isNegative = true;
	private boolean backIsNegative = true;
	private JTextArea textArea;
	private DefaultListModel eventListModel;
	private TimerEventListCellRenderer cellRenderer;
	private JList list_1;
	private boolean isInHold = false;
	private TimeEventEditor ted;
	
	public EventScheduler(Xeon xeon,String title) {
		
		super(xeon,0,"EVENTSHEDULER");
		this.xeon = xeon;
		this.title = title;
		this.thisObj = this;
		
		setBounds(100, 100, 628, 300);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.8);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane_1, BorderLayout.CENTER);
		
		JPanel panel_10 = new JPanel();
		splitPane_1.setRightComponent(panel_10);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_9 = new JPanel();
		panel_10.add(panel_9, BorderLayout.NORTH);
		panel_9.setLayout(new BorderLayout(0, 0));
		
		JLabel lblConsoleLog = new JLabel(" Console Log");
		lblConsoleLog.setFont(new Font("Tahoma", Font.PLAIN, 17));
		panel_9.add(lblConsoleLog, BorderLayout.WEST);
		
		JButton btnX = new JButton("Clear console");
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textArea.setText("");
				
			}
		});
		panel_9.add(btnX, BorderLayout.EAST);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_10.add(scrollPane_1, BorderLayout.CENTER);
		
		textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);
		textArea.setEditable(false);
		
		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.2);
		splitPane_1.setLeftComponent(splitPane_2);
		
		JPanel panel = new JPanel();
		splitPane_2.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.NORTH);
		
		JLabel lblEvents = new JLabel("Events");
		lblEvents.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_2.add(lblEvents);
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.CENTER);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ted = new TimeEventEditor(thisObj);
				ted.setVisible(true);
				
			}
		});
		panel_3.add(btnAdd);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(list_1.getSelectedIndex() != -1){
					
					if(ted != null){
						if(!ted.isVisible()){
							ted = new TimeEventEditor(thisObj,list_1.getSelectedIndex(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getName(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getExecutionTime(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getCommand(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getColor(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).isPostZero());
							ted.setVisible(true);
						}else{
							ted.requestFocus();
						}
					}else{
						ted = new TimeEventEditor(thisObj,list_1.getSelectedIndex(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getName(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getExecutionTime(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getCommand(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).getColor(),cellRenderer.getListOfEvents().get(list_1.getSelectedIndex()).isPostZero());
						ted.setVisible(true);
					}
					
				}
				
			}
		});
		panel_3.add(btnEdit);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(list_1.getSelectedIndex() != -1){
					
					cellRenderer.getListOfEvents().remove(list_1.getSelectedIndex());
					
					eventListModel.remove(list_1.getSelectedIndex());
					
					sortEvents();
					
				}
				
			}
		});
		panel_3.add(btnRemove);
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		eventListModel = new DefaultListModel();
		list_1 = new JList(eventListModel);
		cellRenderer = new TimerEventListCellRenderer();
		list_1.setCellRenderer(cellRenderer);
		scrollPane.setViewportView(list_1);
		
		JPanel panel_4 = new JPanel();
		splitPane_2.setRightComponent(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_4.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		lblTime = new JLabel("T - <>");
		lblTime.setBackground(Color.WHITE);
		lblTime.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTime.setHorizontalAlignment(SwingConstants.CENTER);
		panel_5.add(lblTime);
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6, BorderLayout.NORTH);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_6.add(panel_7, BorderLayout.NORTH);
		
		JLabel lblCountdown = new JLabel("Countdown");
		lblCountdown.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_7.add(lblCountdown);
		
		JPanel panel_8 = new JPanel();
		panel_6.add(panel_8, BorderLayout.CENTER);
		
		JButton btnSetStart = new JButton("Set start");
		btnSetStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TimeSetter ts = new TimeSetter(thisObj);
				ts.setVisible(true);
				
			}
		});
		panel_8.add(btnSetStart);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(countDownThread == null){
					
					startCountDown();
					
				}
				
				
			}
		});
		panel_8.add(btnStart);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(countDownThread != null){
					
					countDownThread.cancel();
					
					timeCount = backTimeCount;
					
					isNegative = backIsNegative;
					
					countDownThread = null;
					
					isInHold = false;
					
					if(visualCountDownThread != null){
						
						visualCountDownThread.stop();
						
						visualCountDownThread = null;
						
					}
					
				}else{
					
					isInHold = false;
					
					timeCount = backTimeCount;
					
					isNegative = backIsNegative;
					
					paintCountDown();
					
				}
				
				resetEventsFire();
				
			}
		});
		panel_8.add(btnStop);
		
		JButton btnHold = new JButton("Hold");
		btnHold.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(countDownThread != null){
					
					isInHold = true;
					
					countDownThread.cancel();
					
					countDownThread = null;
					
					if(visualCountDownThread != null){
						
						visualCountDownThread.stop();
						
						visualCountDownThread = null;
						
					}
					
				}else{
					
					if(isInHold){
						
						startCountDown();
						
					}
					
				}
				
			}
		});
		panel_8.add(btnHold);

		paintCountDown();
		
	}
	
	@Override
	public void windowClosed(){
		
		if(countDownThread != null){
			
			countDownThread.cancel();
			
			countDownThread = null;
			
			isInHold = false;
			
			if(visualCountDownThread != null){
				
				visualCountDownThread.stop();
				
				visualCountDownThread = null;
				
			}
			
		}
		
	}
	
	public void startCountDown(){
		
		if(isInHold){
			
			isInHold = false;
			
		}else{
			
			backTimeCount = timeCount;
			
			backIsNegative = isNegative;
			
		}
		
		countDownThread = new Timer();
		
		countDownThread.scheduleAtFixedRate(new TimerTask() {

	        public void run() {

	        	if(isNegative){
	    			
	    			timeCount--;
	    			
	    		}else{
	    			
	    			timeCount++;
	    			
	    		}
	    		if(timeCount <= 0){
		    		
	    			isNegative = false;
		    		
		    	}

	        }
	    }, 1, 1);
		
		visualCountDownThread = new Thread(){
		    public void run(){
		    	
		    	while(true){
		    		
		    		cycleEvents();
		    		
		    		paintCountDown();
			    	
			    	try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
		    		
		    	}
		    	
		    }
		};

		visualCountDownThread.start();
		
	}
	
	public synchronized void paintCountDown(){
		
		if(isNegative){
			lblTime.setText("T - " + Tools.quickTimeFormat(timeCount));
		}else{
			lblTime.setText("T + " + Tools.quickTimeFormat(timeCount));
		}
		
	}
	
	public void cycleEvents(){
		
		for(int i = 0; i < cellRenderer.getListOfEvents().size(); i++){
			
			if(!cellRenderer.getListOfEvents().get(i).isHasFired()){
				
				cellRenderer.getListOfEvents().get(i).cycle(timeCount);
				
			}
			
		}
		
	}
	
	public void sortEvents(){
		
		ArrayList<TimerEvent> resNeg = new ArrayList<TimerEvent>();
		
		ArrayList<TimerEvent> resPos = new ArrayList<TimerEvent>();
		
		for (int i = 0; i < cellRenderer.getListOfEvents().size(); i++){
			
			if(cellRenderer.getListOfEvents().get(i).isPostZero()){

				resPos.add(cellRenderer.getListOfEvents().get(i));
				
			}else{

				resNeg.add(cellRenderer.getListOfEvents().get(i));
				
			}
			
		}
		
		Sorter sorter = new Sorter();
		ReverseSorter reversesorter = new ReverseSorter();
		Collections.sort(resPos, sorter);
		Collections.sort(resNeg, reversesorter);
		
		resNeg.addAll(resPos);
		
		cellRenderer.setListOfEvents(resNeg);
		
		paintList();
		
	}
	
	public void paintList(){
		
		eventListModel.clear();
		
		for(int i = 0; i < cellRenderer.getListOfEvents().size(); i ++){
			
			String formatedTime = Tools.quickTimeFormat(cellRenderer.getListOfEvents().get(i).getExecutionTime());
			if(!cellRenderer.getListOfEvents().get(i).isPostZero()){
				formatedTime = "- " + formatedTime;
			}
			eventListModel.addElement(cellRenderer.getListOfEvents().get(i).getName()+" : "+formatedTime);
			
		}
		
	}
	
	public void resetEventsFire(){
		
		for(int i = 0; i < cellRenderer.getListOfEvents().size(); i++){
			
			cellRenderer.getListOfEvents().get(i).setHasFired(false);
			
		}
		
	}
	
	public void consoleLog(String s){
		
		textArea.append(s + System.getProperty("line.separator"));
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getTimeCount() {
		return timeCount;
	}

	public void setTimeCount(long timeCount) {
		this.timeCount = timeCount;
	}

	public synchronized DefaultListModel getEventListModel() {
		return eventListModel;
	}

	public synchronized void setEventListModel(DefaultListModel eventListModel) {
		this.eventListModel = eventListModel;
	}

	public synchronized TimerEventListCellRenderer getCellRenderer() {
		return cellRenderer;
	}

	public synchronized void setCellRenderer(TimerEventListCellRenderer cellRenderer) {
		this.cellRenderer = cellRenderer;
	}

	public boolean isNegative() {
		return isNegative;
	}

	public void setNegative(boolean isNegative) {
		this.isNegative = isNegative;
	}

	public Xeon getXeon() {
		return xeon;
	}

	public void setXeon(Xeon xeon) {
		this.xeon = xeon;
	}

	public long getBackTimeCount() {
		return backTimeCount;
	}

	public void setBackTimeCount(long backTimeCount) {
		this.backTimeCount = backTimeCount;
	}

	public boolean isBackIsNegative() {
		return backIsNegative;
	}

	public void setBackIsNegative(boolean backIsNegative) {
		this.backIsNegative = backIsNegative;
	}



	public static class TimerEventListCellRenderer extends DefaultListCellRenderer {
		
		private ArrayList<TimerEvent> listOfEvents = new ArrayList<TimerEvent>();
		
        public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus ) {
            
        	Component c = super.getListCellRendererComponent( list, value, index, isSelected, cellHasFocus );
        	
        	c.setBackground(listOfEvents.get(index).getColor());
        	
        	c.setForeground(Tools.getContrastColor(listOfEvents.get(index).getColor()));
            
        	return c;
            
        }

		public synchronized ArrayList<TimerEvent> getListOfEvents() {
			return listOfEvents;
		}

		public synchronized void setListOfEvents(ArrayList<TimerEvent> listOfEvents) {
			this.listOfEvents = listOfEvents;
		}
        
        
        
    }
	
	class Sorter implements Comparator<TimerEvent>{
		@Override
		public int compare(TimerEvent a, TimerEvent b){
		    if(a.getExecutionTime() >= b.getExecutionTime()){
		        return 1;
		    }else if(a.getExecutionTime() < b.getExecutionTime()){
		        return -1;
		    }
			return 0;
		}
	}
	class ReverseSorter implements Comparator<TimerEvent>{
		@Override
		public int compare(TimerEvent a, TimerEvent b){
		    if(a.getExecutionTime() >= b.getExecutionTime()){
		        return -1;
		    }else if(a.getExecutionTime() < b.getExecutionTime()){
		        return 1;
		    }
			return 0;
		}
	}
}
