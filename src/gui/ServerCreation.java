package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.NumericField;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class ServerCreation extends JFrame {

	private JPanel contentPane;
	private JTextField txtRemoteServer;
	private NumericField textField_2;
	
	private DenebUI denebUI;
	private JCheckBox checkBox;

	public ServerCreation(final DenebUI denebUI) {
		
		this.denebUI = denebUI;
		
		setResizable(false);
		setTitle("New Server");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 247);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel label = new JLabel("      ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel.add(label, gbc_label);
		
		JLabel lblServerName = new JLabel("Server name");
		GridBagConstraints gbc_lblServerName = new GridBagConstraints();
		gbc_lblServerName.anchor = GridBagConstraints.EAST;
		gbc_lblServerName.insets = new Insets(0, 0, 5, 5);
		gbc_lblServerName.gridx = 1;
		gbc_lblServerName.gridy = 1;
		panel.add(lblServerName, gbc_lblServerName);
		
		txtRemoteServer = new JTextField();
		txtRemoteServer.setText("remote server");
		GridBagConstraints gbc_txtRemoteServer = new GridBagConstraints();
		gbc_txtRemoteServer.insets = new Insets(0, 0, 5, 5);
		gbc_txtRemoteServer.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRemoteServer.gridx = 2;
		gbc_txtRemoteServer.gridy = 1;
		panel.add(txtRemoteServer, gbc_txtRemoteServer);
		txtRemoteServer.setColumns(10);
		
		JLabel lblPort = new JLabel("Port");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.anchor = GridBagConstraints.EAST;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 1;
		gbc_lblPort.gridy = 2;
		panel.add(lblPort, gbc_lblPort);
		
		textField_2 = new NumericField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 2;
		gbc_textField_2.gridy = 2;
		panel.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		textField_2.setValue(denebUI.getXeon().getServerCommunicationPort());
		
		JButton button = textField_2.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 3;
		gbc_button.gridy = 2;
		panel.add(button, gbc_button);
		
		JButton button_1 = textField_2.generatePlusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 4;
		gbc_button_1.gridy = 2;
		panel.add(button_1, gbc_button_1);
		
		JLabel lblAutoConnect = new JLabel("Auto connect");
		GridBagConstraints gbc_lblAutoConnect = new GridBagConstraints();
		gbc_lblAutoConnect.insets = new Insets(0, 0, 0, 5);
		gbc_lblAutoConnect.gridx = 1;
		gbc_lblAutoConnect.gridy = 3;
		panel.add(lblAutoConnect, gbc_lblAutoConnect);
		
		checkBox = new JCheckBox("");
		checkBox.setSelected(true);
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.anchor = GridBagConstraints.WEST;
		gbc_checkBox.insets = new Insets(0, 0, 0, 5);
		gbc_checkBox.gridx = 2;
		gbc_checkBox.gridy = 3;
		panel.add(checkBox, gbc_checkBox);

		
		JLabel label_1 = new JLabel("      ");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.gridx = 5;
		gbc_label_1.gridy = 3;
		panel.add(label_1, gbc_label_1);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblNewClient = new JLabel("New Server");
		lblNewClient.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblNewClient);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				denebUI.getInternalFrameManager().addServer(txtRemoteServer.getText(),textField_2.getValue(),checkBox.isSelected(),null);
				dispose();
				
			}
		});
		panel_2.add(btnAdd, BorderLayout.EAST);
	}

}
