package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import Modules.ColorButton;
import Modules.NumericField;
import Modules.StatusObject;
import Modules.Tools;

import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

public class StatusEditor extends JFrame {

	private JPanel contentPane;
	private JTextField txtNewStatus;
	private NumericField textField_1;
	private StatusEditor thisObj;
	private ColorButton button_1;
	
	private StatusIndicatorCreation sic;
	
	private int idList = -1;
	
	public StatusEditor(final StatusIndicatorCreation sic) {
		this.sic = sic;
		this.thisObj = this;
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 515, 247);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		initInterface(1,"New Status",Color.red);
		
	}
	
	public StatusEditor(final StatusIndicatorCreation sic,int idList,int indexStatus,String statusName,Color colorStatus) {
		this.sic = sic;
		this.thisObj = this;
		this.idList = idList;
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 515, 247);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		initInterface(indexStatus,statusName,colorStatus);
		
	}
	
	private void initInterface(int indexStatus,String statusName,Color statusColor){
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{108, 292, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{26, 26, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblStatusName = new JLabel(" Status name ");
		GridBagConstraints gbc_lblStatusName = new GridBagConstraints();
		gbc_lblStatusName.anchor = GridBagConstraints.EAST;
		gbc_lblStatusName.insets = new Insets(0, 0, 5, 5);
		gbc_lblStatusName.gridx = 0;
		gbc_lblStatusName.gridy = 1;
		panel.add(lblStatusName, gbc_lblStatusName);
		
		txtNewStatus = new JTextField();
		txtNewStatus.setText(statusName);
		GridBagConstraints gbc_txtNewStatus = new GridBagConstraints();
		gbc_txtNewStatus.anchor = GridBagConstraints.NORTH;
		gbc_txtNewStatus.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNewStatus.insets = new Insets(0, 0, 5, 5);
		gbc_txtNewStatus.gridx = 1;
		gbc_txtNewStatus.gridy = 1;
		panel.add(txtNewStatus, gbc_txtNewStatus);
		txtNewStatus.setColumns(10);
		
		JLabel lblStatusId = new JLabel("Status value ");
		GridBagConstraints gbc_lblStatusId = new GridBagConstraints();
		gbc_lblStatusId.anchor = GridBagConstraints.EAST;
		gbc_lblStatusId.insets = new Insets(0, 0, 5, 5);
		gbc_lblStatusId.gridx = 0;
		gbc_lblStatusId.gridy = 2;
		panel.add(lblStatusId, gbc_lblStatusId);
		
		textField_1 = new NumericField();
		textField_1.setValue(indexStatus);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.anchor = GridBagConstraints.NORTH;
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		panel.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		JButton button_2 = textField_1.generateMinusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.insets = new Insets(0, 0, 5, 5);
		gbc_button_2.gridx = 2;
		gbc_button_2.gridy = 2;
		panel.add(button_2, gbc_button_2);
		
		JButton buttonIncrement = textField_1.generatePlusButton();
		GridBagConstraints gbc_button_Increment = new GridBagConstraints();
		gbc_button_Increment.fill = GridBagConstraints.BOTH;
		gbc_button_Increment.insets = new Insets(0, 0, 5, 5);
		gbc_button_Increment.gridx = 3;
		gbc_button_Increment.gridy = 2;
		panel.add(buttonIncrement, gbc_button_Increment);
		
		JLabel lblStatusColor = new JLabel("Status Color ");
		GridBagConstraints gbc_lblStatusColor = new GridBagConstraints();
		gbc_lblStatusColor.anchor = GridBagConstraints.EAST;
		gbc_lblStatusColor.insets = new Insets(0, 0, 0, 5);
		gbc_lblStatusColor.gridx = 0;
		gbc_lblStatusColor.gridy = 3;
		panel.add(lblStatusColor, gbc_lblStatusColor);
		
		button_1 = new ColorButton(this,statusColor,"Choose Background Color");
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_button_1.insets = new Insets(0, 0, 0, 5);
		gbc_button_1.gridx = 1;
		gbc_button_1.gridy = 3;
		panel.add(button_1, gbc_button_1);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblStatus = new JLabel(statusName);
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblStatus);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JButton button = new JButton("Add");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(idList == -1){
					StatusObject so = new StatusObject(txtNewStatus.getText(), textField_1.getValue(), thisObj.getStatusColor());
					sic.getCellRenderer().getStatusList().add(so);
					sic.getListModel().addElement("["+so.getStatusId()+"]   "+so.getStatusName());
				}else{
					if(sic.getListModel().size()-1 >= idList){
						StatusObject so = new StatusObject(txtNewStatus.getText(), textField_1.getValue(), thisObj.getStatusColor());
						sic.getCellRenderer().getStatusList().set(idList, so);
						sic.getListModel().set(idList, "["+so.getStatusId()+"]   "+so.getStatusName());
					}
				}
				
				thisObj.dispose();
				
			}
		});
		panel_2.add(button, BorderLayout.EAST);
		
	}

	public Color getStatusColor() {
		return button_1.getColor();
	}
	
	

}
