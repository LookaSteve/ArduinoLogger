package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logger.DataItem;
import Logger.DataSet;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;

public class RawDataAllWindow extends JFrame {

	private JPanel contentPane;
	
	private DefaultListModel listModel;
	private JTable table;
	private JScrollPane scrollPane;

	public RawDataAllWindow(DataSet ds) {
	    setTitle("Data Explorer");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("All channels ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(lblNewLabel);
		
		listModel = new DefaultListModel();
        table = new JTable();
        table.setModel(getTableModel(ds));
		
		scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane, BorderLayout.CENTER);
	
	}
	
	private DefaultTableModel getTableModel(DataSet ds){
	    
	    int extraItemCount = 2;
	    
	    DataItem sampleItem = ds.getDataLines().get(0);
	    String[] headers = new String[sampleItem.safeGetDataListSize()+extraItemCount];
	    Object[][] values = new Object [ds.getDataLines().size()][sampleItem.safeGetDataListSize()+1];
	    
	    headers[0] = "Line";
	    headers[1] = "Time";
	    
	    //Getting headers
	    for(int i = 0; i < sampleItem.safeGetDataListSize(); i++){
	        if(i < ds.getHeaders().size()){
	            headers[extraItemCount+i] = ds.getHeaders().get(i);
	        }else{
	            headers[extraItemCount+i] = "Channel " + i;
	        }
	    }
	    
	    //Gettings values
	    for(int i = 0 ; i < ds.getDataLines().size(); i++){
            
	        Object[] valueLine = new Object[ds.getDataLines().get(i).safeGetDataListSize()+extraItemCount];
	        
	        valueLine[0] = i;
	        valueLine[1] = ds.getDataLines().get(i).getTime();
	        
            for(int j = 0; j < ds.getDataLines().get(i).safeGetDataListSize(); j++){
                
                valueLine[extraItemCount+j] = ds.getDataLines().get(i).safeGetDataList(j);              
                
            }
            
            values[i] = valueLine;
            
        }
	    
	    return new DefaultTableModel(values,headers);
	    
	}

}
