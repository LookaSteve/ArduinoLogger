package gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Modules.ColorButton;
import Modules.NumericField;
import Modules.Tools;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;

public class GaugeCreation extends JFrame {

	private JPanel contentPane;

	private DenebUI denui;
	private JTextField txtNewGauge;
	private JTextField txtNewGauge_1;
	private NumericField textField;
	private NumericField textField_1;
	private NumericField textField_2;
	
	private ColorButton btnLowColor;
	private ColorButton btnHighColor;
	private ColorButton btnBackColor;
	
	public GaugeCreation(final DenebUI denui) {
		this.denui = denui;
		setTitle("New Gauge");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 536, 413);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel label = new JLabel(" ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel.add(label, gbc_label);
		
		JLabel lblWindowTitle = new JLabel("Window Title ");
		GridBagConstraints gbc_lblWindowTitle = new GridBagConstraints();
		gbc_lblWindowTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblWindowTitle.anchor = GridBagConstraints.EAST;
		gbc_lblWindowTitle.gridx = 0;
		gbc_lblWindowTitle.gridy = 1;
		panel.add(lblWindowTitle, gbc_lblWindowTitle);
		
		txtNewGauge = new JTextField();
		txtNewGauge.setText("New Gauge");
		GridBagConstraints gbc_txtNewGauge = new GridBagConstraints();
		gbc_txtNewGauge.insets = new Insets(0, 0, 5, 5);
		gbc_txtNewGauge.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNewGauge.gridx = 1;
		gbc_txtNewGauge.gridy = 1;
		panel.add(txtNewGauge, gbc_txtNewGauge);
		txtNewGauge.setColumns(10);
		
		JLabel lblGaugeTitle = new JLabel("Gauge Title");
		GridBagConstraints gbc_lblGaugeTitle = new GridBagConstraints();
		gbc_lblGaugeTitle.anchor = GridBagConstraints.EAST;
		gbc_lblGaugeTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblGaugeTitle.gridx = 0;
		gbc_lblGaugeTitle.gridy = 2;
		panel.add(lblGaugeTitle, gbc_lblGaugeTitle);
		
		txtNewGauge_1 = new JTextField();
		txtNewGauge_1.setText("New Gauge");
		GridBagConstraints gbc_txtNewGauge_1 = new GridBagConstraints();
		gbc_txtNewGauge_1.insets = new Insets(0, 0, 5, 5);
		gbc_txtNewGauge_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNewGauge_1.gridx = 1;
		gbc_txtNewGauge_1.gridy = 2;
		panel.add(txtNewGauge_1, gbc_txtNewGauge_1);
		txtNewGauge_1.setColumns(10);
		
		JLabel lblReferenceIndex = new JLabel("Channel Index");
		GridBagConstraints gbc_lblReferenceIndex = new GridBagConstraints();
		gbc_lblReferenceIndex.anchor = GridBagConstraints.EAST;
		gbc_lblReferenceIndex.insets = new Insets(0, 0, 5, 5);
		gbc_lblReferenceIndex.gridx = 0;
		gbc_lblReferenceIndex.gridy = 3;
		panel.add(lblReferenceIndex, gbc_lblReferenceIndex);
		
		textField = new NumericField();
		textField.setValue(0);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 3;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton button = textField.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 2;
		gbc_button.gridy = 3;
		panel.add(button, gbc_button);
		
		JButton button_1 = textField.generatePlusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 0);
		gbc_button_1.gridx = 3;
		gbc_button_1.gridy = 3;
		panel.add(button_1, gbc_button_1);
		
		JLabel lblLowestValue = new JLabel("Lowest Value");
		GridBagConstraints gbc_lblLowestValue = new GridBagConstraints();
		gbc_lblLowestValue.anchor = GridBagConstraints.EAST;
		gbc_lblLowestValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblLowestValue.gridx = 0;
		gbc_lblLowestValue.gridy = 4;
		panel.add(lblLowestValue, gbc_lblLowestValue);
		
		textField_1 = new NumericField();
		textField_1.setValue(0);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 4;
		panel.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		JButton button_2 = textField_1.generateMinusButton();
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.insets = new Insets(0, 0, 5, 5);
		gbc_button_2.gridx = 2;
		gbc_button_2.gridy = 4;
		panel.add(button_2, gbc_button_2);
		
		JButton button_4 = textField_1.generatePlusButton();
		GridBagConstraints gbc_button_4 = new GridBagConstraints();
		gbc_button_4.insets = new Insets(0, 0, 5, 0);
		gbc_button_4.gridx = 3;
		gbc_button_4.gridy = 4;
		panel.add(button_4, gbc_button_4);
		
		JLabel lblHihgherValue = new JLabel("Hihghest Value");
		GridBagConstraints gbc_lblHihgherValue = new GridBagConstraints();
		gbc_lblHihgherValue.anchor = GridBagConstraints.EAST;
		gbc_lblHihgherValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblHihgherValue.gridx = 0;
		gbc_lblHihgherValue.gridy = 5;
		panel.add(lblHihgherValue, gbc_lblHihgherValue);
		
		textField_2 = new NumericField();
		textField_2.setValue(100);
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 5;
		panel.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		JButton button_3 = textField_2.generateMinusButton();
		GridBagConstraints gbc_button_3 = new GridBagConstraints();
		gbc_button_3.insets = new Insets(0, 0, 5, 5);
		gbc_button_3.gridx = 2;
		gbc_button_3.gridy = 5;
		panel.add(button_3, gbc_button_3);
		
		JButton button_5 = textField_2.generatePlusButton();
		GridBagConstraints gbc_button_5 = new GridBagConstraints();
		gbc_button_5.insets = new Insets(0, 0, 5, 0);
		gbc_button_5.gridx = 3;
		gbc_button_5.gridy = 5;
		panel.add(button_5, gbc_button_5);
		
		JLabel lblLowColor = new JLabel("Low Color");
		GridBagConstraints gbc_lblLowColor = new GridBagConstraints();
		gbc_lblLowColor.anchor = GridBagConstraints.EAST;
		gbc_lblLowColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblLowColor.gridx = 0;
		gbc_lblLowColor.gridy = 6;
		panel.add(lblLowColor, gbc_lblLowColor);
		
		btnLowColor = new ColorButton(this,Color.green,"Choose Background Color");
		GridBagConstraints gbc_btnLowColor = new GridBagConstraints();
		gbc_btnLowColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLowColor.insets = new Insets(0, 0, 5, 5);
		gbc_btnLowColor.gridx = 1;
		gbc_btnLowColor.gridy = 6;
		panel.add(btnLowColor, gbc_btnLowColor);
		
		JLabel lblHighColor = new JLabel("High Color");
		GridBagConstraints gbc_lblHighColor = new GridBagConstraints();
		gbc_lblHighColor.anchor = GridBagConstraints.EAST;
		gbc_lblHighColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblHighColor.gridx = 0;
		gbc_lblHighColor.gridy = 7;
		panel.add(lblHighColor, gbc_lblHighColor);
		
		btnHighColor = new ColorButton(this,Color.red,"Choose Background Color");
		GridBagConstraints gbc_btnHighColor = new GridBagConstraints();
		gbc_btnHighColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnHighColor.insets = new Insets(0, 0, 5, 5);
		gbc_btnHighColor.gridx = 1;
		gbc_btnHighColor.gridy = 7;
		panel.add(btnHighColor, gbc_btnHighColor);
		
		JLabel lblBackgroundColor = new JLabel("Background Color");
		GridBagConstraints gbc_lblBackgroundColor = new GridBagConstraints();
		gbc_lblBackgroundColor.anchor = GridBagConstraints.EAST;
		gbc_lblBackgroundColor.insets = new Insets(0, 0, 0, 5);
		gbc_lblBackgroundColor.gridx = 0;
		gbc_lblBackgroundColor.gridy = 8;
		panel.add(lblBackgroundColor, gbc_lblBackgroundColor);
		
		btnBackColor = new ColorButton(this,Color.lightGray,"Choose Background Color");
		GridBagConstraints gbc_btnBackColor = new GridBagConstraints();
		gbc_btnBackColor.insets = new Insets(0, 0, 0, 5);
		gbc_btnBackColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnBackColor.gridx = 1;
		gbc_btnBackColor.gridy = 8;
		panel.add(btnBackColor, gbc_btnBackColor);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblNewGauge = new JLabel("New Gauge");
		lblNewGauge.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblNewGauge);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				denui.getInternalFrameManager().addNewGauge(txtNewGauge.getText(), txtNewGauge_1.getText(), textField.getValue(), btnLowColor.getColor(),btnHighColor.getColor(),btnBackColor.getColor(),textField_1.getValue(),textField_2.getValue(),null);
				dispose();
				
			}
		});
		panel_2.setLayout(new BorderLayout(0, 0));
		panel_2.add(btnAdd,BorderLayout.EAST);
	}

}
