package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Modules.NumericField;

import javax.swing.JLabel;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NumericGraphAdvanced extends JFrame {

	private JPanel contentPane;
	private NumericField textField;
	private Boolean isXDisplay = true;
	private Boolean isYDisplay = true;
	private Boolean isRangeAuto = true;
	private int range = 60;

	/**
	 * Create the frame.
	 */
	public NumericGraphAdvanced() {
		setTitle("Numeric Graph Advanced Settings");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		this.addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	           	dispose();
	         }        
	      });    
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel label = new JLabel("      ");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel.add(label, gbc_label);
		
		JLabel lblShowXGraduation = new JLabel(" Show X Graduation ");
		GridBagConstraints gbc_lblShowXGraduation = new GridBagConstraints();
		gbc_lblShowXGraduation.anchor = GridBagConstraints.EAST;
		gbc_lblShowXGraduation.insets = new Insets(0, 0, 5, 5);
		gbc_lblShowXGraduation.gridx = 1;
		gbc_lblShowXGraduation.gridy = 1;
		panel.add(lblShowXGraduation, gbc_lblShowXGraduation);
		
		ChangeListener changeListenerX = new ChangeListener() {
		      public void stateChanged(ChangeEvent changeEvent) {
		        AbstractButton abstractButton = (AbstractButton)changeEvent.getSource();
		        ButtonModel buttonModel = abstractButton.getModel();
		        isXDisplay = buttonModel.isSelected();
		      }
		};
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("");
		chckbxNewCheckBox.setSelected(true);
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox.gridx = 2;
		gbc_chckbxNewCheckBox.gridy = 1;
		panel.add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);
		chckbxNewCheckBox.addChangeListener(changeListenerX);
		
		JLabel lblShowYGraduation = new JLabel(" Show Y Graduation ");
		GridBagConstraints gbc_lblShowYGraduation = new GridBagConstraints();
		gbc_lblShowYGraduation.insets = new Insets(0, 0, 5, 5);
		gbc_lblShowYGraduation.gridx = 1;
		gbc_lblShowYGraduation.gridy = 2;
		panel.add(lblShowYGraduation, gbc_lblShowYGraduation);

		ChangeListener changeListenerY = new ChangeListener() {
		      public void stateChanged(ChangeEvent changeEvent) {
		        AbstractButton abstractButton = (AbstractButton)changeEvent.getSource();
		        ButtonModel buttonModel = abstractButton.getModel();
		        isYDisplay = buttonModel.isSelected();
		      }
		};
		
		JCheckBox chckbxHideYGraduations = new JCheckBox("");
		chckbxHideYGraduations.setSelected(true);
		GridBagConstraints gbc_chckbxHideYGraduations = new GridBagConstraints();
		gbc_chckbxHideYGraduations.anchor = GridBagConstraints.WEST;
		gbc_chckbxHideYGraduations.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxHideYGraduations.gridx = 2;
		gbc_chckbxHideYGraduations.gridy = 2;
		panel.add(chckbxHideYGraduations, gbc_chckbxHideYGraduations);
		chckbxHideYGraduations.addChangeListener(changeListenerY);
		
		JLabel lblHideXGraduations = new JLabel("Time Range ");
		GridBagConstraints gbc_lblHideXGraduations = new GridBagConstraints();
		gbc_lblHideXGraduations.anchor = GridBagConstraints.EAST;
		gbc_lblHideXGraduations.insets = new Insets(0, 0, 5, 5);
		gbc_lblHideXGraduations.gridx = 1;
		gbc_lblHideXGraduations.gridy = 3;
		panel.add(lblHideXGraduations, gbc_lblHideXGraduations);
		
		JLabel lblSetTimeRange = new JLabel("Set time range (s) ");
		GridBagConstraints gbc_lblSetTimeRange = new GridBagConstraints();
		gbc_lblSetTimeRange.insets = new Insets(0, 0, 5, 5);
		gbc_lblSetTimeRange.anchor = GridBagConstraints.EAST;
		gbc_lblSetTimeRange.gridx = 1;
		gbc_lblSetTimeRange.gridy = 4;
		panel.add(lblSetTimeRange, gbc_lblSetTimeRange);
		
		textField = new NumericField();
		textField.setValue(60);
		textField.setEnabled(false);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 4;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		// Define Range Tick ChangeListener
	    ChangeListener changeListenerRange = new ChangeListener() {
	      public void stateChanged(ChangeEvent changeEvent) {
	        AbstractButton abstractButton = (AbstractButton)changeEvent.getSource();
	        ButtonModel buttonModel = abstractButton.getModel();
	        boolean selected = buttonModel.isSelected();
	        if(selected){
	        	if(!isRangeAuto){
	        		isRangeAuto = true;
	        		textField.setEnabled(false);
	        	}
	        }else{
	        	if(isRangeAuto){
	        		isRangeAuto = false;
	        		textField.setEnabled(true);
	        	}
	        }
	        textField.updateState();
	      }
	    };
		
		JCheckBox chckbxSetAutoRange = new JCheckBox("Auto  ");
		chckbxSetAutoRange.setSelected(true);
		GridBagConstraints gbc_chckbxSetAutoRange = new GridBagConstraints();
		gbc_chckbxSetAutoRange.anchor = GridBagConstraints.WEST;
		gbc_chckbxSetAutoRange.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxSetAutoRange.gridx = 2;
		gbc_chckbxSetAutoRange.gridy = 3;
		panel.add(chckbxSetAutoRange, gbc_chckbxSetAutoRange);
		chckbxSetAutoRange.addChangeListener(changeListenerRange);
		
		JButton button = textField.generateMinusButton();
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 3;
		gbc_button.gridy = 4;
		panel.add(button, gbc_button);
		
		JButton button_1 = textField.generatePlusButton();
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 4;
		gbc_button_1.gridy = 4;
		panel.add(button_1, gbc_button_1);
		
		JLabel lblNewLabel = new JLabel("");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 6;
		gbc_lblNewLabel.gridy = 5;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel label_1 = new JLabel("      ");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 0, 5);
		gbc_label_1.gridx = 5;
		gbc_label_1.gridy = 6;
		panel.add(label_1, gbc_label_1);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				range = textField.getValue();
				dispose();
			}
		});
		panel_2.add(btnOk);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblAdvancedOptions = new JLabel("Advanced Settings");
		lblAdvancedOptions.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblAdvancedOptions);
		
		textField.updateState();
		
	}
				
	public Boolean getIsXDisplay() {
		return isXDisplay;
	}

	public void setIsXDisplay(Boolean isXDisplay) {
		this.isXDisplay = isXDisplay;
	}

	public Boolean getIsYDisplay() {
		return isYDisplay;
	}

	public void setIsYDisplay(Boolean isYDisplay) {
		this.isYDisplay = isYDisplay;
	}

	public Boolean getIsRangeAuto() {
		return isRangeAuto;
	}

	public void setIsRangeAuto(Boolean isRangeAuto) {
		this.isRangeAuto = isRangeAuto;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}
	
	

}
