package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

import Modules.StatusObject;
import Modules.TimerEvent;
import card.Card;

public class InternalFrameManager {
	
	private ArrayList<InternalFrame> listOfIF = new ArrayList<InternalFrame>();
	
	private DenebUI denUI;
	private RandomDataGenerator rdg;
	private JDesktopPane desktopPane;
	
	private int defaultXBound = 15;
	private int defaultYBound = 15;
	
	public InternalFrameManager(DenebUI denUI,JDesktopPane desktopPane){
		
		this.denUI = denUI;
		this.desktopPane = desktopPane;
		
	}
	
	public void promptClient(){
		
		ClientCreation cc = new ClientCreation(denUI);
		cc.setVisible(true);
		
	}
	
	public void promptServer(){
		
		ServerCreation sc = new ServerCreation(denUI);
		sc.setVisible(true);
		
	}
	
	public void promptNumericGraph(){
		
		NumericGraphCreation ngc = new NumericGraphCreation(denUI);
		ngc.setVisible(true);
		
	}
	
	public void promptStatusIndicator(){
		
		StatusIndicatorCreation ngc = new StatusIndicatorCreation(denUI);
		ngc.setVisible(true);
		
	}
	
	public void promptGauge(){
		
		GaugeCreation ngc = new GaugeCreation(denUI);
		ngc.setVisible(true);
		
	}
	
	public void addServer(String name, int port, boolean autoConnect,CoordinateSet coordSet){
		
		ServerGui s = new ServerGui(denUI.getXeon(),name,port,autoConnect);
		s.setTitle(name + " - Server");
		s.setResizable(true);
		s.setMaximizable(true);
		s.setIconifiable(true);
		s.setClosable(true);
		applyCoordSet(s, coordSet, 250, 300);
		
		desktopPane.add(s);
		
		listOfIF.add(s);
		
		s.setVisible(true);
		
	}
	
	public void addClient(String name,String address,int port,int timeout, boolean autoConnect,CoordinateSet coordSet){
		
		ClientGui s = new ClientGui(denUI.getXeon(), name, address, port, timeout, autoConnect);
		s.setTitle(name + " - Client");
		s.setResizable(true);
		s.setMaximizable(true);
		s.setIconifiable(true);
		s.setClosable(true);
		applyCoordSet(s, coordSet, 250, 300);
		
		desktopPane.add(s);
		
		listOfIF.add(s);
		
		s.setVisible(true);
		
	}
	
	public void addNewGraphWindow(String title,int dataIndex,Color lineColor,Color backGroundColor,Color GridColor,String graphTitle,String yAxisTitle,boolean isXAxisVisible,boolean isYAxisVisible,boolean isRangeAuto, int range, CoordinateSet coordSet){
		
		CrossHairChart CrossHairChart = new CrossHairChart(graphTitle,lineColor,backGroundColor,GridColor,yAxisTitle,isXAxisVisible,isYAxisVisible,isRangeAuto,range);
		
		NumericGraph internalFrame = new NumericGraph(denUI.getXeon(),CrossHairChart,dataIndex);
		internalFrame.setTitle(title);
		internalFrame.setResizable(true);
		internalFrame.setMaximizable(true);
		internalFrame.setIconifiable(true);
		internalFrame.setClosable(true);
		applyCoordSet(internalFrame, coordSet, 500, 400);
		desktopPane.add(internalFrame);
		
		
		internalFrame.getContentPane().add(CrossHairChart.getChartPanel(), BorderLayout.CENTER);
		
		internalFrame.setVisible(true);
		
		listOfIF.add(internalFrame);
		
	}
	
	public void addNewStatusIndicator(String windowTitle,String title,int dataIndex,int defaultIndex,ArrayList<StatusObject> statusList,CoordinateSet coordSet){
		
		StatusIndicator si = new StatusIndicator(denUI.getXeon(),title, dataIndex, defaultIndex, statusList);
		si.setTitle(title);
		si.setResizable(true);
		si.setMaximizable(true);
		si.setIconifiable(true);
		si.setClosable(true);
		applyCoordSet(si, coordSet, 200, 150);
		desktopPane.add(si);
		
		si.setVisible(true);
		
		listOfIF.add(si);
		
	}
	
	public void addNewGauge(String windowTitle,String title,int dataIndex,Color lowCol,Color highCol,Color backCol,int minVal,int maxVal,CoordinateSet coordSet){
		
		Gauge gau = new Gauge(denUI.getXeon(),title, dataIndex, lowCol, highCol, backCol, minVal, maxVal);
		gau.setTitle(title);
		gau.setResizable(true);
		gau.setMaximizable(true);
		gau.setIconifiable(true);
		gau.setClosable(true);
		applyCoordSet(gau, coordSet, 100, 150);
		desktopPane.add(gau);
		
		gau.setVisible(true);
		
		listOfIF.add(gau);
		
	}
	
	public void addNewTimeOutAlarm(int timeout,boolean autoenable,CoordinateSet coordSet,boolean muted){
        
        TimeoutAlarm indi = new TimeoutAlarm(denUI.getXeon(),timeout,autoenable,muted);
        indi.setTitle("Timeout Alarm");
        indi.setResizable(true);
        indi.setMaximizable(true);
        indi.setIconifiable(true);
        indi.setClosable(true);
        applyCoordSet(indi, coordSet, 500, 300);
        desktopPane.add(indi);
        
        indi.setVisible(true);
        
        listOfIF.add(indi);
        
    }
	
	public EventScheduler addNewEventSheduler(String windowTitle,ArrayList<TimerEvent> eventList,CoordinateSet coordSet){
		
		EventScheduler gau = new EventScheduler(denUI.getXeon(),windowTitle);
		gau.getCellRenderer().setListOfEvents(eventList);
		gau.setTitle(windowTitle);
		gau.setResizable(true);
		gau.setMaximizable(true);
		gau.setIconifiable(true);
		gau.setClosable(true);
		applyCoordSet(gau, coordSet, 500, 300);
		desktopPane.add(gau);
		
		gau.setVisible(true);
		
		listOfIF.add(gau);
		
		return gau;
		
	}
	
	
	public void openRandGen(){
		
		if(rdg != null){
			if(!rdg.isVisible()){
				rdg = new RandomDataGenerator(denUI.getXeon());
				rdg.setVisible(true);
			}else{
				rdg.requestFocus();
			}
		}else{
			rdg = new RandomDataGenerator(denUI.getXeon());
			rdg.setVisible(true);
		}
		
		
	}
	
	private void applyCoordSet(InternalFrame internalFrame, CoordinateSet coordSet,int defaultWidth, int defaultHeight){
	    if(coordSet != null){
	        internalFrame.setBounds(coordSet.getX(), coordSet.getY(), coordSet.getW(), coordSet.getH());
	    }else{
	        internalFrame.setBounds(defaultXBound, defaultYBound, defaultWidth, defaultHeight);
	        //Increasing default position bounds to create cascase effect
	        if(defaultXBound >= 75){
	            defaultXBound = 15;
	        }
	        if(defaultYBound >= 75){
	            defaultYBound = 15;
	        }
	        defaultXBound += 15;
	        defaultYBound += 15;
	    }
	}

	public DenebUI getDenUI() {
		return denUI;
	}

	public void setDenUI(DenebUI denUI) {
		this.denUI = denUI;
	}

	public ArrayList<InternalFrame> getListOfIF() {
		return listOfIF;
	}

	public void setListOfIF(ArrayList<InternalFrame> listOfIF) {
		this.listOfIF = listOfIF;
	}

	
	

}
