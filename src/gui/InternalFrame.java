package gui;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.UUID;

import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import core.Xeon;

public abstract class InternalFrame extends JInternalFrame{

    private Xeon xeon;
    
	private int dataIndex;
	
	private String type;
	
	private boolean areActionsDeclared = false;
	
	private String uuidIdentifier = "";
	
	public InternalFrame(final Xeon xeon,int dataIndex,String type){
		
	    this.xeon = xeon;
		this.dataIndex = dataIndex;
		this.type = type;
		
		//Only used by windowbuilder
		if(xeon == null){
		    return;
		}
		
		xeon.action();
		
		UUID uuid = UUID.randomUUID();
        uuidIdentifier = uuid.toString();
		
		this.addComponentListener(new ComponentAdapter() {
		    @Override
		    public void componentResized(final ComponentEvent e) {
		        super.componentResized(e);
		        if(areActionsDeclared){
		        	xeon.action();
		        }
		    }
		    @Override
		    public void componentMoved(final ComponentEvent e) {
		        super.componentMoved(e);
		        if(areActionsDeclared){
		        	xeon.action();
		        }
		    }
		});
		
		this.addInternalFrameListener(new InternalFrameAdapter(){
            public void internalFrameClosing(InternalFrameEvent e) {
            	
            	if(areActionsDeclared){
		        	xeon.action();
		        }
            	
            	windowClosed();
            	
            	for(int i = 0; i < xeon.getMainWindow().getInternalFrameManager().getListOfIF().size(); i++){
            		
            		if(xeon.getMainWindow().getInternalFrameManager().getListOfIF().get(i).getUuidIdentifier().equals(uuidIdentifier)){
            			
            			xeon.getMainWindow().getInternalFrameManager().getListOfIF().remove(i);
            			
            		}
            		
            	}
            	
            }
        });
		
	}
	
	public void addData(String s){
		
		
		
	}
	
	public void windowClosed(){
		
	}

	public int getDataIndex() {
		return dataIndex;
	}

	public void setDataIndex(int dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAreActionsDeclared() {
		return areActionsDeclared;
	}

	public void setAreActionsDeclared(boolean areActionsDeclared) {
		this.areActionsDeclared = areActionsDeclared;
	}

	public String getUuidIdentifier() {
		return uuidIdentifier;
	}

	public void setUuidIdentifier(String uuidIdentifier) {
		this.uuidIdentifier = uuidIdentifier;
	}

    public Xeon getXeon() {
        return xeon;
    }

    public void setXeon(Xeon xeon) {
        this.xeon = xeon;
    }
	
}
