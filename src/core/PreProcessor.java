package core;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class PreProcessor {

    private Xeon xeon;
    
    private boolean enablePreProcessing = false;
    private String headerToRemove = "";
    private String footerToRemove = "";
    private String regexToExecute = "";
    private String regexToReplace = "";
    
    public PreProcessor(Xeon xeon){
        
        this.xeon = xeon;
        
    }
    
    public String preProcess(String dataInput){
        
        System.out.println("IN:"+dataInput);
        
        //Execute standard preprocessing
        dataInput = dataInput.replaceAll("(?:\\n|\\r)", "");
        
        //If preprocessing is enabled
        if(enablePreProcessing){
            
            //Execute remove header
            if(headerToRemove.length()>0 && dataInput.startsWith(headerToRemove)){
                try{
                    dataInput = dataInput.replaceFirst(Pattern.quote(headerToRemove), "");
                }catch(PatternSyntaxException e){
                    
                }
            }
            //Execute remove footer
            if(footerToRemove.length()>0 && dataInput.endsWith(footerToRemove)){
                try{
                    dataInput = dataInput.replaceAll(Pattern.quote(footerToRemove)+"$", "");
                }catch(PatternSyntaxException e){
                    
                }
            }
            //Execute custom regex
            if(regexToExecute.length()>0){
                try{
                    dataInput = dataInput.replaceAll(regexToExecute, regexToReplace);
                }catch(PatternSyntaxException e){
                    
                }
            }
            
        }
        
        System.out.println("OUT:"+dataInput);
        
        //Return result
        return dataInput;
    }
    
    public void resetConfig(){
        enablePreProcessing = false;
        headerToRemove="";
        footerToRemove="";
        regexToExecute="";
        regexToReplace="";
    }
    
    public boolean isEnablePreProcessing() {
        return enablePreProcessing;
    }

    public void setEnablePreProcessing(boolean enablePreProcessing) {
        this.enablePreProcessing = enablePreProcessing;
    }

    public String getHeaderToRemove() {
        return headerToRemove;
    }

    public void setHeaderToRemove(String headerToRemove) {
        this.headerToRemove = headerToRemove;
    }

    public String getFooterToRemove() {
        return footerToRemove;
    }

    public void setFooterToRemove(String footerToRemove) {
        this.footerToRemove = footerToRemove;
    }

    public String getRegexToExecute() {
        return regexToExecute;
    }

    public void setRegexToExecute(String regexToExecute) {
        this.regexToExecute = regexToExecute;
    }

    public String getRegexToReplace() {
        return regexToReplace;
    }

    public void setRegexToReplace(String regexToReplace) {
        this.regexToReplace = regexToReplace;
    }
    
}
