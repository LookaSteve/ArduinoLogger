package core;

import java.util.UUID;

import com.esotericsoftware.kryo.Kryo;

import Modules.NetPayload;

public class LocalConfig {

    public static UUID localId = null;
    
    public static void register(){
        
        if(localId == null){
            
            localId = UUID.randomUUID();
            
        }
        
    }
    
    public static synchronized void registerKryo(Kryo kryo){
        
        kryo.register(NetPayload.class);
        
    }
    
}