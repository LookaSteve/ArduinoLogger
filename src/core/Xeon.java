package core;

import java.awt.Color;
import java.io.File;

import Logger.LogManager;
import card.CardManager;
import gui.DenebUI;
import xml.DefaultConfig;
import xml.SaveManager;

public class Xeon {
	
	private DenebUI mainWindow;
	private CardManager cardManager;
	private DataProcessor dataProcessor;
	private LogManager logManager;
	private SaveManager saveManager;
	private DefaultConfig defaultConfigManager;
	private PreProcessor preProcessor;
	private SoundManager soundManager;
	
	private String dataSeparator = ";";
	private int numberOfActions = 0;
	private final String defaultLogoPath = "res/logo.png";
	private File placeholderPath = new File(System.getProperty("user.home"));
	private File logPath = placeholderPath;
	private String defaultLogHeader = "log";
	
	//Config variables
	
	private int serverCommunicationPort = 12073;
	private int serverTimeout = 100;
	private int defaultBaudRate = 9600;
	private String logHeader = defaultLogHeader;
	
	private boolean loopProtection = true;
	private File logoFile = new File(defaultLogoPath);
	private int logoSize = 50;
	private int logoOpacity = 100;
	private File defaultSavePath = placeholderPath;
	private File defaultDataFileManagementPath = placeholderPath;
	private Color backgroundColor = Color.white;
	
	public Xeon(){
		
	    //Start sound manager
	    soundManager = new SoundManager(this);
	    
		//Start serial card management
		cardManager = new CardManager(this);
		
		//Creating the log manager
		logManager = new LogManager(this);
		
		//Creating the save manager
		saveManager = new SaveManager(this);
		
		//Loads local config
        defaultConfigManager = new DefaultConfig(this, new File("data/config.xml"));
        defaultConfigManager.loadConfiguration();
        
        //Start ui
        mainWindow = new DenebUI(this);
        
        //Setting up pre processor
        preProcessor = new PreProcessor(this);
        
        //Creating the data processor
        dataProcessor = new DataProcessor(this);
        
        //Starting the card scanner
        cardManager.startCardScanner();
		
	}
	
	public void start(){
		
		mainWindow.frmDenebRcEdition.setVisible(true);
		
	}
	
	public void action(){
		
		numberOfActions++;
		
	}
	
	public void clearActions(){
		
		numberOfActions = 0;
		
	}
	
	

	public synchronized int getServerCommunicationPort() {
		return serverCommunicationPort;
	}

	public synchronized void setServerCommunicationPort(int serverCommunicationPort) {
		this.serverCommunicationPort = serverCommunicationPort;
	}

	public DenebUI getMainWindow() {
		return mainWindow;
	}

	public void setMainWindow(DenebUI mainWindow) {
		this.mainWindow = mainWindow;
	}

	public CardManager getCardManager() {
		return cardManager;
	}

	public void setCardManager(CardManager cardManager) {
		this.cardManager = cardManager;
	}

	public DataProcessor getDataProcessor() {
		return dataProcessor;
	}

	public void setDataProcessor(DataProcessor dataProcessor) {
		this.dataProcessor = dataProcessor;
	}

	public String getDataSeparator() {
		return dataSeparator;
	}

	public void setDataSeparator(String dataSeparator) {
		this.dataSeparator = dataSeparator;
	}

	public LogManager getLogManager() {
		return logManager;
	}

	public void setLogManager(LogManager logManager) {
		this.logManager = logManager;
	}

	public SaveManager getSaveManager() {
		return saveManager;
	}

	public void setSaveManager(SaveManager saveManager) {
		this.saveManager = saveManager;
	}

	public int getNumberOfActions() {
		return numberOfActions;
	}

	public void setNumberOfActions(int numberOfActions) {
		this.numberOfActions = numberOfActions;
	}

	public int getServerTimeout() {
		return serverTimeout;
	}

	public void setServerTimeout(int serverTimeout) {
		this.serverTimeout = serverTimeout;
	}

	public int getDefaultBaudRate() {
		return defaultBaudRate;
	}

	public void setDefaultBaudRate(int defaultBaudRate) {
		this.defaultBaudRate = defaultBaudRate;
	}

	public boolean isLoopProtection() {
		return loopProtection;
	}

	public void setLoopProtection(boolean loopProtection) {
		this.loopProtection = loopProtection;
	}

	public File getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(File logoFile) {
		this.logoFile = logoFile;
	}

	public int getLogoSize() {
		return logoSize;
	}

	public void setLogoSize(int logoSize) {
		this.logoSize = logoSize;
	}

	public String getDefaultLogoPath() {
		return defaultLogoPath;
	}

	public File getDefaultSavePath() {
		return defaultSavePath;
	}

	public void setDefaultSavePath(File defaultSavePath) {
		this.defaultSavePath = defaultSavePath;
	}

	public DefaultConfig getDefaultConfigManager() {
		return defaultConfigManager;
	}

	public void setDefaultConfigManager(DefaultConfig defaultConfigManager) {
		this.defaultConfigManager = defaultConfigManager;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public File getLogPath() {
		return logPath;
	}

	public void setLogPath(File logPath) {
		this.logPath = logPath;
	}

    public File getDefaultDataFileManagementPath() {
        return defaultDataFileManagementPath;
    }

    public void setDefaultDataFileManagementPath(File defaultDataFileManagementPath) {
        this.defaultDataFileManagementPath = defaultDataFileManagementPath;
    }

    public File getPlaceholderPath() {
        return placeholderPath;
    }

    public void setPlaceholderPath(File placeholderPath) {
        this.placeholderPath = placeholderPath;
    }

    public int getLogoOpacity() {
        return logoOpacity;
    }

    public void setLogoOpacity(int logoOpacity) {
        this.logoOpacity = logoOpacity;
    }

    public String getDefaultLogHeader() {
        return defaultLogHeader;
    }

    public void setDefaultLogHeader(String defaultLogHeader) {
        this.defaultLogHeader = defaultLogHeader;
    }

    public String getLogHeader() {
        return logHeader;
    }

    public void setLogHeader(String logHeader) {
        this.logHeader = logHeader;
    }

    public PreProcessor getPreProcessor() {
        return preProcessor;
    }

    public void setPreProcessor(PreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public void setSoundManager(SoundManager soundManager) {
        this.soundManager = soundManager;
    }
}
