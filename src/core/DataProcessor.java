package core;

import gui.ClientGui;
import gui.GlobalConsole;
import gui.InternalFrame;
import gui.InternalFrameManager;
import gui.ServerGui;

public class DataProcessor {
	
	private InternalFrameManager ifm;
	
	private Xeon xeon;
	
	private GlobalConsole globalConsole = null;
	
	public DataProcessor(Xeon xeon){
		
		this.xeon = xeon;
		
		ifm = xeon.getMainWindow().getInternalFrameManager();
		
	}
	
	public synchronized void processData(String data,String source){
		
		//Safeguards
		if(data == null){
			return;
		}
		
		if(data.equals("")){
			return;
		}
		
		//Pre process data
		data = xeon.getPreProcessor().preProcess(data);
		
		//Splitting the pre processed data with the separator
		String[] splitData = data.split(xeon.getDataSeparator());
		
		int dataLength = splitData.length;
		
		//Null size check
		if(dataLength <= 0){
			return;
		}
	
		//Log on file
		xeon.getLogManager().log(data);
		
		//Propagate data in the IF's
		for(int i = 0; i < dataLength; i++){
			for(int j = 0; j < ifm.getListOfIF().size(); j++){
				InternalFrame frame = ifm.getListOfIF().get(j);
				if(frame.getDataIndex() == i){
					frame.addData(splitData[i]);
				}
			}
		}
		
		//Propagate data to internalFrames requiring full dataString (data index -1)
		for(int i = 0; i < ifm.getListOfIF().size(); i++){
            InternalFrame frame = ifm.getListOfIF().get(i);
            if(frame.getDataIndex() == -1){
                frame.addData(data);
            }
        }
		
		//Propagate to global console
		if(globalConsole != null){
			globalConsole.addRemoteBroadcast("[↓] " + data);
		}
		
		//Propagate to servers
		for(int i = 0; i < ifm.getListOfIF().size(); i++){
			InternalFrame frame = ifm.getListOfIF().get(i);
			if(frame.getType().equals("DATASERVER")){
				ServerGui server = (ServerGui) frame;
				server.sendData(data,source);
			}
		}
		
	}
	
	public void upPropagate(String data,String source){
		
		//Propagate to cards
		for(int i = 0; i < xeon.getCardManager().getConnectedCards().size(); i ++){
			
			xeon.getCardManager().getConnectedCards().get(i).writeData(data);
			
		}
		
		//Propagate to global console
		if(globalConsole != null){
			globalConsole.addRemoteBroadcast("[↑] " + data);
		}
		
		//Propagate to clients
		for(int i = 0; i < ifm.getListOfIF().size(); i++){
			
			InternalFrame frame = ifm.getListOfIF().get(i);
			
			if(frame.getType().equals("DATACLIENT")){
				
				ClientGui client = (ClientGui) frame;
				
				client.sendData(data,source);
				
			}
			
		}
		
	}
	
	public String getLocalSource(){
		
		if(LocalConfig.localId == null){
			
			LocalConfig.register();
			
		}
		
		return LocalConfig.localId.toString();
		
	}

	public GlobalConsole getGlobalConsole() {
		return globalConsole;
	}

	public void setGlobalConsole(GlobalConsole globalConsole) {
		this.globalConsole = globalConsole;
	}
	
	
	
}
