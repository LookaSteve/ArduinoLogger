package core;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SoundManager {

    private Xeon xeon;
    
    private Map<String, AudioClip> soundMap = new HashMap<String, AudioClip>();
    
    
    public SoundManager(Xeon xeon){
        
        this.xeon = xeon;
        
        //Loading base sounds
        loadSound("beep");
        loadSound("alert");
        loadSound("triple");
        
    }
    
    public void play(String ressourceName){
        
        AudioClip sound = soundMap.get(ressourceName);
        if(sound != null){
            sound.play();
        }
        
    }
    
    public boolean loadSound(String ressourceName){
        
        
        AudioClip sound = loadRessource("data/"+ressourceName+".wav");
        if(sound!=null){
            soundMap.put(ressourceName, sound);
            return true;
        }
        return false;
        
    }
    
    private AudioClip loadRessource(String ressourcePath){
        
        try {
            URL url = new File(ressourcePath).toURI().toURL();
            return Applet.newAudioClip(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        
        return null;
        
    }
    
}
