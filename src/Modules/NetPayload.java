package Modules;

public class NetPayload {

    //Header values
    private String remoteSource;
    private String originSource;
    
    //Key values
    private String payload;

    //Noarg constructor
    public NetPayload(){
        //KEEP EMPTY
    }
    
    //Useful constructor
    public NetPayload(String remoteSource, String originSource, String payload) {
        this.remoteSource = remoteSource;
        this.originSource = originSource;
        this.payload = payload;
    }

    public String getRemoteSource() {
        return remoteSource;
    }

    public void setRemoteSource(String remoteSource) {
        this.remoteSource = remoteSource;
    }

    public String getOriginSource() {
        return originSource;
    }

    public void setOriginSource(String originSource) {
        this.originSource = originSource;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
    
}
