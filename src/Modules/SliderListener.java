package Modules;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import core.Xeon;

public abstract class SliderListener implements ChangeListener{

    private Xeon xeon;
    
    public SliderListener(Xeon xeon){
        this.xeon = xeon;
    }
    
    public void stateChanged(ChangeEvent e) {
        //Override with an appropriate method
    }

    public Xeon getXeon() {
        return xeon;
    }

    public void setXeon(Xeon xeon) {
        this.xeon = xeon;
    }
    
}
