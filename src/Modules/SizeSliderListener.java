package Modules;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import core.Xeon;

public class SizeSliderListener extends SliderListener{

    public SizeSliderListener(Xeon xeon) {
        super(xeon);
    }
    
    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider)e.getSource();
        if (!source.getValueIsAdjusting()) {
            getXeon().setLogoSize(source.getValue());
            getXeon().getMainWindow().getDesktopPane().repaint();
            getXeon().getDefaultConfigManager().saveConfiguration();
        }    
    }

}
