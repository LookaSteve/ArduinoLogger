package Modules;

import core.Xeon;

public class NetSystem {

	private Xeon xeon;
	
	public NetSystem(Xeon xeon){
		
		this.xeon = xeon;
		
	}
	
	public boolean interpretHeader(NetPayload netPayload){
		
		boolean isValidPacket = true;
		
		if(!checkSource(netPayload.getRemoteSource(),false)){
		    isValidPacket = false; 
		    System.out.println("Direct feedback blocked");
		}
		
		if(!checkSource(netPayload.getOriginSource(),true)){
		    isValidPacket = false;
		    System.out.println("Net loop blocked");
		}
			
		return isValidPacket;
		
	}
	
	private boolean checkSource(String source,boolean checkRights){
		
		if(!xeon.isLoopProtection() && checkRights){
			return true;
		}
		
		if(xeon.getDataProcessor().getLocalSource().equals(source)){
			return false;
		}
		
		return true;
		
	}
	
}
