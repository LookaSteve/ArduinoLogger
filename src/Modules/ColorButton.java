package Modules;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;

public class ColorButton extends JButton{

	private Color color;
	private Component parent;
	private String hint = "Choose a color";
	
	public ColorButton(Component parent,Color color){
		
		super("Choose Color");
		this.color = color;
		this.parent = parent;
		init();
		
	}
	
	public ColorButton(Component parent,Color color,String hint){
		
		super("Choose Color");
		this.color = color;
		this.parent = parent;
		this.hint = hint;
		init();
		
	}
	
	private void init(){
		
		this.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Color newColor = JColorChooser.showDialog(parent,hint,Color.red);
				if(newColor != null){
					color = newColor;
					updateColor();
				}
				
			
			}
		});
		
		updateColor();
		
	}
	
	private void updateColor(){
		
		this.setBackground(color);
		this.setForeground(Tools.getContrastColor(color));
		
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
}
