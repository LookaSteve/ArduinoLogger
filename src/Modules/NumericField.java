package Modules;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;

public class NumericField extends JTextField{

	private JButton minusButton = null;
	private JButton plusButton = null;
	
	private boolean allowNegativeValues = false;
	
	private NumericField thisObj;
	
	public NumericField(){
		
		super();
		init();
		
	}
	
	//Initiate the int filter
	private void init(){
		
		thisObj = this;
		
		PlainDocument doc = (PlainDocument) this.getDocument();
	    doc.setDocumentFilter(new IntFilter());
	    
	}
	
	public void updateState(){
		
		if(isEnabled()){
			setButtonState(minusButton,true);
			setButtonState(plusButton, true);
		}else{
			setButtonState(minusButton,false);
			setButtonState(plusButton, false);
		}
		
	}
	
	public JButton generateMinusButton(){
		
		minusButton = new JButton("-");
		minusButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int number = Tools.cleanInputInt(thisObj.getText());
				
				if(number > 0 || allowNegativeValues){
					
					thisObj.setText(""+(number-1));
				
				}
			
			}
		});
		
		return minusButton;
		
	}
	
	public JButton generatePlusButton(){
		
		plusButton = new JButton("+");
		plusButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int number = Tools.cleanInputInt(thisObj.getText());
				
				thisObj.setText(""+(number+1));
				
			}
		});
		
		return plusButton;
		
	}
	
	public int getValue(){
		
		return Tools.cleanInputInt(thisObj.getText());
		
	}
	
	public void setValue(int value){
		
		this.setText(value+"");
		
	}
	
	private void setButtonState(JButton button,boolean state){
		
		if(button != null){
			
			button.setEnabled(state);
			
		}
		
	}

	public JButton getMinusButton() {
		return minusButton;
	}

	public void setMinusButton(JButton minusButton) {
		this.minusButton = minusButton;
	}

	public JButton getPlusButton() {
		return plusButton;
	}

	public void setPlusButton(JButton plusButton) {
		this.plusButton = plusButton;
	}

	public boolean isAllowNegativeValues() {
		return allowNegativeValues;
	}

	public void setAllowNegativeValues(boolean allowNegativeValues) {
		this.allowNegativeValues = allowNegativeValues;
	}
	
	
	
}
