package Modules;

import java.awt.Color;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Tools {
	
	public static synchronized Color getContrastColor(Color color) {
		
		if(color == null){
			
			return Color.black;
			
		}
		
		  double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
		  return y >= 128 ? Color.black : Color.white;
		  
	}
	
	public static synchronized Color getTransitionColor(Color aColor,Color bColor,double ratio){
		
		int red = (int)Math.abs((ratio * bColor.getRed()) + ((1 - ratio) * aColor.getRed()));
		int green = (int)Math.abs((ratio * bColor.getGreen()) + ((1 - ratio) * aColor.getGreen()));
		int blue = (int)Math.abs((ratio * bColor.getBlue()) + ((1 - ratio) * aColor.getBlue()));
		
		return new Color(red,green,blue);
		
	}
	
	public static synchronized double differenceBetweenTwoReals(double ra, double rb){
		
		return Math.abs(ra - rb);
		
	}
	
	public static synchronized int randIntMax(int max){
		
		return randInt(0,max);
		
	}
	
	public static synchronized int randInt(int min, int max){
		
		Random r = new Random();
		return r.nextInt(max-min) + min;
		
	}
	
	public static synchronized double randDoubleMax(int max){
		
		return randDouble(0,max);
		
	}
	
	public static synchronized double randDouble(int min, int max){
		
		Random r = new Random();
		return min + (max - min) * r.nextDouble();
		
	}
	
	public static synchronized String randString(){
		
		return Long.toHexString(Double.doubleToLongBits(Math.random()));
		
	}
	
	public static synchronized String getOrderedTime(){
    	String str=Calendar.getInstance().getWeekYear()+getMonth()+Calendar.getInstance().get( Calendar.DAY_OF_MONTH)+"-"+Calendar.getInstance().get( Calendar.HOUR_OF_DAY)+"."+Calendar.getInstance().get( Calendar.MINUTE)+"."+Calendar.getInstance().get( Calendar.SECOND);
    	return str;
    }
	public static synchronized String getFormatedTime(){
    	String str=Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+":"+Calendar.getInstance().get(Calendar.MINUTE)+":"+Calendar.getInstance().get(Calendar.SECOND)+":"+Calendar.getInstance().get(Calendar.MILLISECOND);
    	return str;
    }
    public static synchronized String getMonth(){
    	String str="ERR";
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.JANUARY){
    		return "JAN";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.FEBRUARY){
    		return "FEB";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.MARCH){
    		return "MAR";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.APRIL){
    		return "APR";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.MAY){
    		return "MAY";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.JUNE){
    		return "JUN";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.JULY){
    		return "JUL";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.AUGUST){
    		return "AUG";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.SEPTEMBER){
    		return "SEP";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.OCTOBER){
    		return "OCT";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.NOVEMBER){
    		return "NOV";
    	}
    	if(Calendar.getInstance().get( Calendar.MONTH )==Calendar.DECEMBER){
    		return "DEC";
    	}
    	return str;
    }
    
    public static synchronized int reverseMonth(String monthCode){
        
        switch (monthCode) {
        
            case "JAN": return Calendar.JANUARY;
            case "FEB": return Calendar.FEBRUARY;
            case "MAR": return Calendar.MARCH;
            case "APR": return Calendar.APRIL;
            case "MAY": return Calendar.MAY;
            case "JUN": return Calendar.JUNE;
            case "JUL": return Calendar.JULY;
            case "AUG": return Calendar.AUGUST;
            case "SEP": return Calendar.SEPTEMBER;
            case "OCT": return Calendar.OCTOBER;
            case "NOV": return Calendar.NOVEMBER;
            case "DEC": return Calendar.DECEMBER;

        }
        
        return Calendar.JANUARY;
        
    }
    
    public static synchronized String colorToHex(Color c){
    	
    	return "#"+Integer.toHexString(c.getRGB()).substring(2).toUpperCase();
    	
    }
    
    public static synchronized int[] parseTime(String s){
    	
    	int[] returnArray = new int[4];
    	
    	//Default unset values
    	returnArray[0] = -1;
    	returnArray[1] = -1;
    	returnArray[2] = -1;
    	returnArray[3] = -1;
    	
    	if(s == null){
    		
    		return returnArray;
    		
    	}
    	
    	if(s.length() < 1){
    		
    		return returnArray;
    		
    	}
    	
    	String[] splittedStr = s.split(":");
    	
    	if(splittedStr.length == 4){
    		
    		returnArray[0] = Tools.cleanInputInt(splittedStr[0]);
    		returnArray[1] = Tools.cleanInputInt(splittedStr[1]);
    		returnArray[2] = Tools.cleanInputInt(splittedStr[2]);
    		returnArray[3] = Tools.cleanInputInt(splittedStr[3]);
    		
    	}
    	
    	return returnArray;
    	
    }
    
    public static synchronized Date timeArrayToDate(int [] timeArray){
        
        if(timeArray.length < 4){
            return null;
        }
        
        //Check time array validity
        if(timeArray[0]>=0 && timeArray[1]>=0 && timeArray[2]>=0 && timeArray[3]>=0){
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY,timeArray[0]);
            cal.set(Calendar.MINUTE,timeArray[1]);
            cal.set(Calendar.SECOND,timeArray[2]);
            cal.set(Calendar.MILLISECOND,timeArray[3]);
            return cal.getTime();
        }
        
        return null;
    }
    
    public static synchronized int cleanInputInt(String s){
    	
    	if(s.equals("")){
    		
    		return 0;
    		
    	}
    	
    	try {
    		
            return Integer.parseInt(s);
            
        }
        catch (NumberFormatException e) {

            return 0;
            
        }
    	
    }
    
    public static synchronized long cleanInputLong(String s){
        
        if(s.equals("")){
            
            return 0;
            
        }
        
        try {
            
            return Long.parseLong(s);
            
        }
        catch (NumberFormatException e) {

            return 0;
            
        }
        
    }
    
    public static synchronized double cleanInputDouble(String s){
    	
    	if(s.equals("")){
    		
    		return 0.0;
    		
    	}
    	
    	try {
    		
            return Double.parseDouble(s);
            
        }
        catch (NumberFormatException e) {

            return 0.0;
            
        }
    	
    }
    
    public static synchronized String quickTimeFormat(long time){
    	
    	Date date = new Date(time  - 3600000 );
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
		return formatter.format(date);
    	
    }
    
    public static synchronized String filterNewLine(String s){
		
		if(!s.endsWith("\r\n") && !s.endsWith("\n")){
			
			s += "\r\n";
			
		}
		
		return s;
		
	}
    
    public static synchronized boolean isInt(String s){
        if(s == null || s.trim().isEmpty()) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if(!Character.isDigit(s.charAt(i))) {
                return false;
            } 
        }
        return true;
    }
    
    public static boolean isDouble(String str){  
      try{  
        double d = Double.parseDouble(str);  
      }catch(NumberFormatException nfe){  
        return false;  
      }  
      return true;  
    }
    
    public static synchronized String limitString(String input, int limit){
        if (input.length() > limit) {
            return input.substring(0, limit) + "...";
        }else{
            return input;
        }
    }
    


    public static synchronized String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }



}
