package Modules;

import java.awt.Color;

import core.Xeon;
import gui.EventScheduler;

public class TimerEvent {

	private Xeon xeon;
	private EventScheduler eventScheduler;
	private String name;
	private long executionTime;
	private String command;
	private Color color;
	private boolean isPostZero;
	private boolean hasFired = false;
	
	public TimerEvent(Xeon xeon, EventScheduler eventScheduler, String name, long executionTime, String command, Color color, boolean isPostZero){
		
		this.xeon = xeon;
		this.eventScheduler = eventScheduler;
		this.name = name;
		this.executionTime = executionTime;
		this.command = command;
		this.color = color;
		this.isPostZero = isPostZero;
		
	}
	
	public void cycle(long time){
		
		if(!isPostZero && !eventScheduler.isNegative()){
			
			execute();
			
		}
		
		if(isPostZero && !eventScheduler.isNegative()){
			
			if(executionTime < time){
				
				execute();
				
			}
			
		}else if(!isPostZero && eventScheduler.isNegative()){
			
			if(executionTime > time){
				
				execute();
				
			}
			
		}
		
	}
	
	public void execute(){
		
		hasFired = true;
		
		xeon.getDataProcessor().upPropagate(command,xeon.getDataProcessor().getLocalSource());
		
		eventScheduler.consoleLog("Event fired : " + name + " [ " + command + " ]");
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public synchronized Color getColor() {
		return color;
	}

	public synchronized void setColor(Color color) {
		this.color = color;
	}

	public synchronized boolean isPostZero() {
		return isPostZero;
	}

	public synchronized void setPostZero(boolean isPostZero) {
		this.isPostZero = isPostZero;
	}

	public boolean isHasFired() {
		return hasFired;
	}

	public void setHasFired(boolean hasFired) {
		this.hasFired = hasFired;
	}

	public EventScheduler getEventScheduler() {
		return eventScheduler;
	}

	public void setEventScheduler(EventScheduler eventScheduler) {
		this.eventScheduler = eventScheduler;
	}
	
	
	
}
