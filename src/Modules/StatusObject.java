package Modules;

import java.awt.Color;

public class StatusObject {
	
	private String statusName;
	
	private int statusId;
	
	private Color statusColor;
	
	public StatusObject(String statusName,int statusId,Color statusColor){
		
		this.statusName = statusName;
		this.statusId = statusId;
		this.statusColor = statusColor;
		
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Color getStatusColor() {
		return statusColor;
	}

	public void setStatusColor(Color statusColor) {
		this.statusColor = statusColor;
	}
	
	

}
