package Modules;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class HintTextField extends JTextField implements FocusListener {

    private final String hint;
    private boolean showingHint;
    private boolean requestIgnoreEvent;
    
    private List<HintFieldListener> listeners = new ArrayList<HintFieldListener>();

    public HintTextField(final String text,final String hint){
      
      //Default hint setup
      super(hint);
      this.hint = hint;
      this.showingHint = true;
      
      super.addFocusListener(this);
      setForeground(Color.LIGHT_GRAY);
      setDisabledTextColor(Color.LIGHT_GRAY);
      
      //Test if we have valid text pre-entered
      if(text != null){
          if(text.length()>0){
              //Disable hinting
              setText(text);
              this.showingHint = false;
              setForeground(Color.BLACK);
          }
      }
      
      //Setup built in listener
      this.getDocument().addDocumentListener(new DocumentListener(){
          @Override
          public void insertUpdate(DocumentEvent e) {
              update();
          }
          @Override
          public void removeUpdate(DocumentEvent e) {
              update();
          }
          @Override
          public void changedUpdate(DocumentEvent e) {
              update();
          }
          public void update(){
              if(!showingHint && !requestIgnoreEvent){
                  notifyNewText(getText());
              }
              if(requestIgnoreEvent){
                  requestIgnoreEvent=false;
              }
          }
      });
     
    }

    @Override
    public void focusGained(FocusEvent e) {
      if(this.getText().isEmpty()) {
        requestIgnoreEvent = true;
        super.setText("");
        showingHint = false;
        setForeground(Color.BLACK);
      }
    }
    
    @Override
    public void focusLost(FocusEvent e) {
      if(this.getText().isEmpty()) {
        requestIgnoreEvent = true;
        super.setText(hint);
        showingHint = true;
        setForeground(Color.LIGHT_GRAY);
      }
    }

    public void notifyNewText(String newText) {

        for (HintFieldListener hl : listeners){
            hl.fieldUpdated(newText);
        }
        
    }
    
    public void addListener(HintFieldListener toAdd) {
        listeners.add(toAdd);
    }

    @Override
    public String getText() {
      return showingHint ? "" : super.getText();
    }

    public boolean isShowingHint() {
        return showingHint;
    }

    public void setShowingHint(boolean showingHint) {
        this.showingHint = showingHint;
    }

    public boolean isRequestIgnoreEvent() {
        return requestIgnoreEvent;
    }

    public void setRequestIgnoreEvent(boolean requestIgnoreEvent) {
        this.requestIgnoreEvent = requestIgnoreEvent;
    }
    
    public interface HintFieldListener {
        void fieldUpdated(String newText);
    }
    
 }
