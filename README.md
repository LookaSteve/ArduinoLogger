# DenebRC

<p align="center">
  <img src="https://gitlab.com/LookaSteve/ArduinoLogger/raw/master/res/logo.png" width="350"/>
</p>

DenebRC is a java based open source arduino logging software

## Features

### Connects through serial to your arduino cards

![DenebRC serial](https://i.imgur.com/47gcPuZ.png)

### Create graphs to visualize the data

![DenebRC graph](https://i.imgur.com/BsYfWHC.png)

### Create gauges to see bounded values

![DenebRC gauge](https://i.imgur.com/zRAbg70.png)

### Create status indicators to display integer based statuses from your arduino

![DenebRC status indicator](https://i.imgur.com/d9SSB0w.png)

### Create a server to broadcast the telemetry to a remote computer 

![DenebRC server](https://i.imgur.com/v3lAQI3.png)

### Create a client on your remote instance to receive the data

![DenebRC client](https://i.imgur.com/HdzVVBN.png)

### Organize your windows in the virtual desktop

![DenebRC virtual desktop](https://i.imgur.com/OLTQtsD.png)

### Add an event scheduler to create countdowns to serial commands

![DenebRC event scheduler](https://i.imgur.com/YhFSYw4.png)

### Log the data after having chosen your logging path

![DenebRC logging](https://i.imgur.com/wtDpSFb.png)

### See graphs of previously recorded data

![DenebRC data review](https://i.imgur.com/3j6aGy2.png)

### Inject previously recorded data into your instance

![DenebRC injection](https://i.imgur.com/KhTM01m.png)

### And much more

- Serial console to directly adress a connected card
- Global console to talk to cards from a client
- Customizable desktop
- Custom network settings
- Custom baud rates
- Custom channel delimiter
- Default path
- Savable desktop configuration
- More to come !

## Getting Started



### Prerequisites

DenebRC is made with Java, the java jre 1.8 or above has to be installed.
Follow this link to download the jre :
```
http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
```

### Installing / Running

Simply clone this repository and run DenebRC.jar.
You will be greeted by an empty desktop.

![DenebRC welcome](https://i.imgur.com/rbDeJHh.png)

### Connecting your card

Simply connect your card via usb on your computer with DenebRC running.
Make sure that the card isn't already connected to the arduino console.

The card should appear on the "Cards" list of DenebRC.

